package mindbody

import (
	"encoding/xml"
	"strconv"
	"time"
)

// CustomTimeFormats is the various datetime formats we get from MB
var CustomTimeFormats = []string{
	"2006-01-02T15:04:05",
	"2006-01-02T15:04:05Z",
	"2006-01-02T15:04:05.000-07:00",
	"2006-01-02T15:04:05.0000-07:00",
	"2006-01-02T15:04:05.00000-07:00",
	"2006-01-02T15:04:05.000000-07:00",
	"2006-01-02T15:04:05.0000000-07:00",
}

// CustomTime represents a nullable time
type CustomTime struct {
	time.Time
}

// UnmarshalXML for CustomTime
func (c *CustomTime) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	var err error
	var parse time.Time
	for _, tf := range CustomTimeFormats {
		parse, err = time.Parse(tf, v)
		if err == nil {
			break
		}
	}
	if err != nil {
		return err
	}

	*c = CustomTime{parse}
	return nil
}

// MarshalXML for CustomTime
func (c *CustomTime) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	zeroTime := time.Time{}

	if c.Time == zeroTime {
		return nil
	}

	return e.EncodeElement(c.Time.Format("2006-01-02"), start)
}

// CustomInt64 representsa nullable int64
type CustomInt64 struct {
	Int int64
}

// UnmarshalXML for CustomInt64
func (i *CustomInt64) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	parse, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return err
	}
	*i = CustomInt64{parse}
	return nil
}

// MarshalXML for CustomInt64
func (i *CustomInt64) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(i.Int, start)
}

// CustomInt32 representsa nullable int64
type CustomInt32 struct {
	Int int32
}

// UnmarshalXML for CustomInt32
func (i *CustomInt32) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	parse, err := strconv.ParseInt(v, 10, 32)
	if err != nil {
		return err
	}
	*i = CustomInt32{int32(parse)}
	return nil
}

// MarshalXML for CustomInt32
func (i *CustomInt32) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(i.Int, start)
}

// CustomInt16 representsa nullable int16
type CustomInt16 struct {
	Int int16
}

// UnmarshalXML for CustomInt16
func (i *CustomInt16) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	parse, err := strconv.ParseInt(v, 10, 16)
	if err != nil {
		return err
	}
	*i = CustomInt16{int16(parse)}
	return nil
}

// MarshalXML for CustomInt16
func (i *CustomInt16) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(i.Int, start)
}

// CustomFloat32 represents a float32 nullable type
type CustomFloat32 struct {
	Float float32
}

// UnmarshalXML for CustomFloat32
func (f *CustomFloat32) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	parse, err := strconv.ParseFloat(v, 32)
	if err != nil {
		return err
	}

	*f = CustomFloat32{float32(parse)}
	return nil
}

// MarshalXML for CustomFloat32
func (f *CustomFloat32) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(f.Float, start)
}

// CustomFloat64 represents a float64 nullable type
type CustomFloat64 struct {
	Float float64
}

// UnmarshalXML for CustomFloat64
func (f *CustomFloat64) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var v string
	d.DecodeElement(&v, &start)

	if v == "" {
		return nil
	}

	parse, err := strconv.ParseFloat(v, 64)
	if err != nil {
		return err
	}
	*f = CustomFloat64{parse}
	return nil
}

// MarshalXML for CustomFloat64
func (f *CustomFloat64) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(f.Float, start)
}
