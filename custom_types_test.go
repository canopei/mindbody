package mindbody

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"testing"

	"math"

	"time"

	"github.com/stretchr/testify/assert"
)

func TestCustomTime(t *testing.T) {
	var ct CustomTime

	// We do this to get rid of nanoseconds, location - that we would get with Now()
	testTime := "2006-01-02T15:04:05"
	tt, _ := time.Parse(CustomTimeFormats[0], testTime)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%s</tag>", tt.Format(CustomTimeFormats[0]))), &ct)
	assert.Nil(t, err)
	assert.Equal(t, tt, ct.Time)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &ct)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, tt, ct.Time)

	// Unmarshal with invalid format
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &ct)
	assert.NotNil(t, err)

	// Marshal zero-value time
	ct = CustomTime{Time: time.Time{}}
	res, err := xml.Marshal(&ct)
	assert.Nil(t, err)
	assert.Equal(t, "", string(res))

	// Marshal
	ct = CustomTime{Time: tt}
	res, err = xml.Marshal(&ct)
	assert.Nil(t, err)
	// We get an additional "Z" from the default Time marsheler
	assert.Equal(t, fmt.Sprintf("<CustomTime>%sZ</CustomTime>", testTime), string(res), "Z")
}

func TestCustomInt16(t *testing.T) {
	var ci CustomInt16
	ti := int16(math.MaxInt16)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d</tag>", ti)), &ci)
	assert.Nil(t, err)
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &ci)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with non-int value
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &ci)
	assert.NotNil(t, err)

	// Unmarshal out of bounds of int32
	err = xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d</tag>", int32(ti)+1)), &ci)
	assert.NotNil(t, err)

	// Marshal
	ci = CustomInt16{Int: ti}
	res, err := xml.Marshal(&ci)
	assert.Nil(t, err)
	assert.Equal(t, fmt.Sprintf("<CustomInt16>%d</CustomInt16>", ti), string(res))
}

func TestCustomInt32(t *testing.T) {
	var ci CustomInt32
	ti := int32(math.MaxInt32)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d</tag>", ti)), &ci)
	assert.Nil(t, err)
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &ci)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with non-int value
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &ci)
	assert.NotNil(t, err)

	// Unmarshal out of bounds of int32
	err = xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d</tag>", int64(ti)+1)), &ci)
	assert.NotNil(t, err)

	// Marshal
	ci = CustomInt32{Int: ti}
	res, err := xml.Marshal(&ci)
	assert.Nil(t, err)
	assert.Equal(t, fmt.Sprintf("<CustomInt32>%d</CustomInt32>", ti), string(res))
}

func TestCustomInt64(t *testing.T) {
	var ci CustomInt64
	ti := int64(math.MaxInt64)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d</tag>", ti)), &ci)
	assert.Nil(t, err)
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &ci)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, ti, ci.Int)

	// Unmarshal with non-int value
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &ci)
	assert.NotNil(t, err)

	// Unmarshal out of bounds of int64
	err = xml.Unmarshal([]byte(fmt.Sprintf("<tag>%d1</tag>", ti)), &ci)
	assert.NotNil(t, err)

	// Marshal
	ci = CustomInt64{Int: ti}
	res, err := xml.Marshal(&ci)
	assert.Nil(t, err)
	assert.Equal(t, fmt.Sprintf("<CustomInt64>%d</CustomInt64>", ti), string(res))
}

func TestFloat32(t *testing.T) {
	var cf CustomFloat32
	tf := float32(math.MaxFloat32)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%f</tag>", tf)), &cf)
	assert.Nil(t, err)
	assert.Equal(t, tf, cf.Float)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &cf)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, tf, cf.Float)

	// Unmarshal with non-int value
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &cf)
	assert.NotNil(t, err)

	// Unmarshal out of bounds of float32
	err = xml.Unmarshal([]byte(fmt.Sprintf("<tag>1%f</tag>", tf)), &cf)
	assert.NotNil(t, err)

	// Marshal
	cf = CustomFloat32{Float: tf}
	res, err := xml.Marshal(&cf)
	assert.Nil(t, err)
	// This is the exact formatting of the default XML marshaler
	assert.Equal(t, fmt.Sprintf("<CustomFloat32>%s</CustomFloat32>", strconv.FormatFloat(float64(tf), 'g', -1, 32)), string(res))
}

func TestFloat64(t *testing.T) {
	var cf CustomFloat64
	tf := float64(math.MaxFloat64)

	// Unmarshal with a value
	err := xml.Unmarshal([]byte(fmt.Sprintf("<tag>%f</tag>", tf)), &cf)
	assert.Nil(t, err)
	assert.Equal(t, tf, cf.Float)

	// Unmarshal with no value
	err = xml.Unmarshal([]byte("<tag></tag>"), &cf)
	assert.Nil(t, err)
	// It should be unchanged
	assert.Equal(t, tf, cf.Float)

	// Unmarshal with non-int value
	err = xml.Unmarshal([]byte("<tag>string</tag>"), &cf)
	assert.NotNil(t, err)

	// Unmarshal out of bounds of float64
	err = xml.Unmarshal([]byte(fmt.Sprintf("<tag>1%f</tag>", tf)), &cf)
	assert.NotNil(t, err)

	// Marshal
	cf = CustomFloat64{Float: tf}
	res, err := xml.Marshal(&cf)
	assert.Nil(t, err)
	// This is the exact formatting of the default XML marshaler
	assert.Equal(t, fmt.Sprintf("<CustomFloat64>%s</CustomFloat64>", strconv.FormatFloat(tf, 'g', -1, 64)), string(res))
}
