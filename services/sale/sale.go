package sale

import (
	mb "bitbucket.org/canopei/mindbody"
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

// against "unused imports"
var _ xml.Name

type XMLDetailLevel string

const (
	XMLDetailLevelBare XMLDetailLevel = "Bare"

	XMLDetailLevelBasic XMLDetailLevel = "Basic"

	XMLDetailLevelFull XMLDetailLevel = "Full"
)

type StatusCode string

const (
	StatusCodeSuccess StatusCode = "Success"

	StatusCodeInvalidCredentials StatusCode = "InvalidCredentials"

	StatusCodeInvalidParameters StatusCode = "InvalidParameters"

	StatusCodeInternalException StatusCode = "InternalException"

	StatusCodeUnknown StatusCode = "Unknown"

	StatusCodeFailedAction StatusCode = "FailedAction"
)

type ActionCode string

const (
	ActionCodeNone ActionCode = "None"

	ActionCodeAdded ActionCode = "Added"

	ActionCodeUpdated ActionCode = "Updated"

	ActionCodeFailed ActionCode = "Failed"

	ActionCodeRemoved ActionCode = "Removed"
)

type ScheduleType string

const (
	ScheduleTypeAll ScheduleType = "All"

	ScheduleTypeDropIn ScheduleType = "DropIn"

	ScheduleTypeEnrollment ScheduleType = "Enrollment"

	ScheduleTypeAppointment ScheduleType = "Appointment"

	ScheduleTypeResource ScheduleType = "Resource"

	ScheduleTypeMedia ScheduleType = "Media"

	ScheduleTypeArrival ScheduleType = "Arrival"
)

type AppointmentStatus string

const (
	AppointmentStatusBooked AppointmentStatus = "Booked"

	AppointmentStatusCompleted AppointmentStatus = "Completed"

	AppointmentStatusConfirmed AppointmentStatus = "Confirmed"

	AppointmentStatusArrived AppointmentStatus = "Arrived"

	AppointmentStatusNoShow AppointmentStatus = "NoShow"

	AppointmentStatusCancelled AppointmentStatus = "Cancelled"

	AppointmentStatusLateCancelled AppointmentStatus = "LateCancelled"
)

type GetAcceptedCardType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetAcceptedCardType"`

	Request *GetAcceptedCardTypeRequest `xml:"Request,omitempty"`
}

type GetAcceptedCardTypeResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetAcceptedCardTypeResponse"`

	GetAcceptedCardTypeResult *GetAcceptedCardTypeResult `xml:"GetAcceptedCardTypeResult,omitempty"`
}

type CheckoutShoppingCart struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CheckoutShoppingCart"`

	Request *CheckoutShoppingCartRequest `xml:"Request,omitempty"`
}

type CheckoutShoppingCartResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CheckoutShoppingCartResponse"`

	CheckoutShoppingCartResult *CheckoutShoppingCartResult `xml:"CheckoutShoppingCartResult,omitempty"`
}

type GetSales struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSales"`

	Request *GetSalesRequest `xml:"Request,omitempty"`
}

type GetSalesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSalesResponse"`

	GetSalesResult *GetSalesResult `xml:"GetSalesResult,omitempty"`
}

type GetServices struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetServices"`

	Request *GetServicesRequest `xml:"Request,omitempty"`
}

type GetServicesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetServicesResponse"`

	GetServicesResult *GetServicesResult `xml:"GetServicesResult,omitempty"`
}

type UpdateServices struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateServices"`

	Request *UpdateServicesRequest `xml:"Request,omitempty"`
}

type UpdateServicesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateServicesResponse"`

	UpdateServicesResult *UpdateServicesResult `xml:"UpdateServicesResult,omitempty"`
}

type GetPackages struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetPackages"`

	Request *GetPackagesRequest `xml:"Request,omitempty"`
}

type GetPackagesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetPackagesResponse"`

	GetPackagesResult *GetPackagesResult `xml:"GetPackagesResult,omitempty"`
}

type GetProducts struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProducts"`

	Request *GetProductsRequest `xml:"Request,omitempty"`
}

type GetProductsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProductsResponse"`

	GetProductsResult *GetProductsResult `xml:"GetProductsResult,omitempty"`
}

type UpdateProducts struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateProducts"`

	Request *UpdateProductsRequest `xml:"Request,omitempty"`
}

type UpdateProductsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateProductsResponse"`

	UpdateProductsResult *UpdateProductsResult `xml:"UpdateProductsResult,omitempty"`
}

type RedeemSpaFinderWellnessCard struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RedeemSpaFinderWellnessCard"`

	Request *RedeemSpaFinderWellnessCardRequest `xml:"Request,omitempty"`
}

type RedeemSpaFinderWellnessCardResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RedeemSpaFinderWellnessCardResponse"`

	RedeemSpaFinderWellnessCardResult *RedeemSpaFinderWellnessCardResult `xml:"RedeemSpaFinderWellnessCardResult,omitempty"`
}

type GetCustomPaymentMethods struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomPaymentMethods"`

	Request *GetCustomPaymentMethodsRequest `xml:"Request,omitempty"`
}

type GetCustomPaymentMethodsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomPaymentMethodsResponse"`

	GetCustomPaymentMethodsResult *GetCustomPaymentMethodsResult `xml:"GetCustomPaymentMethodsResult,omitempty"`
}

type ReturnSale struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReturnSale"`

	Request *ReturnSaleRequest `xml:"Request,omitempty"`
}

type ReturnSaleResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReturnSaleResponse"`

	ReturnSaleResult *ReturnSaleResult `xml:"ReturnSaleResult,omitempty"`
}

type UpdateSaleDate struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateSaleDate"`

	Request *UpdateSaleDateRequest `xml:"Request,omitempty"`
}

type UpdateSaleDateResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateSaleDateResponse"`

	UpdateSaleDateResult *UpdateSaleDateResult `xml:"UpdateSaleDateResult,omitempty"`
}

type GetAcceptedCardTypeRequest struct {
	*MBRequest
}

type MBRequest struct {
	SourceCredentials *SourceCredentials `xml:"SourceCredentials,omitempty"`
	UserCredentials   *UserCredentials   `xml:"UserCredentials,omitempty"`
	XMLDetail         *XMLDetailLevel    `xml:"XMLDetail,omitempty"`
	PageSize          *mb.CustomInt32    `xml:"PageSize,omitempty"`
	CurrentPageIndex  *mb.CustomInt32    `xml:"CurrentPageIndex,omitempty"`
	Fields            *ArrayOfString     `xml:"Fields,omitempty"`
}

type SourceCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SourceCredentials"`

	SourceName string      `xml:"SourceName,omitempty"`
	Password   string      `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfInt struct {
	Int []*mb.CustomInt32 `xml:"int,omitempty"`
}

type UserCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UserCredentials"`

	Username   string          `xml:"Username,omitempty"`
	Password   string          `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt     `xml:"SiteIDs,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type ArrayOfString struct {
	String []string `xml:"string,omitempty"`
}

type GetAcceptedCardTypeResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetAcceptedCardTypeResult"`

	*MBResult

	CardTypes *ArrayOfString `xml:"CardTypes,omitempty"`
}

type MBResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBResult"`

	Status           *StatusCode     `xml:"Status,omitempty"`
	ErrorCode        *mb.CustomInt32 `xml:"ErrorCode,omitempty"`
	Message          string          `xml:"Message,omitempty"`
	XMLDetail        *XMLDetailLevel `xml:"XMLDetail,omitempty"`
	ResultCount      *mb.CustomInt32 `xml:"ResultCount,omitempty"`
	CurrentPageIndex *mb.CustomInt32 `xml:"CurrentPageIndex,omitempty"`
	TotalPageCount   *mb.CustomInt32 `xml:"TotalPageCount,omitempty"`
}

type CheckoutShoppingCartRequest struct {
	*MBRequest

	CartID        string              `xml:"CartID,omitempty"`
	ClientID      string              `xml:"ClientID,omitempty"`
	Test          bool                `xml:"Test,omitempty"`
	CartItems     *ArrayOfCartItem    `xml:"CartItems,omitempty"`
	InStore       bool                `xml:"InStore,omitempty"`
	PromotionCode string              `xml:"PromotionCode,omitempty"`
	Payments      *ArrayOfPaymentInfo `xml:"Payments,omitempty"`
	SendEmail     bool                `xml:"SendEmail,omitempty"`
	LocationID    *mb.CustomInt32     `xml:"LocationID,omitempty"`
	Image         []byte              `xml:"Image,omitempty"`
	ImageFileName string              `xml:"ImageFileName,omitempty"`
}

type ArrayOfCartItem struct {
	CartItem []*CartItem `xml:"CartItem,omitempty"`
}

type CartItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CartItem"`

	*MBObject

	Item           *Item               `xml:"Item,omitempty"`
	DiscountAmount *mb.CustomFloat64   `xml:"DiscountAmount,omitempty"`
	Appointments   *ArrayOfAppointment `xml:"Appointments,omitempty"`
	EnrollmentIDs  *ArrayOfInt         `xml:"EnrollmentIDs,omitempty"`
	ClassIDs       *ArrayOfInt         `xml:"ClassIDs,omitempty"`
	CourseIDs      *ArrayOfLong        `xml:"CourseIDs,omitempty"`
	VisitIDs       *ArrayOfLong        `xml:"VisitIDs,omitempty"`
	AppointmentIDs *ArrayOfLong        `xml:"AppointmentIDs,omitempty"`
	Action         *ActionCode         `xml:"Action,omitempty"`
	ID             *mb.CustomInt32     `xml:"ID,omitempty"`
	Quantity       *mb.CustomInt32     `xml:"Quantity,omitempty"`
}

type MBObject struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBObject"`
	XSIType string   `xml:"xsi:type,attr,omitempty"`

	Site      *Site          `xml:"Site,omitempty"`
	Messages  *ArrayOfString `xml:"Messages,omitempty"`
	Execute   string         `xml:"Execute,omitempty"`
	ErrorCode string         `xml:"ErrorCode,omitempty"`
}

type Site struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Site"`

	ID                     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name                   string          `xml:"Name,omitempty"`
	Description            string          `xml:"Description,omitempty"`
	LogoURL                string          `xml:"LogoURL,omitempty"`
	PageColor1             string          `xml:"PageColor1,omitempty"`
	PageColor2             string          `xml:"PageColor2,omitempty"`
	PageColor3             string          `xml:"PageColor3,omitempty"`
	PageColor4             string          `xml:"PageColor4,omitempty"`
	AcceptsVisa            bool            `xml:"AcceptsVisa,omitempty"`
	AcceptsDiscover        bool            `xml:"AcceptsDiscover,omitempty"`
	AcceptsMasterCard      bool            `xml:"AcceptsMasterCard,omitempty"`
	AcceptsAmericanExpress bool            `xml:"AcceptsAmericanExpress,omitempty"`
	ContactEmail           string          `xml:"ContactEmail,omitempty"`
	ESA                    bool            `xml:"ESA,omitempty"`
	TotalWOD               bool            `xml:"TotalWOD,omitempty"`
	TaxInclusivePrices     bool            `xml:"TaxInclusivePrices,omitempty"`
	SMSPackageEnabled      bool            `xml:"SMSPackageEnabled,omitempty"`
	AllowsDashboardAccess  bool            `xml:"AllowsDashboardAccess,omitempty"`
	PricingLevel           string          `xml:"PricingLevel,omitempty"`
}

type ClassSchedule struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClassSchedule"`

	*MBObject

	Classes                    *ArrayOfClass     `xml:"Classes,omitempty"`
	Clients                    *ArrayOfClient    `xml:"Clients,omitempty"`
	Course                     *Course           `xml:"Course,omitempty"`
	SemesterID                 *mb.CustomInt32   `xml:"SemesterID,omitempty"`
	IsAvailable                bool              `xml:"IsAvailable,omitempty"`
	Action                     *ActionCode       `xml:"Action,omitempty"`
	ID                         *mb.CustomInt32   `xml:"ID,omitempty"`
	ClassDescription           *ClassDescription `xml:"ClassDescription,omitempty"`
	DaySunday                  bool              `xml:"DaySunday,omitempty"`
	DayMonday                  bool              `xml:"DayMonday,omitempty"`
	DayTuesday                 bool              `xml:"DayTuesday,omitempty"`
	DayWednesday               bool              `xml:"DayWednesday,omitempty"`
	DayThursday                bool              `xml:"DayThursday,omitempty"`
	DayFriday                  bool              `xml:"DayFriday,omitempty"`
	DaySaturday                bool              `xml:"DaySaturday,omitempty"`
	AllowOpenEnrollment        bool              `xml:"AllowOpenEnrollment,omitempty"`
	AllowDateForwardEnrollment bool              `xml:"AllowDateForwardEnrollment,omitempty"`
	StartTime                  *mb.CustomTime    `xml:"StartTime,omitempty"`
	EndTime                    *mb.CustomTime    `xml:"EndTime,omitempty"`
	StartDate                  *mb.CustomTime    `xml:"StartDate,omitempty"`
	EndDate                    *mb.CustomTime    `xml:"EndDate,omitempty"`
	Staff                      *Staff            `xml:"Staff,omitempty"`
	Location                   *Location         `xml:"Location,omitempty"`
}

type ArrayOfClass struct {
	Class []*Class `xml:"Class,omitempty"`
}

type Class struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Class"`

	*MBObject

	ClassScheduleID     *mb.CustomInt32   `xml:"ClassScheduleID,omitempty"`
	Visits              *ArrayOfVisit     `xml:"Visits,omitempty"`
	Clients             *ArrayOfClient    `xml:"Clients,omitempty"`
	Location            *Location         `xml:"Location,omitempty"`
	Resource            *Resource         `xml:"Resource,omitempty"`
	MaxCapacity         *mb.CustomInt32   `xml:"MaxCapacity,omitempty"`
	WebCapacity         *mb.CustomInt32   `xml:"WebCapacity,omitempty"`
	TotalBooked         *mb.CustomInt32   `xml:"TotalBooked,omitempty"`
	TotalBookedWaitlist *mb.CustomInt32   `xml:"TotalBookedWaitlist,omitempty"`
	WebBooked           *mb.CustomInt32   `xml:"WebBooked,omitempty"`
	SemesterID          *mb.CustomInt32   `xml:"SemesterID,omitempty"`
	IsCanceled          bool              `xml:"IsCanceled,omitempty"`
	Substitute          bool              `xml:"Substitute,omitempty"`
	Active              bool              `xml:"Active,omitempty"`
	IsWaitlistAvailable bool              `xml:"IsWaitlistAvailable,omitempty"`
	IsEnrolled          bool              `xml:"IsEnrolled,omitempty"`
	HideCancel          bool              `xml:"HideCancel,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	IsAvailable         bool              `xml:"IsAvailable,omitempty"`
	StartDateTime       *mb.CustomTime    `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime    `xml:"EndDateTime,omitempty"`
	ClassDescription    *ClassDescription `xml:"ClassDescription,omitempty"`
	Staff               *Staff            `xml:"Staff,omitempty"`
}

type ArrayOfVisit struct {
	Visit []*Visit `xml:"Visit,omitempty"`
}

type Visit struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Visit"`

	*MBObject

	ID                          *mb.CustomInt64 `xml:"ID,omitempty"`
	ClassID                     *mb.CustomInt32 `xml:"ClassID,omitempty"`
	AppointmentID               *mb.CustomInt32 `xml:"AppointmentID,omitempty"`
	AppointmentGenderPreference string          `xml:"AppointmentGenderPreference,omitempty"`
	StartDateTime               *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	LateCancelled               bool            `xml:"LateCancelled,omitempty"`
	EndDateTime                 *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Name                        string          `xml:"Name,omitempty"`
	Staff                       *Staff          `xml:"Staff,omitempty"`
	Location                    *Location       `xml:"Location,omitempty"`
	Client                      *Client         `xml:"Client,omitempty"`
	WebSignup                   bool            `xml:"WebSignup,omitempty"`
	Action                      *ActionCode     `xml:"Action,omitempty"`
	SignedIn                    bool            `xml:"SignedIn,omitempty"`
	AppointmentStatus           string          `xml:"AppointmentStatus,omitempty"`
	MakeUp                      bool            `xml:"MakeUp,omitempty"`
	Service                     *ClientService  `xml:"Service,omitempty"`
}

type Staff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Staff"`

	*MBObject

	Appointments             *ArrayOfAppointment      `xml:"Appointments,omitempty"`
	Unavailabilities         *ArrayOfUnavailability   `xml:"Unavailabilities,omitempty"`
	Availabilities           *ArrayOfAvailability     `xml:"Availabilities,omitempty"`
	Email                    string                   `xml:"Email,omitempty"`
	MobilePhone              string                   `xml:"MobilePhone,omitempty"`
	HomePhone                string                   `xml:"HomePhone,omitempty"`
	WorkPhone                string                   `xml:"WorkPhone,omitempty"`
	Address                  string                   `xml:"Address,omitempty"`
	Address2                 string                   `xml:"Address2,omitempty"`
	City                     string                   `xml:"City,omitempty"`
	State                    string                   `xml:"State,omitempty"`
	Country                  string                   `xml:"Country,omitempty"`
	PostalCode               string                   `xml:"PostalCode,omitempty"`
	ForeignZip               string                   `xml:"ForeignZip,omitempty"`
	SortOrder                *mb.CustomInt32          `xml:"SortOrder,omitempty"`
	LoginLocations           *ArrayOfLocation         `xml:"LoginLocations,omitempty"`
	MultiLocation            bool                     `xml:"MultiLocation,omitempty"`
	AppointmentTrn           bool                     `xml:"AppointmentTrn,omitempty"`
	ReservationTrn           bool                     `xml:"ReservationTrn,omitempty"`
	IndependentContractor    bool                     `xml:"IndependentContractor,omitempty"`
	AlwaysAllowDoubleBooking bool                     `xml:"AlwaysAllowDoubleBooking,omitempty"`
	UserAccessLevel          string                   `xml:"UserAccessLevel,omitempty"`
	ProviderIDs              *ArrayOfString           `xml:"ProviderIDs,omitempty"`
	ProviderIDUpdateList     *ArrayOfProviderIDUpdate `xml:"ProviderIDUpdateList,omitempty"`
	Action                   *ActionCode              `xml:"Action,omitempty"`
	ID                       *mb.CustomInt64          `xml:"ID,omitempty"`
	Name                     string                   `xml:"Name,omitempty"`
	FirstName                string                   `xml:"FirstName,omitempty"`
	LastName                 string                   `xml:"LastName,omitempty"`
	ImageURL                 string                   `xml:"ImageURL,omitempty"`
	Bio                      string                   `xml:"Bio,omitempty"`
	IsMale                   bool                     `xml:"isMale,omitempty"`
}

type ArrayOfAppointment struct {
	Appointment []*Appointment `xml:"Appointment,omitempty"`
}

type Appointment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Appointment"`

	*ScheduleItem

	GenderPreference string             `xml:"GenderPreference,omitempty"`
	Duration         *mb.CustomInt32    `xml:"Duration,omitempty"`
	ProviderID       string             `xml:"ProviderID,omitempty"`
	Action           *ActionCode        `xml:"Action,omitempty"`
	ID               *mb.CustomInt64    `xml:"ID,omitempty"`
	Status           *AppointmentStatus `xml:"Status,omitempty"`
	StartDateTime    *mb.CustomTime     `xml:"StartDateTime,omitempty"`
	EndDateTime      *mb.CustomTime     `xml:"EndDateTime,omitempty"`
	Notes            string             `xml:"Notes,omitempty"`
	StaffRequested   bool               `xml:"StaffRequested,omitempty"`
	Program          *Program           `xml:"Program,omitempty"`
	SessionType      *SessionType       `xml:"SessionType,omitempty"`
	Location         *Location          `xml:"Location,omitempty"`
	Staff            *Staff             `xml:"Staff,omitempty"`
	Client           *Client            `xml:"Client,omitempty"`
	FirstAppointment bool               `xml:"FirstAppointment,omitempty"`
	ClientService    *ClientService     `xml:"ClientService,omitempty"`
	Resources        *ArrayOfResource   `xml:"Resources,omitempty"`
}

type ScheduleItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ScheduleItem"`

	*MBObject
}

type Unavailability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Unavailability"`

	*ScheduleItem

	ID            *mb.CustomInt32 `xml:"ID,omitempty"`
	StartDateTime *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime   *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Description   string          `xml:"Description,omitempty"`
}

type Availability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Availability"`

	*ScheduleItem

	ID                  *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff               *Staff          `xml:"Staff,omitempty"`
	SessionType         *SessionType    `xml:"SessionType,omitempty"`
	Programs            *ArrayOfProgram `xml:"Programs,omitempty"`
	StartDateTime       *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	BookableEndDateTime *mb.CustomTime  `xml:"BookableEndDateTime,omitempty"`
	Location            *Location       `xml:"Location,omitempty"`
}

type SessionType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SessionType"`

	*MBObject

	DefaultTimeLength *mb.CustomInt32 `xml:"DefaultTimeLength,omitempty"`
	ProgramID         *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	NumDeducted       *mb.CustomInt32 `xml:"NumDeducted,omitempty"`
	Action            *ActionCode     `xml:"Action,omitempty"`
	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	Name              string          `xml:"Name,omitempty"`
}

type ArrayOfProgram struct {
	Program []*Program `xml:"Program,omitempty"`
}

type Program struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Program"`

	*MBObject

	ID           *mb.CustomInt32 `xml:"ID,omitempty"`
	Name         string          `xml:"Name,omitempty"`
	ScheduleType *ScheduleType   `xml:"ScheduleType,omitempty"`
	CancelOffset *mb.CustomInt32 `xml:"CancelOffset,omitempty"`
}

type Location struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Location"`

	*MBObject

	BusinessID          *mb.CustomInt32   `xml:"BusinessID,omitempty"`
	SiteID              *mb.CustomInt32   `xml:"SiteID,omitempty"`
	BusinessDescription string            `xml:"BusinessDescription,omitempty"`
	AdditionalImageURLs *ArrayOfString    `xml:"AdditionalImageURLs,omitempty"`
	FacilitySquareFeet  *mb.CustomInt32   `xml:"FacilitySquareFeet,omitempty"`
	TreatmentRooms      *mb.CustomInt32   `xml:"TreatmentRooms,omitempty"`
	ProSpaFinderSite    bool              `xml:"ProSpaFinderSite,omitempty"`
	HasClasses          bool              `xml:"HasClasses,omitempty"`
	PhoneExtension      string            `xml:"PhoneExtension,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                string            `xml:"Name,omitempty"`
	Address             string            `xml:"Address,omitempty"`
	Address2            string            `xml:"Address2,omitempty"`
	Tax1                *mb.CustomFloat32 `xml:"Tax1,omitempty"`
	Tax2                *mb.CustomFloat32 `xml:"Tax2,omitempty"`
	Tax3                *mb.CustomFloat32 `xml:"Tax3,omitempty"`
	Tax4                *mb.CustomFloat32 `xml:"Tax4,omitempty"`
	Tax5                *mb.CustomFloat32 `xml:"Tax5,omitempty"`
	Phone               string            `xml:"Phone,omitempty"`
	City                string            `xml:"City,omitempty"`
	StateProvCode       string            `xml:"StateProvCode,omitempty"`
	PostalCode          string            `xml:"PostalCode,omitempty"`
	Latitude            *mb.CustomFloat64 `xml:"Latitude,omitempty"`
	Longitude           *mb.CustomFloat64 `xml:"Longitude,omitempty"`
	DistanceInMiles     *mb.CustomFloat64 `xml:"DistanceInMiles,omitempty"`
	ImageURL            string            `xml:"ImageURL,omitempty"`
	Description         string            `xml:"Description,omitempty"`
	HasSite             bool              `xml:"HasSite,omitempty"`
	CanBook             bool              `xml:"CanBook,omitempty"`
}

type Client struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Client"`

	*MBObject

	NewID                            string                     `xml:"NewID,omitempty"`
	AccountBalance                   *mb.CustomFloat64          `xml:"AccountBalance,omitempty"`
	ClientIndexes                    *ArrayOfClientIndex        `xml:"ClientIndexes,omitempty"`
	Username                         string                     `xml:"Username,omitempty"`
	Password                         string                     `xml:"Password,omitempty"`
	Notes                            string                     `xml:"Notes,omitempty"`
	MobileProvider                   *mb.CustomInt32            `xml:"MobileProvider,omitempty"`
	ClientCreditCard                 *ClientCreditCard          `xml:"ClientCreditCard,omitempty"`
	LastFormulaNotes                 string                     `xml:"LastFormulaNotes,omitempty"`
	AppointmentGenderPreference      string                     `xml:"AppointmentGenderPreference,omitempty"`
	Gender                           string                     `xml:"Gender,omitempty"`
	IsCompany                        bool                       `xml:"IsCompany,omitempty"`
	Inactive                         bool                       `xml:"Inactive,omitempty"`
	ClientRelationships              *ArrayOfClientRelationship `xml:"ClientRelationships,omitempty"`
	Reps                             *ArrayOfRep                `xml:"Reps,omitempty"`
	SaleReps                         *ArrayOfSalesRep           `xml:"SaleReps,omitempty"`
	CustomClientFields               *ArrayOfCustomClientField  `xml:"CustomClientFields,omitempty"`
	LiabilityRelease                 bool                       `xml:"LiabilityRelease,omitempty"`
	EmergencyContactInfoName         string                     `xml:"EmergencyContactInfoName,omitempty"`
	EmergencyContactInfoRelationship string                     `xml:"EmergencyContactInfoRelationship,omitempty"`
	EmergencyContactInfoPhone        string                     `xml:"EmergencyContactInfoPhone,omitempty"`
	EmergencyContactInfoEmail        string                     `xml:"EmergencyContactInfoEmail,omitempty"`
	PromotionalEmailOptIn            bool                       `xml:"PromotionalEmailOptIn,omitempty"`
	CreationDate                     *mb.CustomTime             `xml:"CreationDate,omitempty"`
	Liability                        *Liability                 `xml:"Liability,omitempty"`
	ProspectStage                    *ProspectStage             `xml:"ProspectStage,omitempty"`
	UniqueID                         *mb.CustomInt64            `xml:"UniqueID,omitempty"`
	MembershipIcon                   *mb.CustomInt32            `xml:"MembershipIcon,omitempty"`
	Action                           *ActionCode                `xml:"Action,omitempty"`
	ID                               string                     `xml:"ID,omitempty"`
	FirstName                        string                     `xml:"FirstName,omitempty"`
	MiddleName                       string                     `xml:"MiddleName,omitempty"`
	LastName                         string                     `xml:"LastName,omitempty"`
	Email                            string                     `xml:"Email,omitempty"`
	EmailOptIn                       bool                       `xml:"EmailOptIn,omitempty"`
	AddressLine1                     string                     `xml:"AddressLine1,omitempty"`
	AddressLine2                     string                     `xml:"AddressLine2,omitempty"`
	City                             string                     `xml:"City,omitempty"`
	State                            string                     `xml:"State,omitempty"`
	PostalCode                       string                     `xml:"PostalCode,omitempty"`
	Country                          string                     `xml:"Country,omitempty"`
	MobilePhone                      string                     `xml:"MobilePhone,omitempty"`
	HomePhone                        string                     `xml:"HomePhone,omitempty"`
	WorkPhone                        string                     `xml:"WorkPhone,omitempty"`
	WorkExtension                    string                     `xml:"WorkExtension,omitempty"`
	BirthDate                        *mb.CustomTime             `xml:"BirthDate,omitempty"`
	FirstAppointmentDate             *mb.CustomTime             `xml:"FirstAppointmentDate,omitempty"`
	ReferredBy                       string                     `xml:"ReferredBy,omitempty"`
	HomeLocation                     *HomeLocation              `xml:"HomeLocation,omitempty"`
	YellowAlert                      string                     `xml:"YellowAlert,omitempty"`
	RedAlert                         string                     `xml:"RedAlert,omitempty"`
	PhotoURL                         string                     `xml:"PhotoURL,omitempty"`
	IsProspect                       bool                       `xml:"IsProspect,omitempty"`
	Status                           string                     `xml:"Status,omitempty"`
	ContactMethod                    *mb.CustomInt16            `xml:"ContactMethod,omitempty"`
}

type ArrayOfClientIndex struct {
	ClientIndex []*ClientIndex `xml:"ClientIndex,omitempty"`
}

type ClientIndex struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndex"`

	*MBObject

	RequiredBusinessMode bool                     `xml:"RequiredBusinessMode,omitempty"`
	RequiredConsumerMode bool                     `xml:"RequiredConsumerMode,omitempty"`
	Action               *ActionCode              `xml:"Action,omitempty"`
	ID                   *mb.CustomInt32          `xml:"ID,omitempty"`
	Name                 string                   `xml:"Name,omitempty"`
	Values               *ArrayOfClientIndexValue `xml:"Values,omitempty"`
}

type ArrayOfClientIndexValue struct {
	ClientIndexValue []*ClientIndexValue `xml:"ClientIndexValue,omitempty"`
}

type ClientIndexValue struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndexValue"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
	Active bool            `xml:"Active,omitempty"`
}

type ClientCreditCard struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientCreditCard"`

	CardType   string `xml:"CardType,omitempty"`
	LastFour   string `xml:"LastFour,omitempty"`
	CardNumber string `xml:"CardNumber,omitempty"`
	CardHolder string `xml:"CardHolder,omitempty"`
	ExpMonth   string `xml:"ExpMonth,omitempty"`
	ExpYear    string `xml:"ExpYear,omitempty"`
	Address    string `xml:"Address,omitempty"`
	City       string `xml:"City,omitempty"`
	State      string `xml:"State,omitempty"`
	PostalCode string `xml:"PostalCode,omitempty"`
}

type ArrayOfClientRelationship struct {
	ClientRelationship []*ClientRelationship `xml:"ClientRelationship,omitempty"`
}

type ClientRelationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientRelationship"`

	*MBObject

	RelatedClient    *Client       `xml:"RelatedClient,omitempty"`
	Relationship     *Relationship `xml:"Relationship,omitempty"`
	RelationshipName string        `xml:"RelationshipName,omitempty"`
}

type Relationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Relationship"`

	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	RelationshipName1 string          `xml:"RelationshipName1,omitempty"`
	RelationshipName2 string          `xml:"RelationshipName2,omitempty"`
}

type ArrayOfRep struct {
	Rep []*Rep `xml:"Rep,omitempty"`
}

type Rep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Rep"`

	*MBObject

	ID    *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff *Staff          `xml:"Staff,omitempty"`
}

type ArrayOfSalesRep struct {
	SalesRep []*SalesRep `xml:"SalesRep,omitempty"`
}

type SalesRep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SalesRep"`

	*MBObject

	SalesRepNumber  *mb.CustomInt32 `xml:"SalesRepNumber,omitempty"`
	ID              *mb.CustomInt64 `xml:"ID,omitempty"`
	FirstName       string          `xml:"FirstName,omitempty"`
	LastName        string          `xml:"LastName,omitempty"`
	SalesRepNumbers *ArrayOfInt     `xml:"SalesRepNumbers,omitempty"`
}

type ArrayOfCustomClientField struct {
	CustomClientField []*CustomClientField `xml:"CustomClientField,omitempty"`
}

type CustomClientField struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CustomClientField"`

	ID       *mb.CustomInt32 `xml:"ID,omitempty"`
	DataType string          `xml:"DataType,omitempty"`
	Name     string          `xml:"Name,omitempty"`
	Value    string          `xml:"Value,omitempty"`
}

type Liability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Liability"`

	IsReleased    bool            `xml:"IsReleased,omitempty"`
	AgreementDate *mb.CustomTime  `xml:"AgreementDate,omitempty"`
	ReleasedBy    *mb.CustomInt64 `xml:"ReleasedBy,omitempty"`
}

type ProspectStage struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProspectStage"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type ClientService struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientService"`

	*MBObject

	Current        bool            `xml:"Current,omitempty"`
	Count          *mb.CustomInt32 `xml:"Count,omitempty"`
	Remaining      *mb.CustomInt32 `xml:"Remaining,omitempty"`
	Action         *ActionCode     `xml:"Action,omitempty"`
	ID             *mb.CustomInt64 `xml:"ID,omitempty"`
	Name           string          `xml:"Name,omitempty"`
	PaymentDate    *mb.CustomTime  `xml:"PaymentDate,omitempty"`
	ActiveDate     *mb.CustomTime  `xml:"ActiveDate,omitempty"`
	ExpirationDate *mb.CustomTime  `xml:"ExpirationDate,omitempty"`
	Program        *Program        `xml:"Program,omitempty"`
}

type ArrayOfResource struct {
	Resource []*Resource `xml:"Resource,omitempty"`
}

type Resource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Resource"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type ArrayOfUnavailability struct {
	Unavailability []*Unavailability `xml:"Unavailability,omitempty"`
}

type ArrayOfAvailability struct {
	Availability []*Availability `xml:"Availability,omitempty"`
}

type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty"`
}

type ArrayOfProviderIDUpdate struct {
	ProviderIDUpdate []*ProviderIDUpdate `xml:"ProviderIDUpdate,omitempty"`
}

type ProviderIDUpdate struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProviderIDUpdate"`

	OldProviderID string `xml:"OldProviderID,omitempty"`
	NewProviderID string `xml:"NewProviderID,omitempty"`
}

type ArrayOfClient struct {
	Client []*Client `xml:"Client,omitempty"`
}

type ClassDescription struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClassDescription"`

	*MBObject

	ImageURL    string          `xml:"ImageURL,omitempty"`
	Level       *Level          `xml:"Level,omitempty"`
	Action      *ActionCode     `xml:"Action,omitempty"`
	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Prereq      string          `xml:"Prereq,omitempty"`
	Notes       string          `xml:"Notes,omitempty"`
	LastUpdated *mb.CustomTime  `xml:"LastUpdated,omitempty"`
	Program     *Program        `xml:"Program,omitempty"`
	SessionType *SessionType    `xml:"SessionType,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type Level struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Level"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
}

type Course struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Course"`

	ID          *mb.CustomInt64 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Notes       string          `xml:"Notes,omitempty"`
	StartDate   *mb.CustomTime  `xml:"StartDate,omitempty"`
	EndDate     *mb.CustomTime  `xml:"EndDate,omitempty"`
	Location    *Location       `xml:"Location,omitempty"`
	Organizer   *Staff          `xml:"Organizer,omitempty"`
	Program     *Program        `xml:"Program,omitempty"`
	ImageURL    string          `xml:"ImageURL,omitempty"`
}

type ShoppingCart struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ShoppingCart"`

	*MBObject

	AuthCode      string            `xml:"AuthCode,omitempty"`
	Action        *ActionCode       `xml:"Action,omitempty"`
	ID            string            `xml:"ID,omitempty"`
	CartItems     *ArrayOfCartItem  `xml:"CartItems,omitempty"`
	SubTotal      *mb.CustomFloat64 `xml:"SubTotal,omitempty"`
	DiscountTotal *mb.CustomFloat64 `xml:"DiscountTotal,omitempty"`
	TaxTotal      *mb.CustomFloat64 `xml:"TaxTotal,omitempty"`
	GrandTotal    *mb.CustomFloat64 `xml:"GrandTotal,omitempty"`
}

type Size struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Size"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type Color struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Color"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type Item struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Item"`
	ID      string   `xml:"ID,omitempty"`

	*MBObject
}

type Tip struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Tip"`

	*Item

	Amount  *mb.CustomFloat64 `xml:"Amount,omitempty"`
	StaffID *mb.CustomInt64   `xml:"StaffID,omitempty"`
}

type Package struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Package"`

	*Item

	ID                 *mb.CustomInt32   `xml:"ID,omitempty"`
	Name               string            `xml:"Name,omitempty"`
	DiscountPercentage *mb.CustomFloat64 `xml:"DiscountPercentage,omitempty"`
	SellOnline         bool              `xml:"SellOnline,omitempty"`
	Services           *ArrayOfService   `xml:"Services,omitempty"`
	Products           *ArrayOfProduct   `xml:"Products,omitempty"`
}

type ArrayOfService struct {
	Service []*Service `xml:"Service,omitempty"`
}

type Service struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Service"`

	*Item

	Price       *mb.CustomFloat64 `xml:"Price,omitempty"`
	OnlinePrice *mb.CustomFloat64 `xml:"OnlinePrice,omitempty"`
	TaxIncluded *mb.CustomFloat64 `xml:"TaxIncluded,omitempty"`
	ProgramID   *mb.CustomInt32   `xml:"ProgramID,omitempty"`
	TaxRate     *mb.CustomFloat64 `xml:"TaxRate,omitempty"`
	ProductID   *mb.CustomFloat64 `xml:"ProductID,omitempty"`
	Action      *ActionCode       `xml:"Action,omitempty"`
	ID          string            `xml:"ID,omitempty"`
	Name        string            `xml:"Name,omitempty"`
	Count       *mb.CustomInt32   `xml:"Count,omitempty"`
}

type ArrayOfProduct struct {
	Product []*Product `xml:"Product,omitempty"`
}

type Product struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Product"`

	*Item

	Price       *mb.CustomFloat64 `xml:"Price,omitempty"`
	TaxIncluded *mb.CustomFloat64 `xml:"TaxIncluded,omitempty"`
	TaxRate     *mb.CustomFloat64 `xml:"TaxRate,omitempty"`
	Action      *ActionCode       `xml:"Action,omitempty"`
	ID          string            `xml:"ID,omitempty"`
	GroupID     *mb.CustomInt32   `xml:"GroupID,omitempty"`
	Name        string            `xml:"Name,omitempty"`
	OnlinePrice *mb.CustomFloat64 `xml:"OnlinePrice,omitempty"`
	ShortDesc   string            `xml:"ShortDesc,omitempty"`
	LongDesc    string            `xml:"LongDesc,omitempty"`
	Color       *Color            `xml:"Color,omitempty"`
	Size        *Size             `xml:"Size,omitempty"`
}

type ArrayOfLong struct {
	Long []*mb.CustomInt64 `xml:"long,omitempty"`
}

type ArrayOfPaymentInfo struct {
	PaymentInfo []*PaymentInfo `xml:"PaymentInfo,omitempty"`
}

type PaymentInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 PaymentInfo"`
	*MBObject
	ID     *mb.CustomInt32   `xml:"ID,omitempty"`
	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`

	Name string `xml:"Name,omitempty"`
}

type GiftCardInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GiftCardInfo"`

	*PaymentInfo

	Amount     *mb.CustomFloat64 `xml:"Amount,omitempty"`
	Notes      string            `xml:"Notes,omitempty"`
	CardNumber string            `xml:"CardNumber,omitempty"`
}

type CheckInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CheckInfo"`

	*PaymentInfo

	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`
	Notes  string            `xml:"Notes,omitempty"`
}

type CashInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CashInfo"`

	*PaymentInfo

	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`
	Notes  string            `xml:"Notes,omitempty"`
}

type CompInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CompInfo"`

	*PaymentInfo

	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`
}

type TrackDataInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 TrackDataInfo"`

	*PaymentInfo

	Amount    *mb.CustomFloat64 `xml:"Amount,omitempty"`
	TrackData string            `xml:"TrackData,omitempty"`
}

type StoredCardInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 StoredCardInfo"`

	*PaymentInfo

	Amount   *mb.CustomFloat64 `xml:"Amount,omitempty"`
	LastFour string            `xml:"LastFour,omitempty"`
}

type EncryptedTrackDataInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 EncryptedTrackDataInfo"`

	*PaymentInfo

	Amount    *mb.CustomFloat64 `xml:"Amount,omitempty"`
	TrackData string            `xml:"TrackData,omitempty"`
}

type CustomPaymentInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CustomPaymentInfo"`

	*PaymentInfo

	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`
	ID     *mb.CustomInt32   `xml:"ID,omitempty"`
}

type DebitAccountInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 DebitAccountInfo"`

	*PaymentInfo

	Amount *mb.CustomFloat64 `xml:"Amount,omitempty"`
}

type CreditCardInfo struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CreditCardInfo"`

	*PaymentInfo

	CVV               string            `xml:"CVV,omitempty"`
	Action            *ActionCode       `xml:"Action,omitempty"`
	CreditCardNumber  string            `xml:"CreditCardNumber,omitempty"`
	Amount            *mb.CustomFloat64 `xml:"Amount,omitempty"`
	ExpMonth          string            `xml:"ExpMonth,omitempty"`
	ExpYear           string            `xml:"ExpYear,omitempty"`
	BillingName       string            `xml:"BillingName,omitempty"`
	BillingAddress    string            `xml:"BillingAddress,omitempty"`
	BillingCity       string            `xml:"BillingCity,omitempty"`
	BillingState      string            `xml:"BillingState,omitempty"`
	BillingPostalCode string            `xml:"BillingPostalCode,omitempty"`
	SaveInfo          bool              `xml:"SaveInfo,omitempty"`
}

type CheckoutShoppingCartResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CheckoutShoppingCartResult"`

	*MBResult

	ShoppingCart *ShoppingCart         `xml:"ShoppingCart,omitempty"`
	Classes      *ArrayOfClass         `xml:"Classes,omitempty"`
	Appointments *ArrayOfAppointment   `xml:"Appointments,omitempty"`
	Enrollments  *ArrayOfClassSchedule `xml:"Enrollments,omitempty"`
}

type ArrayOfClassSchedule struct {
	ClassSchedule []*ClassSchedule `xml:"ClassSchedule,omitempty"`
}

type GetSalesRequest struct {
	*MBRequest

	SaleID            *mb.CustomInt64 `xml:"SaleID,omitempty"`
	StartSaleDateTime *mb.CustomTime  `xml:"StartSaleDateTime,omitempty"`
	EndSaleDateTime   *mb.CustomTime  `xml:"EndSaleDateTime,omitempty"`
	PaymentMethodID   *mb.CustomInt32 `xml:"PaymentMethodID,omitempty"`
}

type GetSalesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSalesResult"`

	*MBResult

	Sales *ArrayOfSale `xml:"Sales,omitempty"`
}

type ArrayOfSale struct {
	Sale []*Sale `xml:"Sale,omitempty"`
}

type Sale struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Sale"`

	ID           *mb.CustomInt64 `xml:"ID,omitempty"`
	SaleTime     *mb.CustomTime  `xml:"SaleTime,omitempty"`
	SaleDate     *mb.CustomTime  `xml:"SaleDate,omitempty"`
	SaleDateTime *mb.CustomTime  `xml:"SaleDateTime,omitempty"`
	Location     *Location       `xml:"Location,omitempty"`
	Payments     *ArrayOfPayment `xml:"Payments,omitempty"`
}

type ArrayOfPayment struct {
	Payment []*Payment `xml:"Payment,omitempty"`
}

type Payment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Payment"`

	ID       *mb.CustomInt64   `xml:"ID,omitempty"`
	Amount   *mb.CustomFloat64 `xml:"Amount,omitempty"`
	Method   *mb.CustomInt32   `xml:"Method,omitempty"`
	Type     string            `xml:"Type,omitempty"`
	LastFour string            `xml:"LastFour,omitempty"`
	Notes    string            `xml:"Notes,omitempty"`
}

type GetServicesRequest struct {
	*MBRequest

	ProgramIDs          *ArrayOfInt     `xml:"ProgramIDs,omitempty"`
	SessionTypeIDs      *ArrayOfInt     `xml:"SessionTypeIDs,omitempty"`
	ServiceIDs          *ArrayOfString  `xml:"ServiceIDs,omitempty"`
	ClassID             *mb.CustomInt32 `xml:"ClassID,omitempty"`
	ClassScheduleID     *mb.CustomInt32 `xml:"ClassScheduleID,omitempty"`
	SellOnline          bool            `xml:"SellOnline,omitempty"`
	LocationID          *mb.CustomInt32 `xml:"LocationID,omitempty"`
	HideRelatedPrograms bool            `xml:"HideRelatedPrograms,omitempty"`
	StaffID             *mb.CustomInt64 `xml:"StaffID,omitempty"`
}

type GetServicesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetServicesResult"`

	*MBResult

	Services *ArrayOfService `xml:"Services,omitempty"`
}

type UpdateServicesRequest struct {
	*MBRequest

	Services *ArrayOfService `xml:"Services,omitempty"`
	Test     bool            `xml:"Test,omitempty"`
}

type UpdateServicesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateServicesResult"`

	*MBResult

	Services *ArrayOfService `xml:"Services,omitempty"`
}

type GetPackagesRequest struct {
	*MBRequest

	PackageIDs *ArrayOfInt `xml:"PackageIDs,omitempty"`
	SellOnline bool        `xml:"SellOnline,omitempty"`
}

type GetPackagesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetPackagesResult"`

	*MBResult

	Packages *ArrayOfPackage `xml:"Packages,omitempty"`
}

type ArrayOfPackage struct {
	Package []*Package `xml:"Package,omitempty"`
}

type GetProductsRequest struct {
	*MBRequest

	ProductIDs     *ArrayOfString  `xml:"ProductIDs,omitempty"`
	SearchText     string          `xml:"SearchText,omitempty"`
	SearchDomain   string          `xml:"SearchDomain,omitempty"`
	CategoryIDs    *ArrayOfInt     `xml:"CategoryIDs,omitempty"`
	SubCategoryIDs *ArrayOfInt     `xml:"SubCategoryIDs,omitempty"`
	SellOnline     bool            `xml:"SellOnline,omitempty"`
	LocationID     *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type GetProductsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProductsResult"`

	*MBResult

	Products *ArrayOfProduct `xml:"Products,omitempty"`
}

type UpdateProductsRequest struct {
	*MBRequest

	Products *ArrayOfProduct `xml:"Products,omitempty"`
	Test     bool            `xml:"Test,omitempty"`
}

type UpdateProductsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateProductsResult"`

	*MBResult

	Products *ArrayOfProduct `xml:"Products,omitempty"`
}

type RedeemSpaFinderWellnessCardRequest struct {
	*MBRequest

	CardID     string            `xml:"CardID,omitempty"`
	FaceAmount *mb.CustomFloat64 `xml:"FaceAmount,omitempty"`
	Currency   string            `xml:"Currency,omitempty"`
	ClientID   string            `xml:"ClientID,omitempty"`
	LocationID *mb.CustomInt32   `xml:"LocationID,omitempty"`
}

type RedeemSpaFinderWellnessCardResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RedeemSpaFinderWellnessCardResult"`

	*MBResult
}

type GetCustomPaymentMethodsRequest struct {
	*MBRequest
}

type GetCustomPaymentMethodsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomPaymentMethodsResult"`

	*MBResult

	PaymentMethods *ArrayOfCustomPaymentInfo `xml:"PaymentMethods,omitempty"`
}

type ArrayOfCustomPaymentInfo struct {
	CustomPaymentInfo []*CustomPaymentInfo `xml:"CustomPaymentInfo,omitempty"`
}

type ReturnSaleRequest struct {
	*MBRequest

	Test         bool            `xml:"Test,omitempty"`
	SaleID       *mb.CustomInt64 `xml:"SaleID,omitempty"`
	ReturnReason string          `xml:"ReturnReason,omitempty"`
}

type ReturnSaleResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReturnSaleResult"`

	*MBResult

	ReturnSaleID *mb.CustomInt64   `xml:"ReturnSaleID,omitempty"`
	TrainerID    *mb.CustomInt64   `xml:"TrainerID,omitempty"`
	Amount       *mb.CustomFloat64 `xml:"Amount,omitempty"`
}

type UpdateSaleDateRequest struct {
	*MBRequest

	SaleID   *mb.CustomInt64 `xml:"SaleID,omitempty"`
	SaleDate *mb.CustomTime  `xml:"SaleDate,omitempty"`
}

type UpdateSaleDateResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateSaleDateResult"`

	*MBResult

	Sale *Sale `xml:"Sale,omitempty"`
}

type Sale_x0020_ServiceSoap struct {
	client *SOAPClient
}

func NewSale_x0020_ServiceSoap(url string, tls bool, auth *BasicAuth) *Sale_x0020_ServiceSoap {
	if url == "" {
		url = "https://api.mindbodyonline.com/0_5/SaleService.asmx"
	}
	client := NewSOAPClient(url, tls, auth)

	return &Sale_x0020_ServiceSoap{
		client: client,
	}
}

/* Gets a list of card types that the site accepts. */
func (service *Sale_x0020_ServiceSoap) GetAcceptedCardType(request *GetAcceptedCardType) (*GetAcceptedCardTypeResponse, error) {
	response := new(GetAcceptedCardTypeResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetAcceptedCardType", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Validates and completes a sale by processing all items added to a shopping cart. */
func (service *Sale_x0020_ServiceSoap) CheckoutShoppingCart(request *CheckoutShoppingCart) (*CheckoutShoppingCartResponse, error) {
	response := new(CheckoutShoppingCartResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/CheckoutShoppingCart", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of sales. */
func (service *Sale_x0020_ServiceSoap) GetSales(request *GetSales) (*GetSalesResponse, error) {
	response := new(GetSalesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetSales", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of services available for sale. */
func (service *Sale_x0020_ServiceSoap) GetServices(request *GetServices) (*GetServicesResponse, error) {
	response := new(GetServicesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetServices", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Update select services information. */
func (service *Sale_x0020_ServiceSoap) UpdateServices(request *UpdateServices) (*UpdateServicesResponse, error) {
	response := new(UpdateServicesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UpdateServices", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of packages available for sale. */
func (service *Sale_x0020_ServiceSoap) GetPackages(request *GetPackages) (*GetPackagesResponse, error) {
	response := new(GetPackagesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetPackages", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get a list of products available for sale. */
func (service *Sale_x0020_ServiceSoap) GetProducts(request *GetProducts) (*GetProductsResponse, error) {
	response := new(GetProductsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetProducts", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Update select products information. */
func (service *Sale_x0020_ServiceSoap) UpdateProducts(request *UpdateProducts) (*UpdateProductsResponse, error) {
	response := new(UpdateProductsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UpdateProducts", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Redeem a Spa Finder Gift Card. */
func (service *Sale_x0020_ServiceSoap) RedeemSpaFinderWellnessCard(request *RedeemSpaFinderWellnessCard) (*RedeemSpaFinderWellnessCardResponse, error) {
	response := new(RedeemSpaFinderWellnessCardResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/RedeemSpaFinderWellnessCard", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of Custom Payment Methods. */
func (service *Sale_x0020_ServiceSoap) GetCustomPaymentMethods(request *GetCustomPaymentMethods) (*GetCustomPaymentMethodsResponse, error) {
	response := new(GetCustomPaymentMethodsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetCustomPaymentMethods", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Return a sale used in business mode. This only supports comp payment method. */
func (service *Sale_x0020_ServiceSoap) ReturnSale(request *ReturnSale) (*ReturnSaleResponse, error) {
	response := new(ReturnSaleResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/ReturnSale", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Modify sale date in business mode */
func (service *Sale_x0020_ServiceSoap) UpdateSaleDate(request *UpdateSaleDate) (*UpdateSaleDateResponse, error) {
	response := new(UpdateSaleDateResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UpdateSaleDate", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	XSINS   string   `xml:"xmlns:xsi,attr,omitempty"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
		XSINS: "http://www.w3.org/2001/XMLSchema-instance",
		//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Println(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Println("empty response")
		return nil
	}

	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}

type HomeLocation struct {
	*Location

	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 HomeLocation"`
}
