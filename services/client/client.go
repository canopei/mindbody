package client

import (
	mb "bitbucket.org/canopei/mindbody"
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

// against "unused imports"
var _ xml.Name

type XMLDetailLevel string

const (
	XMLDetailLevelBare XMLDetailLevel = "Bare"

	XMLDetailLevelBasic XMLDetailLevel = "Basic"

	XMLDetailLevelFull XMLDetailLevel = "Full"
)

type StatusCode string

const (
	StatusCodeSuccess StatusCode = "Success"

	StatusCodeInvalidCredentials StatusCode = "InvalidCredentials"

	StatusCodeInvalidParameters StatusCode = "InvalidParameters"

	StatusCodeInternalException StatusCode = "InternalException"

	StatusCodeUnknown StatusCode = "Unknown"

	StatusCodeFailedAction StatusCode = "FailedAction"
)

type ScheduleType string

const (
	ScheduleTypeAll ScheduleType = "All"

	ScheduleTypeDropIn ScheduleType = "DropIn"

	ScheduleTypeEnrollment ScheduleType = "Enrollment"

	ScheduleTypeAppointment ScheduleType = "Appointment"

	ScheduleTypeResource ScheduleType = "Resource"

	ScheduleTypeMedia ScheduleType = "Media"

	ScheduleTypeArrival ScheduleType = "Arrival"
)

type ActionCode string

const (
	ActionCodeNone ActionCode = "None"

	ActionCodeAdded ActionCode = "Added"

	ActionCodeUpdated ActionCode = "Updated"

	ActionCodeFailed ActionCode = "Failed"

	ActionCodeRemoved ActionCode = "Removed"
)

type AppointmentStatus string

const (
	AppointmentStatusBooked AppointmentStatus = "Booked"

	AppointmentStatusCompleted AppointmentStatus = "Completed"

	AppointmentStatusConfirmed AppointmentStatus = "Confirmed"

	AppointmentStatusArrived AppointmentStatus = "Arrived"

	AppointmentStatusNoShow AppointmentStatus = "NoShow"

	AppointmentStatusCancelled AppointmentStatus = "Cancelled"

	AppointmentStatusLateCancelled AppointmentStatus = "LateCancelled"
)

type AddArrival struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddArrival"`

	Request *AddArrivalRequest `xml:"Request,omitempty"`
}

type AddArrivalResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddArrivalResponse"`

	AddArrivalResult *AddArrivalResult `xml:"AddArrivalResult,omitempty"`
}

type AddOrUpdateClients struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateClients"`

	Request *AddOrUpdateClientsRequest `xml:"Request,omitempty"`
}

type AddOrUpdateClientsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateClientsResponse"`

	AddOrUpdateClientsResult *AddOrUpdateClientsResult `xml:"AddOrUpdateClientsResult,omitempty"`
}

type GetClients struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClients"`

	Request *GetClientsRequest `xml:"Request,omitempty"`
}

type GetClientsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientsResponse"`

	GetClientsResult *GetClientsResult `xml:"GetClientsResult,omitempty"`
}

type GetCustomClientFields struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomClientFields"`

	Request *GetCustomClientFieldsRequest `xml:"Request,omitempty"`
}

type GetCustomClientFieldsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomClientFieldsResponse"`

	GetCustomClientFieldsResult *GetCustomClientFieldsResult `xml:"GetCustomClientFieldsResult,omitempty"`
}

type GetClientIndexes struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientIndexes"`

	Request *GetClientIndexesRequest `xml:"Request,omitempty"`
}

type GetClientIndexesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientIndexesResponse"`

	GetClientIndexesResult *GetClientIndexesResult `xml:"GetClientIndexesResult,omitempty"`
}

type GetClientContactLogs struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContactLogs"`

	Request *GetClientContactLogsRequest `xml:"Request,omitempty"`
}

type GetClientContactLogsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContactLogsResponse"`

	GetClientContactLogsResult *GetClientContactLogsResult `xml:"GetClientContactLogsResult,omitempty"`
}

type AddOrUpdateContactLogs struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateContactLogs"`

	Request *AddOrUpdateContactLogsRequest `xml:"Request,omitempty"`
}

type AddOrUpdateContactLogsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateContactLogsResponse"`

	AddOrUpdateContactLogsResult *AddOrUpdateContactLogsResult `xml:"AddOrUpdateContactLogsResult,omitempty"`
}

type GetContactLogTypes struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetContactLogTypes"`

	Request *GetContactLogTypesRequest `xml:"Request,omitempty"`
}

type GetContactLogTypesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetContactLogTypesResponse"`

	GetContactLogTypesResult *GetContactLogTypesResult `xml:"GetContactLogTypesResult,omitempty"`
}

type UploadClientDocument struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientDocument"`

	Request *UploadClientDocumentRequest `xml:"Request,omitempty"`
}

type UploadClientDocumentResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientDocumentResponse"`

	UploadClientDocumentResult *UploadClientDocumentResult `xml:"UploadClientDocumentResult,omitempty"`
}

type UploadClientPhoto struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientPhoto"`

	Request *UploadClientPhotoRequest `xml:"Request,omitempty"`
}

type UploadClientPhotoResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientPhotoResponse"`

	UploadClientPhotoResult *UploadClientPhotoResult `xml:"UploadClientPhotoResult,omitempty"`
}

type GetClientFormulaNotes struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientFormulaNotes"`

	Request *GetClientFormulaNotesRequest `xml:"Request,omitempty"`
}

type GetClientFormulaNotesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientFormulaNotesResponse"`

	GetClientFormulaNotesResult *GetClientFormulaNotesResult `xml:"GetClientFormulaNotesResult,omitempty"`
}

type AddClientFormulaNote struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientFormulaNote"`

	Request *AddClientFormulaNoteRequest `xml:"Request,omitempty"`
}

type AddClientFormulaNoteResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientFormulaNoteResponse"`

	AddClientFormulaNoteResult *AddClientFormulaNoteResult `xml:"AddClientFormulaNoteResult,omitempty"`
}

type DeleteClientFormulaNote struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 DeleteClientFormulaNote"`

	Request *DeleteCientFormulaNoteRequest `xml:"Request,omitempty"`
}

type DeleteClientFormulaNoteResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 DeleteClientFormulaNoteResponse"`

	DeleteClientFormulaNoteResult *DeleteClientFormulaNoteResult `xml:"DeleteClientFormulaNoteResult,omitempty"`
}

type GetClientReferralTypes struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientReferralTypes"`

	Request *GetClientReferralTypesRequest `xml:"Request,omitempty"`
}

type GetClientReferralTypesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientReferralTypesResponse"`

	GetClientReferralTypesResult *GetClientReferralTypesResult `xml:"GetClientReferralTypesResult,omitempty"`
}

type GetActiveClientMemberships struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActiveClientMemberships"`

	Request *GetActiveClientMembershipsRequest `xml:"Request,omitempty"`
}

type GetActiveClientMembershipsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActiveClientMembershipsResponse"`

	GetActiveClientMembershipsResult *GetActiveClientMembershipsResult `xml:"GetActiveClientMembershipsResult,omitempty"`
}

type GetClientContracts struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContracts"`

	Request *GetClientContractsRequest `xml:"Request,omitempty"`
}

type GetClientContractsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContractsResponse"`

	GetClientContractsResult *GetClientContractsResult `xml:"GetClientContractsResult,omitempty"`
}

type GetClientAccountBalances struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientAccountBalances"`

	Request *GetClientAccountBalancesRequest `xml:"Request,omitempty"`
}

type GetClientAccountBalancesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientAccountBalancesResponse"`

	GetClientAccountBalancesResult *GetClientAccountBalancesResult `xml:"GetClientAccountBalancesResult,omitempty"`
}

type GetClientServices struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientServices"`

	Request *GetClientServicesRequest `xml:"Request,omitempty"`
}

type GetClientServicesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientServicesResponse"`

	GetClientServicesResult *GetClientServicesResult `xml:"GetClientServicesResult,omitempty"`
}

type GetClientVisits struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientVisits"`

	Request *GetClientVisitsRequest `xml:"Request,omitempty"`
}

type GetClientVisitsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientVisitsResponse"`

	GetClientVisitsResult *GetClientVisitsResult `xml:"GetClientVisitsResult,omitempty"`
}

type GetClientPurchases struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientPurchases"`

	Request *GetClientPurchasesRequest `xml:"Request,omitempty"`
}

type GetClientPurchasesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientPurchasesResponse"`

	GetClientPurchasesResult *GetClientPurchasesResult `xml:"GetClientPurchasesResult,omitempty"`
}

type GetClientSchedule struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientSchedule"`

	Request *GetClientScheduleRequest `xml:"Request,omitempty"`
}

type GetClientScheduleResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientScheduleResponse"`

	GetClientScheduleResult *GetClientScheduleResult `xml:"GetClientScheduleResult,omitempty"`
}

type GetRequiredClientFields struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRequiredClientFields"`

	Request *GetRequiredClientFieldsRequest `xml:"Request,omitempty"`
}

type GetRequiredClientFieldsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRequiredClientFieldsResponse"`

	GetRequiredClientFieldsResult *GetRequiredClientFieldsResult `xml:"GetRequiredClientFieldsResult,omitempty"`
}

type ValidateLogin struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateLogin"`

	Request *ValidateLoginRequest `xml:"Request,omitempty"`
}

type ValidateLoginResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateLoginResponse"`

	ValidateLoginResult *ValidateLoginResult `xml:"ValidateLoginResult,omitempty"`
}

type UpdateClientServices struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientServices"`

	Request *UpdateClientServicesRequest `xml:"Request,omitempty"`
}

type UpdateClientServicesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientServicesResponse"`

	UpdateClientServicesResult *UpdateClientServicesResult `xml:"UpdateClientServicesResult,omitempty"`
}

type SendUserNewPassword struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SendUserNewPassword"`

	Request *ClientSendUserNewPasswordRequest `xml:"Request,omitempty"`
}

type SendUserNewPasswordResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SendUserNewPasswordResponse"`

	SendUserNewPasswordResult *ClientSendUserNewPasswordResult `xml:"SendUserNewPasswordResult,omitempty"`
}

type AddArrivalRequest struct {
	*MBRequest

	ClientID   string          `xml:"ClientID,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type MBRequest struct {
	SourceCredentials *SourceCredentials `xml:"SourceCredentials,omitempty"`
	UserCredentials   *UserCredentials   `xml:"UserCredentials,omitempty"`
	XMLDetail         *XMLDetailLevel    `xml:"XMLDetail,omitempty"`
	PageSize          *mb.CustomInt32    `xml:"PageSize,omitempty"`
	CurrentPageIndex  *mb.CustomInt32    `xml:"CurrentPageIndex,omitempty"`
	Fields            *ArrayOfString     `xml:"Fields,omitempty"`
}

type SourceCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SourceCredentials"`

	SourceName string      `xml:"SourceName,omitempty"`
	Password   string      `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfInt struct {
	Int []*mb.CustomInt32 `xml:"int,omitempty"`
}

type UserCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UserCredentials"`

	Username   string          `xml:"Username,omitempty"`
	Password   string          `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt     `xml:"SiteIDs,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type ArrayOfString struct {
	String []string `xml:"string,omitempty"`
}

type AddArrivalResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddArrivalResult"`

	*MBResult

	ArrivalAdded  bool           `xml:"ArrivalAdded,omitempty"`
	ClientService *ClientService `xml:"ClientService,omitempty"`
}

type MBResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBResult"`

	Status           *StatusCode     `xml:"Status,omitempty"`
	ErrorCode        *mb.CustomInt32 `xml:"ErrorCode,omitempty"`
	Message          string          `xml:"Message,omitempty"`
	XMLDetail        *XMLDetailLevel `xml:"XMLDetail,omitempty"`
	ResultCount      *mb.CustomInt32 `xml:"ResultCount,omitempty"`
	CurrentPageIndex *mb.CustomInt32 `xml:"CurrentPageIndex,omitempty"`
	TotalPageCount   *mb.CustomInt32 `xml:"TotalPageCount,omitempty"`
}

type ClientService struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientService"`

	*MBObject

	Current        bool            `xml:"Current,omitempty"`
	Count          *mb.CustomInt32 `xml:"Count,omitempty"`
	Remaining      *mb.CustomInt32 `xml:"Remaining,omitempty"`
	Action         *ActionCode     `xml:"Action,omitempty"`
	ID             *mb.CustomInt64 `xml:"ID,omitempty"`
	Name           string          `xml:"Name,omitempty"`
	PaymentDate    *mb.CustomTime  `xml:"PaymentDate,omitempty"`
	ActiveDate     *mb.CustomTime  `xml:"ActiveDate,omitempty"`
	ExpirationDate *mb.CustomTime  `xml:"ExpirationDate,omitempty"`
	Program        *Program        `xml:"Program,omitempty"`
}

type MBObject struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBObject"`
	XSIType string   `xml:"xsi:type,attr,omitempty"`

	Site      *Site          `xml:"Site,omitempty"`
	Messages  *ArrayOfString `xml:"Messages,omitempty"`
	Execute   string         `xml:"Execute,omitempty"`
	ErrorCode string         `xml:"ErrorCode,omitempty"`
}

type Site struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Site"`

	ID                     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name                   string          `xml:"Name,omitempty"`
	Description            string          `xml:"Description,omitempty"`
	LogoURL                string          `xml:"LogoURL,omitempty"`
	PageColor1             string          `xml:"PageColor1,omitempty"`
	PageColor2             string          `xml:"PageColor2,omitempty"`
	PageColor3             string          `xml:"PageColor3,omitempty"`
	PageColor4             string          `xml:"PageColor4,omitempty"`
	AcceptsVisa            bool            `xml:"AcceptsVisa,omitempty"`
	AcceptsDiscover        bool            `xml:"AcceptsDiscover,omitempty"`
	AcceptsMasterCard      bool            `xml:"AcceptsMasterCard,omitempty"`
	AcceptsAmericanExpress bool            `xml:"AcceptsAmericanExpress,omitempty"`
	ContactEmail           string          `xml:"ContactEmail,omitempty"`
	ESA                    bool            `xml:"ESA,omitempty"`
	TotalWOD               bool            `xml:"TotalWOD,omitempty"`
	TaxInclusivePrices     bool            `xml:"TaxInclusivePrices,omitempty"`
	SMSPackageEnabled      bool            `xml:"SMSPackageEnabled,omitempty"`
	AllowsDashboardAccess  bool            `xml:"AllowsDashboardAccess,omitempty"`
	PricingLevel           string          `xml:"PricingLevel,omitempty"`
}

type Program struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Program"`

	*MBObject

	ID           *mb.CustomInt32 `xml:"ID,omitempty"`
	Name         string          `xml:"Name,omitempty"`
	ScheduleType *ScheduleType   `xml:"ScheduleType,omitempty"`
	CancelOffset *mb.CustomInt32 `xml:"CancelOffset,omitempty"`
}

type AddOrUpdateClientsRequest struct {
	*MBRequest

	UpdateAction string         `xml:"UpdateAction,omitempty"`
	Test         bool           `xml:"Test,omitempty"`
	Clients      *ArrayOfClient `xml:"Clients,omitempty"`
	SendEmail    bool           `xml:"SendEmail,omitempty"`
}

type ArrayOfClient struct {
	Client []*Client `xml:"Client,omitempty"`
}

type Client struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Client"`

	*MBObject

	NewID                            string                     `xml:"NewID,omitempty"`
	AccountBalance                   *mb.CustomFloat64          `xml:"AccountBalance,omitempty"`
	ClientIndexes                    *ArrayOfClientIndex        `xml:"ClientIndexes,omitempty"`
	Username                         string                     `xml:"Username,omitempty"`
	Password                         string                     `xml:"Password,omitempty"`
	Notes                            string                     `xml:"Notes,omitempty"`
	MobileProvider                   *mb.CustomInt32            `xml:"MobileProvider,omitempty"`
	ClientCreditCard                 *ClientCreditCard          `xml:"ClientCreditCard,omitempty"`
	LastFormulaNotes                 string                     `xml:"LastFormulaNotes,omitempty"`
	AppointmentGenderPreference      string                     `xml:"AppointmentGenderPreference,omitempty"`
	Gender                           string                     `xml:"Gender,omitempty"`
	IsCompany                        bool                       `xml:"IsCompany,omitempty"`
	Inactive                         bool                       `xml:"Inactive,omitempty"`
	ClientRelationships              *ArrayOfClientRelationship `xml:"ClientRelationships,omitempty"`
	Reps                             *ArrayOfRep                `xml:"Reps,omitempty"`
	SaleReps                         *ArrayOfSalesRep           `xml:"SaleReps,omitempty"`
	CustomClientFields               *ArrayOfCustomClientField  `xml:"CustomClientFields,omitempty"`
	LiabilityRelease                 bool                       `xml:"LiabilityRelease,omitempty"`
	EmergencyContactInfoName         string                     `xml:"EmergencyContactInfoName,omitempty"`
	EmergencyContactInfoRelationship string                     `xml:"EmergencyContactInfoRelationship,omitempty"`
	EmergencyContactInfoPhone        string                     `xml:"EmergencyContactInfoPhone,omitempty"`
	EmergencyContactInfoEmail        string                     `xml:"EmergencyContactInfoEmail,omitempty"`
	PromotionalEmailOptIn            bool                       `xml:"PromotionalEmailOptIn,omitempty"`
	CreationDate                     *mb.CustomTime             `xml:"CreationDate,omitempty"`
	Liability                        *Liability                 `xml:"Liability,omitempty"`
	ProspectStage                    *ProspectStage             `xml:"ProspectStage,omitempty"`
	UniqueID                         *mb.CustomInt64            `xml:"UniqueID,omitempty"`
	MembershipIcon                   *mb.CustomInt32            `xml:"MembershipIcon,omitempty"`
	Action                           *ActionCode                `xml:"Action,omitempty"`
	ID                               string                     `xml:"ID,omitempty"`
	FirstName                        string                     `xml:"FirstName,omitempty"`
	MiddleName                       string                     `xml:"MiddleName,omitempty"`
	LastName                         string                     `xml:"LastName,omitempty"`
	Email                            string                     `xml:"Email,omitempty"`
	EmailOptIn                       bool                       `xml:"EmailOptIn,omitempty"`
	AddressLine1                     string                     `xml:"AddressLine1,omitempty"`
	AddressLine2                     string                     `xml:"AddressLine2,omitempty"`
	City                             string                     `xml:"City,omitempty"`
	State                            string                     `xml:"State,omitempty"`
	PostalCode                       string                     `xml:"PostalCode,omitempty"`
	Country                          string                     `xml:"Country,omitempty"`
	MobilePhone                      string                     `xml:"MobilePhone,omitempty"`
	HomePhone                        string                     `xml:"HomePhone,omitempty"`
	WorkPhone                        string                     `xml:"WorkPhone,omitempty"`
	WorkExtension                    string                     `xml:"WorkExtension,omitempty"`
	BirthDate                        *mb.CustomTime             `xml:"BirthDate,omitempty"`
	FirstAppointmentDate             *mb.CustomTime             `xml:"FirstAppointmentDate,omitempty"`
	ReferredBy                       string                     `xml:"ReferredBy,omitempty"`
	HomeLocation                     *HomeLocation              `xml:"HomeLocation,omitempty"`
	YellowAlert                      string                     `xml:"YellowAlert,omitempty"`
	RedAlert                         string                     `xml:"RedAlert,omitempty"`
	PhotoURL                         string                     `xml:"PhotoURL,omitempty"`
	IsProspect                       bool                       `xml:"IsProspect,omitempty"`
	Status                           string                     `xml:"Status,omitempty"`
	ContactMethod                    *mb.CustomInt16            `xml:"ContactMethod,omitempty"`
}

type ArrayOfClientIndex struct {
	ClientIndex []*ClientIndex `xml:"ClientIndex,omitempty"`
}

type ClientIndex struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndex"`

	*MBObject

	RequiredBusinessMode bool                     `xml:"RequiredBusinessMode,omitempty"`
	RequiredConsumerMode bool                     `xml:"RequiredConsumerMode,omitempty"`
	Action               *ActionCode              `xml:"Action,omitempty"`
	ID                   *mb.CustomInt32          `xml:"ID,omitempty"`
	Name                 string                   `xml:"Name,omitempty"`
	Values               *ArrayOfClientIndexValue `xml:"Values,omitempty"`
}

type ArrayOfClientIndexValue struct {
	ClientIndexValue []*ClientIndexValue `xml:"ClientIndexValue,omitempty"`
}

type ClientIndexValue struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndexValue"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
	Active bool            `xml:"Active,omitempty"`
}

type ClientCreditCard struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientCreditCard"`

	CardType   string `xml:"CardType,omitempty"`
	LastFour   string `xml:"LastFour,omitempty"`
	CardNumber string `xml:"CardNumber,omitempty"`
	CardHolder string `xml:"CardHolder,omitempty"`
	ExpMonth   string `xml:"ExpMonth,omitempty"`
	ExpYear    string `xml:"ExpYear,omitempty"`
	Address    string `xml:"Address,omitempty"`
	City       string `xml:"City,omitempty"`
	State      string `xml:"State,omitempty"`
	PostalCode string `xml:"PostalCode,omitempty"`
}

type ArrayOfClientRelationship struct {
	ClientRelationship []*ClientRelationship `xml:"ClientRelationship,omitempty"`
}

type ClientRelationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientRelationship"`

	*MBObject

	RelatedClient    *Client       `xml:"RelatedClient,omitempty"`
	Relationship     *Relationship `xml:"Relationship,omitempty"`
	RelationshipName string        `xml:"RelationshipName,omitempty"`
}

type Relationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Relationship"`

	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	RelationshipName1 string          `xml:"RelationshipName1,omitempty"`
	RelationshipName2 string          `xml:"RelationshipName2,omitempty"`
}

type ArrayOfRep struct {
	Rep []*Rep `xml:"Rep,omitempty"`
}

type Rep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Rep"`

	*MBObject

	ID    *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff *Staff          `xml:"Staff,omitempty"`
}

type Staff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Staff"`

	*MBObject

	Appointments             *ArrayOfAppointment      `xml:"Appointments,omitempty"`
	Unavailabilities         *ArrayOfUnavailability   `xml:"Unavailabilities,omitempty"`
	Availabilities           *ArrayOfAvailability     `xml:"Availabilities,omitempty"`
	Email                    string                   `xml:"Email,omitempty"`
	MobilePhone              string                   `xml:"MobilePhone,omitempty"`
	HomePhone                string                   `xml:"HomePhone,omitempty"`
	WorkPhone                string                   `xml:"WorkPhone,omitempty"`
	Address                  string                   `xml:"Address,omitempty"`
	Address2                 string                   `xml:"Address2,omitempty"`
	City                     string                   `xml:"City,omitempty"`
	State                    string                   `xml:"State,omitempty"`
	Country                  string                   `xml:"Country,omitempty"`
	PostalCode               string                   `xml:"PostalCode,omitempty"`
	ForeignZip               string                   `xml:"ForeignZip,omitempty"`
	SortOrder                *mb.CustomInt32          `xml:"SortOrder,omitempty"`
	LoginLocations           *ArrayOfLocation         `xml:"LoginLocations,omitempty"`
	MultiLocation            bool                     `xml:"MultiLocation,omitempty"`
	AppointmentTrn           bool                     `xml:"AppointmentTrn,omitempty"`
	ReservationTrn           bool                     `xml:"ReservationTrn,omitempty"`
	IndependentContractor    bool                     `xml:"IndependentContractor,omitempty"`
	AlwaysAllowDoubleBooking bool                     `xml:"AlwaysAllowDoubleBooking,omitempty"`
	UserAccessLevel          string                   `xml:"UserAccessLevel,omitempty"`
	ProviderIDs              *ArrayOfString           `xml:"ProviderIDs,omitempty"`
	ProviderIDUpdateList     *ArrayOfProviderIDUpdate `xml:"ProviderIDUpdateList,omitempty"`
	Action                   *ActionCode              `xml:"Action,omitempty"`
	ID                       *mb.CustomInt64          `xml:"ID,omitempty"`
	Name                     string                   `xml:"Name,omitempty"`
	FirstName                string                   `xml:"FirstName,omitempty"`
	LastName                 string                   `xml:"LastName,omitempty"`
	ImageURL                 string                   `xml:"ImageURL,omitempty"`
	Bio                      string                   `xml:"Bio,omitempty"`
	IsMale                   bool                     `xml:"isMale,omitempty"`
}

type ArrayOfAppointment struct {
	Appointment []*Appointment `xml:"Appointment,omitempty"`
}

type Appointment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Appointment"`

	*ScheduleItem

	GenderPreference string             `xml:"GenderPreference,omitempty"`
	Duration         *mb.CustomInt32    `xml:"Duration,omitempty"`
	ProviderID       string             `xml:"ProviderID,omitempty"`
	Action           *ActionCode        `xml:"Action,omitempty"`
	ID               *mb.CustomInt64    `xml:"ID,omitempty"`
	Status           *AppointmentStatus `xml:"Status,omitempty"`
	StartDateTime    *mb.CustomTime     `xml:"StartDateTime,omitempty"`
	EndDateTime      *mb.CustomTime     `xml:"EndDateTime,omitempty"`
	Notes            string             `xml:"Notes,omitempty"`
	StaffRequested   bool               `xml:"StaffRequested,omitempty"`
	Program          *Program           `xml:"Program,omitempty"`
	SessionType      *SessionType       `xml:"SessionType,omitempty"`
	Location         *Location          `xml:"Location,omitempty"`
	Staff            *Staff             `xml:"Staff,omitempty"`
	Client           *Client            `xml:"Client,omitempty"`
	FirstAppointment bool               `xml:"FirstAppointment,omitempty"`
	ClientService    *ClientService     `xml:"ClientService,omitempty"`
	Resources        *ArrayOfResource   `xml:"Resources,omitempty"`
}

type ScheduleItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ScheduleItem"`

	*MBObject
}

type Unavailability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Unavailability"`

	*ScheduleItem

	ID            *mb.CustomInt32 `xml:"ID,omitempty"`
	StartDateTime *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime   *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Description   string          `xml:"Description,omitempty"`
}

type Availability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Availability"`

	*ScheduleItem

	ID                  *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff               *Staff          `xml:"Staff,omitempty"`
	SessionType         *SessionType    `xml:"SessionType,omitempty"`
	Programs            *ArrayOfProgram `xml:"Programs,omitempty"`
	StartDateTime       *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	BookableEndDateTime *mb.CustomTime  `xml:"BookableEndDateTime,omitempty"`
	Location            *Location       `xml:"Location,omitempty"`
}

type SessionType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SessionType"`

	*MBObject

	DefaultTimeLength *mb.CustomInt32 `xml:"DefaultTimeLength,omitempty"`
	ProgramID         *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	NumDeducted       *mb.CustomInt32 `xml:"NumDeducted,omitempty"`
	Action            *ActionCode     `xml:"Action,omitempty"`
	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	Name              string          `xml:"Name,omitempty"`
}

type ArrayOfProgram struct {
	Program []*Program `xml:"Program,omitempty"`
}

type Location struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Location"`

	*MBObject

	BusinessID          *mb.CustomInt32   `xml:"BusinessID,omitempty"`
	SiteID              *mb.CustomInt32   `xml:"SiteID,omitempty"`
	BusinessDescription string            `xml:"BusinessDescription,omitempty"`
	AdditionalImageURLs *ArrayOfString    `xml:"AdditionalImageURLs,omitempty"`
	FacilitySquareFeet  *mb.CustomInt32   `xml:"FacilitySquareFeet,omitempty"`
	TreatmentRooms      *mb.CustomInt32   `xml:"TreatmentRooms,omitempty"`
	ProSpaFinderSite    bool              `xml:"ProSpaFinderSite,omitempty"`
	HasClasses          bool              `xml:"HasClasses,omitempty"`
	PhoneExtension      string            `xml:"PhoneExtension,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                string            `xml:"Name,omitempty"`
	Address             string            `xml:"Address,omitempty"`
	Address2            string            `xml:"Address2,omitempty"`
	Tax1                *mb.CustomFloat32 `xml:"Tax1,omitempty"`
	Tax2                *mb.CustomFloat32 `xml:"Tax2,omitempty"`
	Tax3                *mb.CustomFloat32 `xml:"Tax3,omitempty"`
	Tax4                *mb.CustomFloat32 `xml:"Tax4,omitempty"`
	Tax5                *mb.CustomFloat32 `xml:"Tax5,omitempty"`
	Phone               string            `xml:"Phone,omitempty"`
	City                string            `xml:"City,omitempty"`
	StateProvCode       string            `xml:"StateProvCode,omitempty"`
	PostalCode          string            `xml:"PostalCode,omitempty"`
	Latitude            *mb.CustomFloat64 `xml:"Latitude,omitempty"`
	Longitude           *mb.CustomFloat64 `xml:"Longitude,omitempty"`
	DistanceInMiles     *mb.CustomFloat64 `xml:"DistanceInMiles,omitempty"`
	ImageURL            string            `xml:"ImageURL,omitempty"`
	Description         string            `xml:"Description,omitempty"`
	HasSite             bool              `xml:"HasSite,omitempty"`
	CanBook             bool              `xml:"CanBook,omitempty"`
}

type ArrayOfResource struct {
	Resource []*Resource `xml:"Resource,omitempty"`
}

type Resource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Resource"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type ArrayOfUnavailability struct {
	Unavailability []*Unavailability `xml:"Unavailability,omitempty"`
}

type ArrayOfAvailability struct {
	Availability []*Availability `xml:"Availability,omitempty"`
}

type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty"`
}

type ArrayOfProviderIDUpdate struct {
	ProviderIDUpdate []*ProviderIDUpdate `xml:"ProviderIDUpdate,omitempty"`
}

type ProviderIDUpdate struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProviderIDUpdate"`

	OldProviderID string `xml:"OldProviderID,omitempty"`
	NewProviderID string `xml:"NewProviderID,omitempty"`
}

type ArrayOfSalesRep struct {
	SalesRep []*SalesRep `xml:"SalesRep,omitempty"`
}

type SalesRep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SalesRep"`

	*MBObject

	SalesRepNumber  *mb.CustomInt32 `xml:"SalesRepNumber,omitempty"`
	ID              *mb.CustomInt64 `xml:"ID,omitempty"`
	FirstName       string          `xml:"FirstName,omitempty"`
	LastName        string          `xml:"LastName,omitempty"`
	SalesRepNumbers *ArrayOfInt     `xml:"SalesRepNumbers,omitempty"`
}

type ArrayOfCustomClientField struct {
	CustomClientField []*CustomClientField `xml:"CustomClientField,omitempty"`
}

type CustomClientField struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CustomClientField"`

	ID       *mb.CustomInt32 `xml:"ID,omitempty"`
	DataType string          `xml:"DataType,omitempty"`
	Name     string          `xml:"Name,omitempty"`
	Value    string          `xml:"Value,omitempty"`
}

type Liability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Liability"`

	IsReleased    bool            `xml:"IsReleased,omitempty"`
	AgreementDate *mb.CustomTime  `xml:"AgreementDate,omitempty"`
	ReleasedBy    *mb.CustomInt64 `xml:"ReleasedBy,omitempty"`
}

type ProspectStage struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProspectStage"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type AddOrUpdateClientsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateClientsResult"`

	*MBResult

	Clients *ArrayOfClient `xml:"Clients,omitempty"`
}

type GetClientsRequest struct {
	*MBRequest

	ClientIDs  *ArrayOfString `xml:"ClientIDs,omitempty"`
	SearchText string         `xml:"SearchText"`
	IsProspect bool           `xml:"IsProspect,omitempty"`
}

type GetClientsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientsResult"`

	*MBResult

	Clients *ArrayOfClient `xml:"Clients,omitempty"`
}

type GetCustomClientFieldsRequest struct {
	*MBRequest
}

type GetCustomClientFieldsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCustomClientFieldsResult"`

	*MBResult

	CustomClientFields *ArrayOfCustomClientField `xml:"CustomClientFields,omitempty"`
}

type GetClientIndexesRequest struct {
	*MBRequest

	RequiredOnly bool `xml:"RequiredOnly,omitempty"`
}

type GetClientIndexesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientIndexesResult"`

	*MBResult

	ClientIndexes *ArrayOfClientIndex `xml:"ClientIndexes,omitempty"`
}

type GetClientContactLogsRequest struct {
	*MBRequest

	ClientID            string         `xml:"ClientID,omitempty"`
	StartDate           *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate             *mb.CustomTime `xml:"EndDate,omitempty"`
	StaffIDs            *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	ShowSystemGenerated bool           `xml:"ShowSystemGenerated,omitempty"`
	TypeIDs             *ArrayOfInt    `xml:"TypeIDs,omitempty"`
	SubtypeIDs          *ArrayOfInt    `xml:"SubtypeIDs,omitempty"`
}

type ArrayOfLong struct {
	Long []*mb.CustomInt64 `xml:"long,omitempty"`
}

type GetClientContactLogsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContactLogsResult"`

	*MBResult

	ContactLogs *ArrayOfContactLog `xml:"ContactLogs,omitempty"`
}

type ArrayOfContactLog struct {
	ContactLog []*ContactLog `xml:"ContactLog,omitempty"`
}

type ContactLog struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ContactLog"`

	*MBObject

	ID                *mb.CustomInt64           `xml:"ID,omitempty"`
	CreatedBy         *Staff                    `xml:"CreatedBy,omitempty"`
	Client            *Client                   `xml:"Client,omitempty"`
	CreatedDateTime   *mb.CustomTime            `xml:"CreatedDateTime,omitempty"`
	FollowupByDate    *mb.CustomTime            `xml:"FollowupByDate,omitempty"`
	ContactName       string                    `xml:"ContactName,omitempty"`
	Text              string                    `xml:"Text,omitempty"`
	AssignedTo        *Staff                    `xml:"AssignedTo,omitempty"`
	ContactMethod     string                    `xml:"ContactMethod,omitempty"`
	IsSystemGenerated bool                      `xml:"IsSystemGenerated,omitempty"`
	Comments          *ArrayOfContactLogComment `xml:"Comments,omitempty"`
	Types             *ArrayOfContactLogType    `xml:"Types,omitempty"`
	Action            *ActionCode               `xml:"Action,omitempty"`
}

type ArrayOfContactLogComment struct {
	ContactLogComment []*ContactLogComment `xml:"ContactLogComment,omitempty"`
}

type ContactLogComment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ContactLogComment"`

	*MBObject

	ID              *mb.CustomInt32 `xml:"ID,omitempty"`
	Text            string          `xml:"Text,omitempty"`
	CreatedBy       *Staff          `xml:"CreatedBy,omitempty"`
	CreatedDateTime *mb.CustomTime  `xml:"CreatedDateTime,omitempty"`
}

type ArrayOfContactLogType struct {
	ContactLogType []*ContactLogType `xml:"ContactLogType,omitempty"`
}

type ContactLogType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ContactLogType"`

	*MBObject

	ID       *mb.CustomInt32           `xml:"ID,omitempty"`
	Name     string                    `xml:"Name,omitempty"`
	Subtypes *ArrayOfContactLogSubtype `xml:"Subtypes,omitempty"`
}

type ArrayOfContactLogSubtype struct {
	ContactLogSubtype []*ContactLogSubtype `xml:"ContactLogSubtype,omitempty"`
}

type ContactLogSubtype struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ContactLogSubtype"`

	*MBObject

	ID   *mb.CustomInt32 `xml:"ID,omitempty"`
	Name string          `xml:"Name,omitempty"`
}

type AddOrUpdateContactLogsRequest struct {
	*MBRequest

	UpdateAction string             `xml:"UpdateAction,omitempty"`
	Test         bool               `xml:"Test,omitempty"`
	ContactLogs  *ArrayOfContactLog `xml:"ContactLogs,omitempty"`
}

type AddOrUpdateContactLogsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateContactLogsResult"`

	*MBResult

	ContactLogs *ArrayOfContactLog `xml:"ContactLogs,omitempty"`
}

type GetContactLogTypesRequest struct {
	*MBRequest
}

type GetContactLogTypesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetContactLogTypesResult"`

	*MBResult

	ContatctLogTypes *ArrayOfContactLogType `xml:"ContatctLogTypes,omitempty"`
}

type UploadClientDocumentRequest struct {
	*MBRequest

	ClientID string `xml:"ClientID,omitempty"`
	FileName string `xml:"FileName,omitempty"`
	Bytes    []byte `xml:"Bytes,omitempty"`
}

type UploadClientDocumentResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientDocumentResult"`

	*MBResult

	FileSize *mb.CustomInt64 `xml:"FileSize,omitempty"`
	FileName string          `xml:"FileName,omitempty"`
}

type UploadClientPhotoRequest struct {
	*MBRequest

	ClientID string `xml:"ClientID,omitempty"`
	Bytes    []byte `xml:"Bytes,omitempty"`
}

type UploadClientPhotoResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UploadClientPhotoResult"`

	*MBResult

	PhotoUrl string `xml:"PhotoUrl,omitempty"`
	ClientID string `xml:"ClientID,omitempty"`
}

type GetClientFormulaNotesRequest struct {
	*MBRequest

	ClientID      string          `xml:"ClientID,omitempty"`
	AppointmentID *mb.CustomInt64 `xml:"AppointmentID,omitempty"`
}

type GetClientFormulaNotesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientFormulaNotesResult"`

	*MBResult

	FormulaNotes *ArrayOfFormulaNote `xml:"FormulaNotes,omitempty"`
}

type ArrayOfFormulaNote struct {
	FormulaNote []*FormulaNote `xml:"FormulaNote,omitempty"`
}

type FormulaNote struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 FormulaNote"`

	*MBObject

	ID            *mb.CustomInt64 `xml:"ID,omitempty"`
	ClientID      string          `xml:"ClientID,omitempty"`
	Note          string          `xml:"Note,omitempty"`
	EntryDateTime *mb.CustomTime  `xml:"EntryDateTime,omitempty"`
	AppointmentID *mb.CustomInt64 `xml:"AppointmentID,omitempty"`
}

type AddClientFormulaNoteRequest struct {
	*MBRequest

	ClientID      string          `xml:"ClientID,omitempty"`
	AppointmentID *mb.CustomInt64 `xml:"AppointmentID,omitempty"`
	Note          string          `xml:"Note,omitempty"`
}

type AddClientFormulaNoteResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientFormulaNoteResult"`

	*MBResult

	FormulaNote *FormulaNote `xml:"FormulaNote,omitempty"`
}

type DeleteCientFormulaNoteRequest struct {
	*MBRequest

	FormulaNoteID *mb.CustomInt64 `xml:"FormulaNoteID,omitempty"`
	ClientID      string          `xml:"ClientID,omitempty"`
}

type DeleteClientFormulaNoteResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 DeleteClientFormulaNoteResult"`

	*MBResult

	FormulaNote *FormulaNote `xml:"FormulaNote,omitempty"`
}

type GetClientReferralTypesRequest struct {
	*MBRequest

	IncludeInactive bool `xml:"IncludeInactive,omitempty"`
}

type GetClientReferralTypesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientReferralTypesResult"`

	*MBResult

	ReferralTypes *ArrayOfString `xml:"ReferralTypes,omitempty"`
}

type GetActiveClientMembershipsRequest struct {
	*MBRequest

	ClientID   string          `xml:"ClientID,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type GetActiveClientMembershipsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActiveClientMembershipsResult"`

	*MBResult

	ClientMemberships *ArrayOfClientMembership `xml:"ClientMemberships,omitempty"`
}

type ArrayOfClientMembership struct {
	ClientMembership []*ClientMembership `xml:"ClientMembership,omitempty"`
}

type ClientMembership struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientMembership"`

	*ClientService

	RestrictedLocations *ArrayOfLocation `xml:"RestrictedLocations,omitempty"`
	IconCode            string           `xml:"IconCode,omitempty"`
}

type GetClientContractsRequest struct {
	*MBRequest

	ClientID string `xml:"ClientID,omitempty"`
}

type GetClientContractsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientContractsResult"`

	*MBResult

	Contracts *ArrayOfClientContract `xml:"Contracts,omitempty"`
}

type ArrayOfClientContract struct {
	ClientContract []*ClientContract `xml:"ClientContract,omitempty"`
}

type ClientContract struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientContract"`

	*MBObject

	AgreementDate *mb.CustomTime  `xml:"AgreementDate,omitempty"`
	StartDate     *mb.CustomTime  `xml:"StartDate,omitempty"`
	EndDate       *mb.CustomTime  `xml:"EndDate,omitempty"`
	ContractName  string          `xml:"ContractName,omitempty"`
	Action        *ActionCode     `xml:"Action,omitempty"`
	ID            *mb.CustomInt32 `xml:"ID,omitempty"`
}

type GetClientAccountBalancesRequest struct {
	*MBRequest

	ClientIDs   *ArrayOfString  `xml:"ClientIDs,omitempty"`
	BalanceDate *mb.CustomTime  `xml:"BalanceDate,omitempty"`
	ClassID     *mb.CustomInt64 `xml:"ClassID,omitempty"`
}

type GetClientAccountBalancesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientAccountBalancesResult"`

	*MBResult

	Clients *ArrayOfClient `xml:"Clients,omitempty"`
}

type GetClientServicesRequest struct {
	*MBRequest

	ClientID       string          `xml:"ClientID,omitempty"`
	ClassID        *mb.CustomInt32 `xml:"ClassID,omitempty"`
	ProgramIDs     *ArrayOfInt     `xml:"ProgramIDs,omitempty"`
	SessionTypeIDs *ArrayOfInt     `xml:"SessionTypeIDs,omitempty"`
	LocationIDs    *ArrayOfInt     `xml:"LocationIDs,omitempty"`
	VisitCount     *mb.CustomInt32 `xml:"VisitCount,omitempty"`
	StartDate      *mb.CustomTime  `xml:"StartDate,omitempty"`
	EndDate        *mb.CustomTime  `xml:"EndDate,omitempty"`
	ShowActiveOnly bool            `xml:"ShowActiveOnly,omitempty"`
}

type GetClientServicesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientServicesResult"`

	*MBResult

	ClientServices *ArrayOfClientService `xml:"ClientServices,omitempty"`
}

type ArrayOfClientService struct {
	ClientService []*ClientService `xml:"ClientService,omitempty"`
}

type GetClientVisitsRequest struct {
	*MBRequest

	ClientID    string         `xml:"ClientID,omitempty"`
	StartDate   *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate     *mb.CustomTime `xml:"EndDate,omitempty"`
	UnpaidsOnly bool           `xml:"UnpaidsOnly,omitempty"`
}

type GetClientVisitsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientVisitsResult"`

	*MBResult

	Visits *ArrayOfVisit `xml:"Visits,omitempty"`
}

type ArrayOfVisit struct {
	Visit []*Visit `xml:"Visit,omitempty"`
}

type Visit struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Visit"`

	*MBObject

	ID                          *mb.CustomInt64 `xml:"ID,omitempty"`
	ClassID                     *mb.CustomInt32 `xml:"ClassID,omitempty"`
	AppointmentID               *mb.CustomInt32 `xml:"AppointmentID,omitempty"`
	AppointmentGenderPreference string          `xml:"AppointmentGenderPreference,omitempty"`
	StartDateTime               *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	LateCancelled               bool            `xml:"LateCancelled,omitempty"`
	EndDateTime                 *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Name                        string          `xml:"Name,omitempty"`
	Staff                       *Staff          `xml:"Staff,omitempty"`
	Location                    *Location       `xml:"Location,omitempty"`
	Client                      *Client         `xml:"Client,omitempty"`
	WebSignup                   bool            `xml:"WebSignup,omitempty"`
	Action                      *ActionCode     `xml:"Action,omitempty"`
	SignedIn                    bool            `xml:"SignedIn,omitempty"`
	AppointmentStatus           string          `xml:"AppointmentStatus,omitempty"`
	MakeUp                      bool            `xml:"MakeUp,omitempty"`
	Service                     *Service        `xml:"Service,omitempty"`
}

type GetClientPurchasesRequest struct {
	*MBRequest

	ClientID  string          `xml:"ClientID,omitempty"`
	StartDate *mb.CustomTime  `xml:"StartDate,omitempty"`
	EndDate   *mb.CustomTime  `xml:"EndDate,omitempty"`
	SaleID    *mb.CustomInt32 `xml:"SaleID,omitempty"`
}

type GetClientPurchasesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientPurchasesResult"`

	*MBResult

	Purchases *ArrayOfSaleItem `xml:"Purchases,omitempty"`
}

type ArrayOfSaleItem struct {
	SaleItem []*SaleItem `xml:"SaleItem,omitempty"`
}

type SaleItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SaleItem"`

	*MBObject

	Sale           *Sale             `xml:"Sale,omitempty"`
	Description    string            `xml:"Description,omitempty"`
	AccountPayment bool              `xml:"AccountPayment,omitempty"`
	Price          *mb.CustomFloat64 `xml:"Price,omitempty"`
	AmountPaid     *mb.CustomFloat64 `xml:"AmountPaid,omitempty"`
	Discount       *mb.CustomFloat64 `xml:"Discount,omitempty"`
	Tax            *mb.CustomFloat64 `xml:"Tax,omitempty"`
	Returned       bool              `xml:"Returned,omitempty"`
	Quantity       *mb.CustomInt32   `xml:"Quantity,omitempty"`
}

type Sale struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Sale"`

	ID           *mb.CustomInt64 `xml:"ID,omitempty"`
	SaleTime     *mb.CustomTime  `xml:"SaleTime,omitempty"`
	SaleDate     *mb.CustomTime  `xml:"SaleDate,omitempty"`
	SaleDateTime *mb.CustomTime  `xml:"SaleDateTime,omitempty"`
	Location     *Location       `xml:"Location,omitempty"`
	Payments     *ArrayOfPayment `xml:"Payments,omitempty"`
}

type ArrayOfPayment struct {
	Payment []*Payment `xml:"Payment,omitempty"`
}

type Payment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Payment"`

	ID       *mb.CustomInt64   `xml:"ID,omitempty"`
	Amount   *mb.CustomFloat64 `xml:"Amount,omitempty"`
	Method   *mb.CustomInt32   `xml:"Method,omitempty"`
	Type     string            `xml:"Type,omitempty"`
	LastFour string            `xml:"LastFour,omitempty"`
	Notes    string            `xml:"Notes,omitempty"`
}

type GetClientScheduleRequest struct {
	*MBRequest

	ClientID  string         `xml:"ClientID,omitempty"`
	StartDate *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate   *mb.CustomTime `xml:"EndDate,omitempty"`
}

type GetClientScheduleResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClientScheduleResult"`

	*MBResult

	Visits *ArrayOfVisit `xml:"Visits,omitempty"`
}

type GetRequiredClientFieldsRequest struct {
	*MBRequest
}

type GetRequiredClientFieldsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRequiredClientFieldsResult"`

	*MBResult

	RequiredClientFields *ArrayOfString `xml:"RequiredClientFields,omitempty"`
}

type ValidateLoginRequest struct {
	*MBRequest

	Username string `xml:"Username,omitempty"`
	Password string `xml:"Password,omitempty"`
}

type ValidateLoginResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateLoginResult"`

	*MBResult

	GUID   string  `xml:"GUID,omitempty"`
	Client *Client `xml:"Client,omitempty"`
	Staff  *Staff  `xml:"Staff,omitempty"`
}

type UpdateClientServicesRequest struct {
	*MBRequest

	ClientServices *ArrayOfClientService `xml:"ClientServices,omitempty"`
	Test           bool                  `xml:"Test,omitempty"`
}

type UpdateClientServicesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientServicesResult"`

	*MBResult

	ClientServices *ArrayOfClientService `xml:"ClientServices,omitempty"`
}

type ClientSendUserNewPasswordRequest struct {
	*MBRequest

	UserEmail     string `xml:"UserEmail,omitempty"`
	UserFirstName string `xml:"UserFirstName,omitempty"`
	UserLastName  string `xml:"UserLastName,omitempty"`
}

type ClientSendUserNewPasswordResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientSendUserNewPasswordResult"`

	*MBResult
}

type Client_x0020_ServiceSoap struct {
	client *SOAPClient
}

func NewClient_x0020_ServiceSoap(url string, tls bool, auth *BasicAuth) *Client_x0020_ServiceSoap {
	if url == "" {
		url = "https://api.mindbodyonline.com/0_5/ClientService.asmx"
	}
	client := NewSOAPClient(url, tls, auth)

	return &Client_x0020_ServiceSoap{
		client: client,
	}
}

/* Adds an arrival record for the given client. */
func (service *Client_x0020_ServiceSoap) AddArrival(request *AddArrival) (*AddArrivalResponse, error) {
	response := new(AddArrivalResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddArrival", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Adds or updates information for a list of clients. */
func (service *Client_x0020_ServiceSoap) AddOrUpdateClients(request *AddOrUpdateClients) (*AddOrUpdateClientsResponse, error) {
	response := new(AddOrUpdateClientsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddOrUpdateClients", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of clients. */
func (service *Client_x0020_ServiceSoap) GetClients(request *GetClients) (*GetClientsResponse, error) {
	response := new(GetClientsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClients", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of currently available client indexes. */
func (service *Client_x0020_ServiceSoap) GetCustomClientFields(request *GetCustomClientFields) (*GetCustomClientFieldsResponse, error) {
	response := new(GetCustomClientFieldsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetCustomClientFields", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of currently available client indexes. */
func (service *Client_x0020_ServiceSoap) GetClientIndexes(request *GetClientIndexes) (*GetClientIndexesResponse, error) {
	response := new(GetClientIndexesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientIndexes", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get contact logs for a client. */
func (service *Client_x0020_ServiceSoap) GetClientContactLogs(request *GetClientContactLogs) (*GetClientContactLogsResponse, error) {
	response := new(GetClientContactLogsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientContactLogs", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Add or update client contact logs. */
func (service *Client_x0020_ServiceSoap) AddOrUpdateContactLogs(request *AddOrUpdateContactLogs) (*AddOrUpdateContactLogsResponse, error) {
	response := new(AddOrUpdateContactLogsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddOrUpdateContactLogs", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get contact log types for a client. */
func (service *Client_x0020_ServiceSoap) GetContactLogTypes(request *GetContactLogTypes) (*GetContactLogTypesResponse, error) {
	response := new(GetContactLogTypesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetContactLogTypes", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Upload a client document. */
func (service *Client_x0020_ServiceSoap) UploadClientDocument(request *UploadClientDocument) (*UploadClientDocumentResponse, error) {
	response := new(UploadClientDocumentResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UploadClientDocument", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Upload a client photo. */
func (service *Client_x0020_ServiceSoap) UploadClientPhoto(request *UploadClientPhoto) (*UploadClientPhotoResponse, error) {
	response := new(UploadClientPhotoResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UploadClientPhoto", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of client formula notes. */
func (service *Client_x0020_ServiceSoap) GetClientFormulaNotes(request *GetClientFormulaNotes) (*GetClientFormulaNotesResponse, error) {
	response := new(GetClientFormulaNotesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientFormulaNotes", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Adds a formula note to a client. */
func (service *Client_x0020_ServiceSoap) AddClientFormulaNote(request *AddClientFormulaNote) (*AddClientFormulaNoteResponse, error) {
	response := new(AddClientFormulaNoteResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddClientFormulaNote", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Deletes a formula note to a client. */
func (service *Client_x0020_ServiceSoap) DeleteClientFormulaNote(request *DeleteClientFormulaNote) (*DeleteClientFormulaNoteResponse, error) {
	response := new(DeleteClientFormulaNoteResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/DeleteClientFormulaNote", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of clients. */
func (service *Client_x0020_ServiceSoap) GetClientReferralTypes(request *GetClientReferralTypes) (*GetClientReferralTypesResponse, error) {
	response := new(GetClientReferralTypesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientReferralTypes", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets the active membership for a given client. */
func (service *Client_x0020_ServiceSoap) GetActiveClientMemberships(request *GetActiveClientMemberships) (*GetActiveClientMembershipsResponse, error) {
	response := new(GetActiveClientMembershipsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetActiveClientMemberships", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of contracts for a given client. */
func (service *Client_x0020_ServiceSoap) GetClientContracts(request *GetClientContracts) (*GetClientContractsResponse, error) {
	response := new(GetClientContractsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientContracts", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets account balances for the given clients. */
func (service *Client_x0020_ServiceSoap) GetClientAccountBalances(request *GetClientAccountBalances) (*GetClientAccountBalancesResponse, error) {
	response := new(GetClientAccountBalancesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientAccountBalances", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a client service for a given client. */
func (service *Client_x0020_ServiceSoap) GetClientServices(request *GetClientServices) (*GetClientServicesResponse, error) {
	response := new(GetClientServicesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientServices", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get visits for a client. */
func (service *Client_x0020_ServiceSoap) GetClientVisits(request *GetClientVisits) (*GetClientVisitsResponse, error) {
	response := new(GetClientVisitsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientVisits", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get purchases for a client. */
func (service *Client_x0020_ServiceSoap) GetClientPurchases(request *GetClientPurchases) (*GetClientPurchasesResponse, error) {
	response := new(GetClientPurchasesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientPurchases", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get visits for a client. */
func (service *Client_x0020_ServiceSoap) GetClientSchedule(request *GetClientSchedule) (*GetClientScheduleResponse, error) {
	response := new(GetClientScheduleResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClientSchedule", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Updates a client service for a given client. */
func (service *Client_x0020_ServiceSoap) GetRequiredClientFields(request *GetRequiredClientFields) (*GetRequiredClientFieldsResponse, error) {
	response := new(GetRequiredClientFieldsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetRequiredClientFields", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Validates a username and password. This method returns the associated clients record and a session GUID on success. */
func (service *Client_x0020_ServiceSoap) ValidateLogin(request *ValidateLogin) (*ValidateLoginResponse, error) {
	response := new(ValidateLoginResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/ValidateLogin", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Updates a client service for a given client. */
func (service *Client_x0020_ServiceSoap) UpdateClientServices(request *UpdateClientServices) (*UpdateClientServicesResponse, error) {
	response := new(UpdateClientServicesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UpdateClientServices", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Sends the user a new password. */
func (service *Client_x0020_ServiceSoap) SendUserNewPassword(request *SendUserNewPassword) (*SendUserNewPasswordResponse, error) {
	response := new(SendUserNewPasswordResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/SendUserNewPassword", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
	//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Println(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Println("empty response")
		return nil
	}

	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}

type HomeLocation struct {
	*Location

	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 HomeLocation"`
}

type Service struct {
	*ClientService

	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Service"`
}
