package staff

import (
	mb "bitbucket.org/canopei/mindbody"
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

// against "unused imports"
var _ xml.Name

type XMLDetailLevel string

const (
	XMLDetailLevelBare XMLDetailLevel = "Bare"

	XMLDetailLevelBasic XMLDetailLevel = "Basic"

	XMLDetailLevelFull XMLDetailLevel = "Full"
)

type StaffFilter string

const (
	StaffFilterStaffViewable StaffFilter = "StaffViewable"

	StaffFilterAppointmentInstructor StaffFilter = "AppointmentInstructor"

	StaffFilterClassInstructor StaffFilter = "ClassInstructor"

	StaffFilterMale StaffFilter = "Male"

	StaffFilterFemale StaffFilter = "Female"
)

type StatusCode string

const (
	StatusCodeSuccess StatusCode = "Success"

	StatusCodeInvalidCredentials StatusCode = "InvalidCredentials"

	StatusCodeInvalidParameters StatusCode = "InvalidParameters"

	StatusCodeInternalException StatusCode = "InternalException"

	StatusCodeUnknown StatusCode = "Unknown"

	StatusCodeFailedAction StatusCode = "FailedAction"
)

type ActionCode string

const (
	ActionCodeNone ActionCode = "None"

	ActionCodeAdded ActionCode = "Added"

	ActionCodeUpdated ActionCode = "Updated"

	ActionCodeFailed ActionCode = "Failed"

	ActionCodeRemoved ActionCode = "Removed"
)

type ScheduleType string

const (
	ScheduleTypeAll ScheduleType = "All"

	ScheduleTypeDropIn ScheduleType = "DropIn"

	ScheduleTypeEnrollment ScheduleType = "Enrollment"

	ScheduleTypeAppointment ScheduleType = "Appointment"

	ScheduleTypeResource ScheduleType = "Resource"

	ScheduleTypeMedia ScheduleType = "Media"

	ScheduleTypeArrival ScheduleType = "Arrival"
)

type AppointmentStatus string

const (
	AppointmentStatusBooked AppointmentStatus = "Booked"

	AppointmentStatusCompleted AppointmentStatus = "Completed"

	AppointmentStatusConfirmed AppointmentStatus = "Confirmed"

	AppointmentStatusArrived AppointmentStatus = "Arrived"

	AppointmentStatusNoShow AppointmentStatus = "NoShow"

	AppointmentStatusCancelled AppointmentStatus = "Cancelled"

	AppointmentStatusLateCancelled AppointmentStatus = "LateCancelled"
)

type GetStaff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaff"`

	Request *GetStaffRequest `xml:"Request,omitempty"`
}

type GetStaffResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffResponse"`

	GetStaffResult *GetStaffResult `xml:"GetStaffResult,omitempty"`
}

type GetStaffPermissions struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffPermissions"`

	Request *GetStaffPermissionsRequest `xml:"Request,omitempty"`
}

type GetStaffPermissionsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffPermissionsResponse"`

	GetStaffPermissionsResult *GetStaffPermissionsResult `xml:"GetStaffPermissionsResult,omitempty"`
}

type AddOrUpdateStaff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateStaff"`

	Request *AddOrUpdateStaffRequest `xml:"Request,omitempty"`
}

type AddOrUpdateStaffResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateStaffResponse"`

	AddOrUpdateStaffResult *AddOrUpdateStaffResult `xml:"AddOrUpdateStaffResult,omitempty"`
}

type GetStaffImgURL struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffImgURL"`

	Request *GetStaffImgURLRequest `xml:"Request,omitempty"`
}

type GetStaffImgURLResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffImgURLResponse"`

	GetStaffImgURLResult *GetStaffImgURLResult `xml:"GetStaffImgURLResult,omitempty"`
}

type ValidateStaffLogin struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateStaffLogin"`

	Request *ValidateLoginRequest `xml:"Request,omitempty"`
}

type ValidateStaffLoginResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateStaffLoginResponse"`

	ValidateStaffLoginResult *ValidateLoginResult `xml:"ValidateStaffLoginResult,omitempty"`
}

type GetSalesReps struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSalesReps"`

	Request *GetSalesRepsRequest `xml:"Request,omitempty"`
}

type GetSalesRepsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSalesRepsResponse"`

	GetSalesRepsResult *GetSalesRepsResult `xml:"GetSalesRepsResult,omitempty"`
}

type GetStaffRequest struct {
	*MBRequest

	StaffIDs         *ArrayOfLong        `xml:"StaffIDs,omitempty"`
	StaffCredentials *StaffCredentials   `xml:"StaffCredentials,omitempty"`
	Filters          *ArrayOfStaffFilter `xml:"Filters,omitempty"`
	SessionTypeID    *mb.CustomInt32     `xml:"SessionTypeID,omitempty"`
	StartDateTime    *mb.CustomTime      `xml:"StartDateTime,omitempty"`
	LocationID       *mb.CustomInt32     `xml:"LocationID,omitempty"`
}

type MBRequest struct {
	SourceCredentials *SourceCredentials `xml:"SourceCredentials,omitempty"`
	UserCredentials   *UserCredentials   `xml:"UserCredentials,omitempty"`
	XMLDetail         *XMLDetailLevel    `xml:"XMLDetail,omitempty"`
	PageSize          *mb.CustomInt32    `xml:"PageSize,omitempty"`
	CurrentPageIndex  *mb.CustomInt32    `xml:"CurrentPageIndex,omitempty"`
	Fields            *ArrayOfString     `xml:"Fields,omitempty"`
}

type SourceCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SourceCredentials"`

	SourceName string      `xml:"SourceName,omitempty"`
	Password   string      `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfInt struct {
	Int []*mb.CustomInt32 `xml:"int,omitempty"`
}

type UserCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UserCredentials"`

	Username   string          `xml:"Username,omitempty"`
	Password   string          `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt     `xml:"SiteIDs,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type ArrayOfString struct {
	String []string `xml:"string,omitempty"`
}

type ArrayOfLong struct {
	Long []*mb.CustomInt64 `xml:"long,omitempty"`
}

type StaffCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 StaffCredentials"`

	Username string      `xml:"Username,omitempty"`
	Password string      `xml:"Password,omitempty"`
	SiteIDs  *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfStaffFilter struct {
	StaffFilter []*StaffFilter `xml:"StaffFilter,omitempty"`
}

type GetStaffResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffResult"`

	*MBResult

	StaffMembers *ArrayOfStaff `xml:"StaffMembers,omitempty"`
}

type MBResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBResult"`

	Status           *StatusCode     `xml:"Status,omitempty"`
	ErrorCode        *mb.CustomInt32 `xml:"ErrorCode,omitempty"`
	Message          string          `xml:"Message,omitempty"`
	XMLDetail        *XMLDetailLevel `xml:"XMLDetail,omitempty"`
	ResultCount      *mb.CustomInt32 `xml:"ResultCount,omitempty"`
	CurrentPageIndex *mb.CustomInt32 `xml:"CurrentPageIndex,omitempty"`
	TotalPageCount   *mb.CustomInt32 `xml:"TotalPageCount,omitempty"`
}

type ArrayOfStaff struct {
	Staff []*Staff `xml:"Staff,omitempty"`
}

type Staff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Staff"`

	*MBObject

	Appointments             *ArrayOfAppointment      `xml:"Appointments,omitempty"`
	Unavailabilities         *ArrayOfUnavailability   `xml:"Unavailabilities,omitempty"`
	Availabilities           *ArrayOfAvailability     `xml:"Availabilities,omitempty"`
	Email                    string                   `xml:"Email,omitempty"`
	MobilePhone              string                   `xml:"MobilePhone,omitempty"`
	HomePhone                string                   `xml:"HomePhone,omitempty"`
	WorkPhone                string                   `xml:"WorkPhone,omitempty"`
	Address                  string                   `xml:"Address,omitempty"`
	Address2                 string                   `xml:"Address2,omitempty"`
	City                     string                   `xml:"City,omitempty"`
	State                    string                   `xml:"State,omitempty"`
	Country                  string                   `xml:"Country,omitempty"`
	PostalCode               string                   `xml:"PostalCode,omitempty"`
	ForeignZip               string                   `xml:"ForeignZip,omitempty"`
	SortOrder                *mb.CustomInt32          `xml:"SortOrder,omitempty"`
	LoginLocations           *ArrayOfLocation         `xml:"LoginLocations,omitempty"`
	MultiLocation            bool                     `xml:"MultiLocation,omitempty"`
	AppointmentTrn           bool                     `xml:"AppointmentTrn,omitempty"`
	ReservationTrn           bool                     `xml:"ReservationTrn,omitempty"`
	IndependentContractor    bool                     `xml:"IndependentContractor,omitempty"`
	AlwaysAllowDoubleBooking bool                     `xml:"AlwaysAllowDoubleBooking,omitempty"`
	UserAccessLevel          string                   `xml:"UserAccessLevel,omitempty"`
	ProviderIDs              *ArrayOfString           `xml:"ProviderIDs,omitempty"`
	ProviderIDUpdateList     *ArrayOfProviderIDUpdate `xml:"ProviderIDUpdateList,omitempty"`
	Action                   *ActionCode              `xml:"Action,omitempty"`
	ID                       *mb.CustomInt64          `xml:"ID,omitempty"`
	Name                     string                   `xml:"Name,omitempty"`
	FirstName                string                   `xml:"FirstName,omitempty"`
	LastName                 string                   `xml:"LastName,omitempty"`
	ImageURL                 string                   `xml:"ImageURL,omitempty"`
	Bio                      string                   `xml:"Bio,omitempty"`
	IsMale                   bool                     `xml:"isMale,omitempty"`
}

type MBObject struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBObject"`
	XSIType string   `xml:"xsi:type,attr,omitempty"`

	Site      *Site          `xml:"Site,omitempty"`
	Messages  *ArrayOfString `xml:"Messages,omitempty"`
	Execute   string         `xml:"Execute,omitempty"`
	ErrorCode string         `xml:"ErrorCode,omitempty"`
}

type Site struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Site"`

	ID                     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name                   string          `xml:"Name,omitempty"`
	Description            string          `xml:"Description,omitempty"`
	LogoURL                string          `xml:"LogoURL,omitempty"`
	PageColor1             string          `xml:"PageColor1,omitempty"`
	PageColor2             string          `xml:"PageColor2,omitempty"`
	PageColor3             string          `xml:"PageColor3,omitempty"`
	PageColor4             string          `xml:"PageColor4,omitempty"`
	AcceptsVisa            bool            `xml:"AcceptsVisa,omitempty"`
	AcceptsDiscover        bool            `xml:"AcceptsDiscover,omitempty"`
	AcceptsMasterCard      bool            `xml:"AcceptsMasterCard,omitempty"`
	AcceptsAmericanExpress bool            `xml:"AcceptsAmericanExpress,omitempty"`
	ContactEmail           string          `xml:"ContactEmail,omitempty"`
	ESA                    bool            `xml:"ESA,omitempty"`
	TotalWOD               bool            `xml:"TotalWOD,omitempty"`
	TaxInclusivePrices     bool            `xml:"TaxInclusivePrices,omitempty"`
	SMSPackageEnabled      bool            `xml:"SMSPackageEnabled,omitempty"`
	AllowsDashboardAccess  bool            `xml:"AllowsDashboardAccess,omitempty"`
	PricingLevel           string          `xml:"PricingLevel,omitempty"`
}

type Resource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Resource"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type ClientService struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientService"`

	*MBObject

	Current        bool            `xml:"Current,omitempty"`
	Count          *mb.CustomInt32 `xml:"Count,omitempty"`
	Remaining      *mb.CustomInt32 `xml:"Remaining,omitempty"`
	Action         *ActionCode     `xml:"Action,omitempty"`
	ID             *mb.CustomInt64 `xml:"ID,omitempty"`
	Name           string          `xml:"Name,omitempty"`
	PaymentDate    *mb.CustomTime  `xml:"PaymentDate,omitempty"`
	ActiveDate     *mb.CustomTime  `xml:"ActiveDate,omitempty"`
	ExpirationDate *mb.CustomTime  `xml:"ExpirationDate,omitempty"`
	Program        *Program        `xml:"Program,omitempty"`
}

type Program struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Program"`

	*MBObject

	ID           *mb.CustomInt32 `xml:"ID,omitempty"`
	Name         string          `xml:"Name,omitempty"`
	ScheduleType *ScheduleType   `xml:"ScheduleType,omitempty"`
	CancelOffset *mb.CustomInt32 `xml:"CancelOffset,omitempty"`
}

type SalesRep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SalesRep"`

	*MBObject

	SalesRepNumber  *mb.CustomInt32 `xml:"SalesRepNumber,omitempty"`
	ID              *mb.CustomInt64 `xml:"ID,omitempty"`
	FirstName       string          `xml:"FirstName,omitempty"`
	LastName        string          `xml:"LastName,omitempty"`
	SalesRepNumbers *ArrayOfInt     `xml:"SalesRepNumbers,omitempty"`
}

type Rep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Rep"`

	*MBObject

	ID    *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff *Staff          `xml:"Staff,omitempty"`
}

type ClientRelationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientRelationship"`

	*MBObject

	RelatedClient    *Client       `xml:"RelatedClient,omitempty"`
	Relationship     *Relationship `xml:"Relationship,omitempty"`
	RelationshipName string        `xml:"RelationshipName,omitempty"`
}

type Client struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Client"`

	*MBObject

	NewID                            string                     `xml:"NewID,omitempty"`
	AccountBalance                   *mb.CustomFloat64          `xml:"AccountBalance,omitempty"`
	ClientIndexes                    *ArrayOfClientIndex        `xml:"ClientIndexes,omitempty"`
	Username                         string                     `xml:"Username,omitempty"`
	Password                         string                     `xml:"Password,omitempty"`
	Notes                            string                     `xml:"Notes,omitempty"`
	MobileProvider                   *mb.CustomInt32            `xml:"MobileProvider,omitempty"`
	ClientCreditCard                 *ClientCreditCard          `xml:"ClientCreditCard,omitempty"`
	LastFormulaNotes                 string                     `xml:"LastFormulaNotes,omitempty"`
	AppointmentGenderPreference      string                     `xml:"AppointmentGenderPreference,omitempty"`
	Gender                           string                     `xml:"Gender,omitempty"`
	IsCompany                        bool                       `xml:"IsCompany,omitempty"`
	Inactive                         bool                       `xml:"Inactive,omitempty"`
	ClientRelationships              *ArrayOfClientRelationship `xml:"ClientRelationships,omitempty"`
	Reps                             *ArrayOfRep                `xml:"Reps,omitempty"`
	SaleReps                         *ArrayOfSalesRep           `xml:"SaleReps,omitempty"`
	CustomClientFields               *ArrayOfCustomClientField  `xml:"CustomClientFields,omitempty"`
	LiabilityRelease                 bool                       `xml:"LiabilityRelease,omitempty"`
	EmergencyContactInfoName         string                     `xml:"EmergencyContactInfoName,omitempty"`
	EmergencyContactInfoRelationship string                     `xml:"EmergencyContactInfoRelationship,omitempty"`
	EmergencyContactInfoPhone        string                     `xml:"EmergencyContactInfoPhone,omitempty"`
	EmergencyContactInfoEmail        string                     `xml:"EmergencyContactInfoEmail,omitempty"`
	PromotionalEmailOptIn            bool                       `xml:"PromotionalEmailOptIn,omitempty"`
	CreationDate                     *mb.CustomTime             `xml:"CreationDate,omitempty"`
	Liability                        *Liability                 `xml:"Liability,omitempty"`
	ProspectStage                    *ProspectStage             `xml:"ProspectStage,omitempty"`
	UniqueID                         *mb.CustomInt64            `xml:"UniqueID,omitempty"`
	MembershipIcon                   *mb.CustomInt32            `xml:"MembershipIcon,omitempty"`
	Action                           *ActionCode                `xml:"Action,omitempty"`
	ID                               string                     `xml:"ID,omitempty"`
	FirstName                        string                     `xml:"FirstName,omitempty"`
	MiddleName                       string                     `xml:"MiddleName,omitempty"`
	LastName                         string                     `xml:"LastName,omitempty"`
	Email                            string                     `xml:"Email,omitempty"`
	EmailOptIn                       bool                       `xml:"EmailOptIn,omitempty"`
	AddressLine1                     string                     `xml:"AddressLine1,omitempty"`
	AddressLine2                     string                     `xml:"AddressLine2,omitempty"`
	City                             string                     `xml:"City,omitempty"`
	State                            string                     `xml:"State,omitempty"`
	PostalCode                       string                     `xml:"PostalCode,omitempty"`
	Country                          string                     `xml:"Country,omitempty"`
	MobilePhone                      string                     `xml:"MobilePhone,omitempty"`
	HomePhone                        string                     `xml:"HomePhone,omitempty"`
	WorkPhone                        string                     `xml:"WorkPhone,omitempty"`
	WorkExtension                    string                     `xml:"WorkExtension,omitempty"`
	BirthDate                        *mb.CustomTime             `xml:"BirthDate,omitempty"`
	FirstAppointmentDate             *mb.CustomTime             `xml:"FirstAppointmentDate,omitempty"`
	ReferredBy                       string                     `xml:"ReferredBy,omitempty"`
	HomeLocation                     *Location                  `xml:"HomeLocation,omitempty"`
	YellowAlert                      string                     `xml:"YellowAlert,omitempty"`
	RedAlert                         string                     `xml:"RedAlert,omitempty"`
	PhotoURL                         string                     `xml:"PhotoURL,omitempty"`
	IsProspect                       bool                       `xml:"IsProspect,omitempty"`
	Status                           string                     `xml:"Status,omitempty"`
	ContactMethod                    *mb.CustomInt16            `xml:"ContactMethod,omitempty"`
}

type ArrayOfClientIndex struct {
	ClientIndex []*ClientIndex `xml:"ClientIndex,omitempty"`
}

type ClientIndex struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndex"`

	*MBObject

	RequiredBusinessMode bool                     `xml:"RequiredBusinessMode,omitempty"`
	RequiredConsumerMode bool                     `xml:"RequiredConsumerMode,omitempty"`
	Action               *ActionCode              `xml:"Action,omitempty"`
	ID                   *mb.CustomInt32          `xml:"ID,omitempty"`
	Name                 string                   `xml:"Name,omitempty"`
	Values               *ArrayOfClientIndexValue `xml:"Values,omitempty"`
}

type ArrayOfClientIndexValue struct {
	ClientIndexValue []*ClientIndexValue `xml:"ClientIndexValue,omitempty"`
}

type ClientIndexValue struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndexValue"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
	Active bool            `xml:"Active,omitempty"`
}

type ClientCreditCard struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientCreditCard"`

	CardType   string `xml:"CardType,omitempty"`
	LastFour   string `xml:"LastFour,omitempty"`
	CardNumber string `xml:"CardNumber,omitempty"`
	CardHolder string `xml:"CardHolder,omitempty"`
	ExpMonth   string `xml:"ExpMonth,omitempty"`
	ExpYear    string `xml:"ExpYear,omitempty"`
	Address    string `xml:"Address,omitempty"`
	City       string `xml:"City,omitempty"`
	State      string `xml:"State,omitempty"`
	PostalCode string `xml:"PostalCode,omitempty"`
}

type ArrayOfClientRelationship struct {
	ClientRelationship []*ClientRelationship `xml:"ClientRelationship,omitempty"`
}

type ArrayOfRep struct {
	Rep []*Rep `xml:"Rep,omitempty"`
}

type ArrayOfSalesRep struct {
	SalesRep []*SalesRep `xml:"SalesRep,omitempty"`
}

type ArrayOfCustomClientField struct {
	CustomClientField []*CustomClientField `xml:"CustomClientField,omitempty"`
}

type CustomClientField struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CustomClientField"`

	ID       *mb.CustomInt32 `xml:"ID,omitempty"`
	DataType string          `xml:"DataType,omitempty"`
	Name     string          `xml:"Name,omitempty"`
	Value    string          `xml:"Value,omitempty"`
}

type Liability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Liability"`

	IsReleased    bool            `xml:"IsReleased,omitempty"`
	AgreementDate *mb.CustomTime  `xml:"AgreementDate,omitempty"`
	ReleasedBy    *mb.CustomInt64 `xml:"ReleasedBy,omitempty"`
}

type ProspectStage struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProspectStage"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type Location struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Location"`

	*MBObject

	BusinessID          *mb.CustomInt32   `xml:"BusinessID,omitempty"`
	SiteID              *mb.CustomInt32   `xml:"SiteID,omitempty"`
	BusinessDescription string            `xml:"BusinessDescription,omitempty"`
	AdditionalImageURLs *ArrayOfString    `xml:"AdditionalImageURLs,omitempty"`
	FacilitySquareFeet  *mb.CustomInt32   `xml:"FacilitySquareFeet,omitempty"`
	TreatmentRooms      *mb.CustomInt32   `xml:"TreatmentRooms,omitempty"`
	ProSpaFinderSite    bool              `xml:"ProSpaFinderSite,omitempty"`
	HasClasses          bool              `xml:"HasClasses,omitempty"`
	PhoneExtension      string            `xml:"PhoneExtension,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                string            `xml:"Name,omitempty"`
	Address             string            `xml:"Address,omitempty"`
	Address2            string            `xml:"Address2,omitempty"`
	Tax1                *mb.CustomFloat32 `xml:"Tax1,omitempty"`
	Tax2                *mb.CustomFloat32 `xml:"Tax2,omitempty"`
	Tax3                *mb.CustomFloat32 `xml:"Tax3,omitempty"`
	Tax4                *mb.CustomFloat32 `xml:"Tax4,omitempty"`
	Tax5                *mb.CustomFloat32 `xml:"Tax5,omitempty"`
	Phone               string            `xml:"Phone,omitempty"`
	City                string            `xml:"City,omitempty"`
	StateProvCode       string            `xml:"StateProvCode,omitempty"`
	PostalCode          string            `xml:"PostalCode,omitempty"`
	Latitude            *mb.CustomFloat64 `xml:"Latitude,omitempty"`
	Longitude           *mb.CustomFloat64 `xml:"Longitude,omitempty"`
	DistanceInMiles     *mb.CustomFloat64 `xml:"DistanceInMiles,omitempty"`
	ImageURL            string            `xml:"ImageURL,omitempty"`
	Description         string            `xml:"Description,omitempty"`
	HasSite             bool              `xml:"HasSite,omitempty"`
	CanBook             bool              `xml:"CanBook,omitempty"`
}

type Relationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Relationship"`

	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	RelationshipName1 string          `xml:"RelationshipName1,omitempty"`
	RelationshipName2 string          `xml:"RelationshipName2,omitempty"`
}

type SessionType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SessionType"`

	*MBObject

	DefaultTimeLength *mb.CustomInt32 `xml:"DefaultTimeLength,omitempty"`
	ProgramID         *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	NumDeducted       *mb.CustomInt32 `xml:"NumDeducted,omitempty"`
	Action            *ActionCode     `xml:"Action,omitempty"`
	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	Name              string          `xml:"Name,omitempty"`
}

type ScheduleItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ScheduleItem"`

	*MBObject
}

type Appointment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Appointment"`

	*ScheduleItem

	GenderPreference string             `xml:"GenderPreference,omitempty"`
	Duration         *mb.CustomInt32    `xml:"Duration,omitempty"`
	ProviderID       string             `xml:"ProviderID,omitempty"`
	Action           *ActionCode        `xml:"Action,omitempty"`
	ID               *mb.CustomInt64    `xml:"ID,omitempty"`
	Status           *AppointmentStatus `xml:"Status,omitempty"`
	StartDateTime    *mb.CustomTime     `xml:"StartDateTime,omitempty"`
	EndDateTime      *mb.CustomTime     `xml:"EndDateTime,omitempty"`
	Notes            string             `xml:"Notes,omitempty"`
	StaffRequested   bool               `xml:"StaffRequested,omitempty"`
	Program          *Program           `xml:"Program,omitempty"`
	SessionType      *SessionType       `xml:"SessionType,omitempty"`
	Location         *Location          `xml:"Location,omitempty"`
	Staff            *Staff             `xml:"Staff,omitempty"`
	Client           *Client            `xml:"Client,omitempty"`
	FirstAppointment bool               `xml:"FirstAppointment,omitempty"`
	ClientService    *ClientService     `xml:"ClientService,omitempty"`
	Resources        *ArrayOfResource   `xml:"Resources,omitempty"`
}

type ArrayOfResource struct {
	Resource []*Resource `xml:"Resource,omitempty"`
}

type Unavailability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Unavailability"`

	*ScheduleItem

	ID            *mb.CustomInt32 `xml:"ID,omitempty"`
	StartDateTime *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime   *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Description   string          `xml:"Description,omitempty"`
}

type Availability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Availability"`

	*ScheduleItem

	ID                  *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff               *Staff          `xml:"Staff,omitempty"`
	SessionType         *SessionType    `xml:"SessionType,omitempty"`
	Programs            *ArrayOfProgram `xml:"Programs,omitempty"`
	StartDateTime       *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	BookableEndDateTime *mb.CustomTime  `xml:"BookableEndDateTime,omitempty"`
	Location            *Location       `xml:"Location,omitempty"`
}

type ArrayOfProgram struct {
	Program []*Program `xml:"Program,omitempty"`
}

type ArrayOfAppointment struct {
	Appointment []*Appointment `xml:"Appointment,omitempty"`
}

type ArrayOfUnavailability struct {
	Unavailability []*Unavailability `xml:"Unavailability,omitempty"`
}

type ArrayOfAvailability struct {
	Availability []*Availability `xml:"Availability,omitempty"`
}

type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty"`
}

type ArrayOfProviderIDUpdate struct {
	ProviderIDUpdate []*ProviderIDUpdate `xml:"ProviderIDUpdate,omitempty"`
}

type ProviderIDUpdate struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProviderIDUpdate"`

	OldProviderID string `xml:"OldProviderID,omitempty"`
	NewProviderID string `xml:"NewProviderID,omitempty"`
}

type GetStaffPermissionsRequest struct {
	*MBRequest

	StaffID *mb.CustomInt64 `xml:"StaffID,omitempty"`
}

type GetStaffPermissionsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffPermissionsResult"`

	*MBResult

	Permissions *ArrayOfPermission `xml:"Permissions,omitempty"`
}

type ArrayOfPermission struct {
	Permission []*Permission `xml:"Permission,omitempty"`
}

type Permission struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Permission"`

	DisplayName string `xml:"DisplayName,omitempty"`
	Name        string `xml:"Name,omitempty"`
	Value       string `xml:"Value,omitempty"`
}

type AddOrUpdateStaffRequest struct {
	*MBRequest

	UpdateAction string        `xml:"UpdateAction,omitempty"`
	Test         bool          `xml:"Test,omitempty"`
	Staff        *ArrayOfStaff `xml:"Staff,omitempty"`
}

type AddOrUpdateStaffResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddOrUpdateStaffResult"`

	*MBResult

	Staff *ArrayOfStaff `xml:"Staff,omitempty"`
}

type GetStaffImgURLRequest struct {
	*MBRequest

	StaffID *mb.CustomInt64 `xml:"StaffID,omitempty"`
}

type GetStaffImgURLResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetStaffImgURLResult"`

	*MBResult

	ImageURL       string `xml:"ImageURL,omitempty"`
	MobileImageURL string `xml:"MobileImageURL,omitempty"`
}

type ValidateLoginRequest struct {
	*MBRequest

	Username string `xml:"Username,omitempty"`
	Password string `xml:"Password,omitempty"`
}

type ValidateLoginResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ValidateLoginResult"`

	*MBResult

	GUID   string  `xml:"GUID,omitempty"`
	Client *Client `xml:"Client,omitempty"`
	Staff  *Staff  `xml:"Staff,omitempty"`
}

type GetSalesRepsRequest struct {
	*MBRequest

	SalesRepNumbers *ArrayOfInt `xml:"SalesRepNumbers,omitempty"`
	ShowActiveOnly  bool        `xml:"ShowActiveOnly,omitempty"`
}

type GetSalesRepsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSalesRepsResult"`

	*MBResult

	SalesReps *ArrayOfSalesRep `xml:"SalesReps,omitempty"`
}

type Staff_x0020_ServiceSoap struct {
	client *SOAPClient
}

func NewStaff_x0020_ServiceSoap(url string, tls bool, auth *BasicAuth) *Staff_x0020_ServiceSoap {
	if url == "" {
		url = "https://api.mindbodyonline.com/0_5/StaffService.asmx"
	}
	client := NewSOAPClient(url, tls, auth)

	return &Staff_x0020_ServiceSoap{
		client: client,
	}
}

/* Gets a list of staff members. */
func (service *Staff_x0020_ServiceSoap) GetStaff(request *GetStaff) (*GetStaffResponse, error) {
	response := new(GetStaffResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetStaff", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of staff permissions based on the given staff member. */
func (service *Staff_x0020_ServiceSoap) GetStaffPermissions(request *GetStaffPermissions) (*GetStaffPermissionsResponse, error) {
	response := new(GetStaffPermissionsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetStaffPermissions", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Add or update staff. */
func (service *Staff_x0020_ServiceSoap) AddOrUpdateStaff(request *AddOrUpdateStaff) (*AddOrUpdateStaffResponse, error) {
	response := new(AddOrUpdateStaffResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddOrUpdateStaff", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a staff member's image URL if it exists. */
func (service *Staff_x0020_ServiceSoap) GetStaffImgURL(request *GetStaffImgURL) (*GetStaffImgURLResponse, error) {
	response := new(GetStaffImgURLResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetStaffImgURL", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Validates a username and password. This method returns the staff on success. */
func (service *Staff_x0020_ServiceSoap) ValidateStaffLogin(request *ValidateStaffLogin) (*ValidateStaffLoginResponse, error) {
	response := new(ValidateStaffLoginResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/ValidateStaffLogin", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of sales reps based on the requested rep IDs */
func (service *Staff_x0020_ServiceSoap) GetSalesReps(request *GetSalesReps) (*GetSalesRepsResponse, error) {
	response := new(GetSalesRepsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetSalesReps", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
	//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Println(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Println("empty response")
		return nil
	}

	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}
