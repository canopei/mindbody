package site

import (
	mb "bitbucket.org/canopei/mindbody"
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

// against "unused imports"
var _ xml.Name

type XMLDetailLevel string

const (
	XMLDetailLevelBare XMLDetailLevel = "Bare"

	XMLDetailLevelBasic XMLDetailLevel = "Basic"

	XMLDetailLevelFull XMLDetailLevel = "Full"
)

type StatusCode string

const (
	StatusCodeSuccess StatusCode = "Success"

	StatusCodeInvalidCredentials StatusCode = "InvalidCredentials"

	StatusCodeInvalidParameters StatusCode = "InvalidParameters"

	StatusCodeInternalException StatusCode = "InternalException"

	StatusCodeUnknown StatusCode = "Unknown"

	StatusCodeFailedAction StatusCode = "FailedAction"
)

type ActionCode string

const (
	ActionCodeNone ActionCode = "None"

	ActionCodeAdded ActionCode = "Added"

	ActionCodeUpdated ActionCode = "Updated"

	ActionCodeFailed ActionCode = "Failed"

	ActionCodeRemoved ActionCode = "Removed"
)

type ScheduleType string

const (
	ScheduleTypeAll ScheduleType = "All"

	ScheduleTypeDropIn ScheduleType = "DropIn"

	ScheduleTypeEnrollment ScheduleType = "Enrollment"

	ScheduleTypeAppointment ScheduleType = "Appointment"

	ScheduleTypeResource ScheduleType = "Resource"

	ScheduleTypeMedia ScheduleType = "Media"

	ScheduleTypeArrival ScheduleType = "Arrival"
)

type GetSites struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSites"`

	Request *GetSitesRequest `xml:"Request,omitempty"`
}

type GetSitesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSitesResponse"`

	GetSitesResult *GetSitesResult `xml:"GetSitesResult,omitempty"`
}

type GetLocations struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetLocations"`

	Request *GetLocationsRequest `xml:"Request,omitempty"`
}

type GetLocationsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetLocationsResponse"`

	GetLocationsResult *GetLocationsResult `xml:"GetLocationsResult,omitempty"`
}

type GetActivationCode struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActivationCode"`

	Request *GetActivationCodeRequest `xml:"Request,omitempty"`
}

type GetActivationCodeResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActivationCodeResponse"`

	GetActivationCodeResult *GetActivationCodeResult `xml:"GetActivationCodeResult,omitempty"`
}

type GetPrograms struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetPrograms"`

	Request *GetProgramsRequest `xml:"Request,omitempty"`
}

type GetProgramsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProgramsResponse"`

	GetProgramsResult *GetProgramsResult `xml:"GetProgramsResult,omitempty"`
}

type GetSessionTypes struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSessionTypes"`

	Request *GetSessionTypesRequest `xml:"Request,omitempty"`
}

type GetSessionTypesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSessionTypesResponse"`

	GetSessionTypesResult *GetSessionTypesResult `xml:"GetSessionTypesResult,omitempty"`
}

type GetResources struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResources"`

	Request *GetResourcesRequest `xml:"Request,omitempty"`
}

type GetResourcesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResourcesResponse"`

	GetResourcesResult *GetResourcesResult `xml:"GetResourcesResult,omitempty"`
}

type GetRelationships struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRelationships"`

	Request *GetRelationshipsRequest `xml:"Request,omitempty"`
}

type GetRelationshipsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRelationshipsResponse"`

	GetRelationshipsResult *GetRelationshipsResult `xml:"GetRelationshipsResult,omitempty"`
}

type GetResourceSchedule struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResourceSchedule"`

	Request *GetResourceScheduleRequest `xml:"Request,omitempty"`
}

type GetResourceScheduleResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResourceScheduleResponse"`

	GetResourceScheduleResult *GetResourceScheduleResult `xml:"GetResourceScheduleResult,omitempty"`
}

type ReserveResource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReserveResource"`

	Request *ReserveResourceRequest `xml:"Request,omitempty"`
}

type ReserveResourceResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReserveResourceResponse"`

	ReserveResourceResult *ReserveResourceResult `xml:"ReserveResourceResult,omitempty"`
}

type GetMobileProviders struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetMobileProviders"`

	Request *GetMobileProvidersRequest `xml:"Request,omitempty"`
}

type GetMobileProvidersResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetMobileProvidersResponse"`

	GetMobileProvidersResult *GetMobileProvidersResult `xml:"GetMobileProvidersResult,omitempty"`
}

type GetProspectStages struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProspectStages"`

	Request *GetProspectStagesRequest `xml:"Request,omitempty"`
}

type GetProspectStagesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProspectStagesResponse"`

	GetProspectStagesResult *GetProspectStagesResult `xml:"GetProspectStagesResult,omitempty"`
}

type GetGenders struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetGenders"`

	Request *GetGendersRequest `xml:"Request,omitempty"`
}

type GetGendersResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetGendersResponse"`

	GetGendersResult *GetGendersResult `xml:"GetGendersResult,omitempty"`
}

type GetSitesRequest struct {
	*MBRequest

	SearchText       string          `xml:"SearchText,omitempty"`
	RelatedSiteID    *mb.CustomInt32 `xml:"RelatedSiteID,omitempty"`
	ShowOnlyTotalWOD bool            `xml:"ShowOnlyTotalWOD,omitempty"`
}

type MBRequest struct {
	SourceCredentials *SourceCredentials `xml:"SourceCredentials,omitempty"`
	UserCredentials   *UserCredentials   `xml:"UserCredentials,omitempty"`
	XMLDetail         *XMLDetailLevel    `xml:"XMLDetail,omitempty"`
	PageSize          *mb.CustomInt32    `xml:"PageSize,omitempty"`
	CurrentPageIndex  *mb.CustomInt32    `xml:"CurrentPageIndex,omitempty"`
	Fields            *ArrayOfString     `xml:"Fields,omitempty"`
}

type SourceCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SourceCredentials"`

	SourceName string      `xml:"SourceName,omitempty"`
	Password   string      `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfInt struct {
	Int []*mb.CustomInt32 `xml:"int,omitempty"`
}

type UserCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UserCredentials"`

	Username   string          `xml:"Username,omitempty"`
	Password   string          `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt     `xml:"SiteIDs,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type ArrayOfString struct {
	String []string `xml:"string,omitempty"`
}

type GetSitesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSitesResult"`

	*MBResult

	Sites *ArrayOfSite `xml:"Sites,omitempty"`
}

type MBResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBResult"`

	Status           *StatusCode     `xml:"Status,omitempty"`
	ErrorCode        *mb.CustomInt32 `xml:"ErrorCode,omitempty"`
	Message          string          `xml:"Message,omitempty"`
	XMLDetail        *XMLDetailLevel `xml:"XMLDetail,omitempty"`
	ResultCount      *mb.CustomInt32 `xml:"ResultCount,omitempty"`
	CurrentPageIndex *mb.CustomInt32 `xml:"CurrentPageIndex,omitempty"`
	TotalPageCount   *mb.CustomInt32 `xml:"TotalPageCount,omitempty"`
}

type ArrayOfSite struct {
	Site []*Site `xml:"Site,omitempty"`
}

type Site struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Site"`

	ID                     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name                   string          `xml:"Name,omitempty"`
	Description            string          `xml:"Description,omitempty"`
	LogoURL                string          `xml:"LogoURL,omitempty"`
	PageColor1             string          `xml:"PageColor1,omitempty"`
	PageColor2             string          `xml:"PageColor2,omitempty"`
	PageColor3             string          `xml:"PageColor3,omitempty"`
	PageColor4             string          `xml:"PageColor4,omitempty"`
	AcceptsVisa            bool            `xml:"AcceptsVisa,omitempty"`
	AcceptsDiscover        bool            `xml:"AcceptsDiscover,omitempty"`
	AcceptsMasterCard      bool            `xml:"AcceptsMasterCard,omitempty"`
	AcceptsAmericanExpress bool            `xml:"AcceptsAmericanExpress,omitempty"`
	ContactEmail           string          `xml:"ContactEmail,omitempty"`
	ESA                    bool            `xml:"ESA,omitempty"`
	TotalWOD               bool            `xml:"TotalWOD,omitempty"`
	TaxInclusivePrices     bool            `xml:"TaxInclusivePrices,omitempty"`
	SMSPackageEnabled      bool            `xml:"SMSPackageEnabled,omitempty"`
	AllowsDashboardAccess  bool            `xml:"AllowsDashboardAccess,omitempty"`
	PricingLevel           string          `xml:"PricingLevel,omitempty"`
}

type GetLocationsRequest struct {
	*MBRequest
}

type GetLocationsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetLocationsResult"`

	*MBResult

	Locations *ArrayOfLocation `xml:"Locations,omitempty"`
}

type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty"`
}

type Location struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Location"`

	*MBObject

	BusinessID          *mb.CustomInt32   `xml:"BusinessID,omitempty"`
	SiteID              *mb.CustomInt32   `xml:"SiteID,omitempty"`
	BusinessDescription string            `xml:"BusinessDescription,omitempty"`
	AdditionalImageURLs *ArrayOfString    `xml:"AdditionalImageURLs,omitempty"`
	FacilitySquareFeet  *mb.CustomInt32   `xml:"FacilitySquareFeet,omitempty"`
	TreatmentRooms      *mb.CustomInt32   `xml:"TreatmentRooms,omitempty"`
	ProSpaFinderSite    bool              `xml:"ProSpaFinderSite,omitempty"`
	HasClasses          bool              `xml:"HasClasses,omitempty"`
	PhoneExtension      string            `xml:"PhoneExtension,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                string            `xml:"Name,omitempty"`
	Address             string            `xml:"Address,omitempty"`
	Address2            string            `xml:"Address2,omitempty"`
	Tax1                *mb.CustomFloat32 `xml:"Tax1,omitempty"`
	Tax2                *mb.CustomFloat32 `xml:"Tax2,omitempty"`
	Tax3                *mb.CustomFloat32 `xml:"Tax3,omitempty"`
	Tax4                *mb.CustomFloat32 `xml:"Tax4,omitempty"`
	Tax5                *mb.CustomFloat32 `xml:"Tax5,omitempty"`
	Phone               string            `xml:"Phone,omitempty"`
	City                string            `xml:"City,omitempty"`
	StateProvCode       string            `xml:"StateProvCode,omitempty"`
	PostalCode          string            `xml:"PostalCode,omitempty"`
	Latitude            *mb.CustomFloat64 `xml:"Latitude,omitempty"`
	Longitude           *mb.CustomFloat64 `xml:"Longitude,omitempty"`
	DistanceInMiles     *mb.CustomFloat64 `xml:"DistanceInMiles,omitempty"`
	ImageURL            string            `xml:"ImageURL,omitempty"`
	Description         string            `xml:"Description,omitempty"`
	HasSite             bool              `xml:"HasSite,omitempty"`
	CanBook             bool              `xml:"CanBook,omitempty"`
}

type MBObject struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBObject"`
	XSIType string   `xml:"xsi:type,attr,omitempty"`

	Site      *Site          `xml:"Site,omitempty"`
	Messages  *ArrayOfString `xml:"Messages,omitempty"`
	Execute   string         `xml:"Execute,omitempty"`
	ErrorCode string         `xml:"ErrorCode,omitempty"`
}

type GetActivationCodeRequest struct {
	*MBRequest
}

type GetActivationCodeResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetActivationCodeResult"`

	*MBResult

	ActivationCode string `xml:"ActivationCode,omitempty"`
	ActivationLink string `xml:"ActivationLink,omitempty"`
}

type GetProgramsRequest struct {
	*MBRequest

	ScheduleType *ScheduleType `xml:"ScheduleType,omitempty"`
	OnlineOnly   bool          `xml:"OnlineOnly,omitempty"`
}

type GetProgramsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProgramsResult"`

	*MBResult

	Programs *ArrayOfProgram `xml:"Programs,omitempty"`
}

type ArrayOfProgram struct {
	Program []*Program `xml:"Program,omitempty"`
}

type Program struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Program"`

	*MBObject

	ID           *mb.CustomInt32 `xml:"ID,omitempty"`
	Name         string          `xml:"Name,omitempty"`
	ScheduleType *ScheduleType   `xml:"ScheduleType,omitempty"`
	CancelOffset *mb.CustomInt32 `xml:"CancelOffset,omitempty"`
}

type GetSessionTypesRequest struct {
	*MBRequest

	ProgramIDs *ArrayOfInt `xml:"ProgramIDs,omitempty"`
	OnlineOnly bool        `xml:"OnlineOnly,omitempty"`
}

type GetSessionTypesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSessionTypesResult"`

	*MBResult

	SessionTypes *ArrayOfSessionType `xml:"SessionTypes,omitempty"`
}

type ArrayOfSessionType struct {
	SessionType []*SessionType `xml:"SessionType,omitempty"`
}

type SessionType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SessionType"`

	*MBObject

	DefaultTimeLength *mb.CustomInt32 `xml:"DefaultTimeLength,omitempty"`
	ProgramID         *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	NumDeducted       *mb.CustomInt32 `xml:"NumDeducted,omitempty"`
	Action            *ActionCode     `xml:"Action,omitempty"`
	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	Name              string          `xml:"Name,omitempty"`
}

type GetResourcesRequest struct {
	*MBRequest

	SessionTypeIDs *ArrayOfInt     `xml:"SessionTypeIDs,omitempty"`
	LocationID     *mb.CustomInt32 `xml:"LocationID,omitempty"`
	StartDateTime  *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime    *mb.CustomTime  `xml:"EndDateTime,omitempty"`
}

type GetResourcesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResourcesResult"`

	*MBResult

	Resources *ArrayOfResource `xml:"Resources,omitempty"`
}

type ArrayOfResource struct {
	Resource []*Resource `xml:"Resource,omitempty"`
}

type Resource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Resource"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type GetRelationshipsRequest struct {
	*MBRequest
}

type GetRelationshipsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetRelationshipsResult"`

	*MBResult

	Relationships *ArrayOfRelationship `xml:"Relationships,omitempty"`
}

type ArrayOfRelationship struct {
	Relationship []*Relationship `xml:"Relationship,omitempty"`
}

type Relationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Relationship"`

	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	RelationshipName1 string          `xml:"RelationshipName1,omitempty"`
	RelationshipName2 string          `xml:"RelationshipName2,omitempty"`
}

type GetResourceScheduleRequest struct {
	*MBRequest

	Date *mb.CustomTime `xml:"Date,omitempty"`
}

type GetResourceScheduleResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetResourceScheduleResult"`

	*MBResult

	Results *RecordSet `xml:"Results,omitempty"`
}

type ReserveResourceRequest struct {
	*MBRequest

	ResourceID    *mb.CustomInt32 `xml:"ResourceID,omitempty"`
	ClientID      *mb.CustomInt32 `xml:"ClientID,omitempty"`
	StaffID       *mb.CustomInt32 `xml:"StaffID,omitempty"`
	StartDateTime *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime   *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	LocationID    *mb.CustomInt32 `xml:"LocationID,omitempty"`
	ProgramID     *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	Notes         string          `xml:"Notes,omitempty"`
}

type ReserveResourceResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ReserveResourceResult"`

	*MBResult
}

type GetMobileProvidersRequest struct {
	*MBRequest
}

type GetMobileProvidersResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetMobileProvidersResult"`

	*MBResult

	MobileProviders *ArrayOfMobileProvider `xml:"MobileProviders,omitempty"`
}

type ArrayOfMobileProvider struct {
	MobileProvider []*MobileProvider `xml:"MobileProvider,omitempty"`
}

type MobileProvider struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MobileProvider"`

	ProviderID      *mb.CustomInt32 `xml:"ProviderID,omitempty"`
	ProviderName    string          `xml:"ProviderName,omitempty"`
	ProviderAddress string          `xml:"ProviderAddress,omitempty"`
	Active          bool            `xml:"Active,omitempty"`
}

type GetProspectStagesRequest struct {
	*MBRequest

	IncludeInactive bool `xml:"IncludeInactive,omitempty"`
}

type GetProspectStagesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetProspectStagesResult"`

	*MBResult

	ProspectStages *ArrayOfProspectStage `xml:"ProspectStages,omitempty"`
}

type ArrayOfProspectStage struct {
	ProspectStage []*ProspectStage `xml:"ProspectStage,omitempty"`
}

type ProspectStage struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProspectStage"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type GetGendersRequest struct {
	*MBRequest

	IncludeInactive bool `xml:"IncludeInactive,omitempty"`
}

type GetGendersResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetGendersResult"`

	*MBResult

	GenderOptions *ArrayOfGenderOption `xml:"GenderOptions,omitempty"`
}

type ArrayOfGenderOption struct {
	GenderOption []*GenderOption `xml:"GenderOption,omitempty"`
}

type GenderOption struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GenderOption"`

	ID       *mb.CustomInt32 `xml:"ID,omitempty"`
	Name     string          `xml:"Name,omitempty"`
	IsActive bool            `xml:"IsActive,omitempty"`
	IsSystem bool            `xml:"IsSystem,omitempty"`
}

type Row struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/API/0_4 Row"`

	Content struct {
	} `xml:"Content,omitempty"`
}

type RecordSet struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/API/0_4 RecordSet"`

	Row []*Row `xml:"Row,omitempty"`
}

type Site_x0020_ServiceSoap struct {
	client *SOAPClient
}

func NewSite_x0020_ServiceSoap(url string, tls bool, auth *BasicAuth) *Site_x0020_ServiceSoap {
	if url == "" {
		url = "https://api.mindbodyonline.com/0_5/SiteService.asmx"
	}
	client := NewSOAPClient(url, tls, auth)

	return &Site_x0020_ServiceSoap{
		client: client,
	}
}

/* Gets a list of sites. */
func (service *Site_x0020_ServiceSoap) GetSites(request *GetSites) (*GetSitesResponse, error) {
	response := new(GetSitesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetSites", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of locations. */
func (service *Site_x0020_ServiceSoap) GetLocations(request *GetLocations) (*GetLocationsResponse, error) {
	response := new(GetLocationsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetLocations", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets an activation code. */
func (service *Site_x0020_ServiceSoap) GetActivationCode(request *GetActivationCode) (*GetActivationCodeResponse, error) {
	response := new(GetActivationCodeResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetActivationCode", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of programs. */
func (service *Site_x0020_ServiceSoap) GetPrograms(request *GetPrograms) (*GetProgramsResponse, error) {
	response := new(GetProgramsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetPrograms", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of session types. */
func (service *Site_x0020_ServiceSoap) GetSessionTypes(request *GetSessionTypes) (*GetSessionTypesResponse, error) {
	response := new(GetSessionTypesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetSessionTypes", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of resources. */
func (service *Site_x0020_ServiceSoap) GetResources(request *GetResources) (*GetResourcesResponse, error) {
	response := new(GetResourcesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetResources", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of relationships. */
func (service *Site_x0020_ServiceSoap) GetRelationships(request *GetRelationships) (*GetRelationshipsResponse, error) {
	response := new(GetRelationshipsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetRelationships", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets all resources schedule. */
func (service *Site_x0020_ServiceSoap) GetResourceSchedule(request *GetResourceSchedule) (*GetResourceScheduleResponse, error) {
	response := new(GetResourceScheduleResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetResourceSchedule", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Reserves a resource. */
func (service *Site_x0020_ServiceSoap) ReserveResource(request *ReserveResource) (*ReserveResourceResponse, error) {
	response := new(ReserveResourceResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/ReserveResource", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of active mobile providers. */
func (service *Site_x0020_ServiceSoap) GetMobileProviders(request *GetMobileProviders) (*GetMobileProvidersResponse, error) {
	response := new(GetMobileProvidersResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetMobileProviders", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of prospect stages for a site. */
func (service *Site_x0020_ServiceSoap) GetProspectStages(request *GetProspectStages) (*GetProspectStagesResponse, error) {
	response := new(GetProspectStagesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetProspectStages", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of prospect stages for a site. */
func (service *Site_x0020_ServiceSoap) GetGenders(request *GetGenders) (*GetGendersResponse, error) {
	response := new(GetGendersResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetGenders", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
	//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Println(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Println("empty response")
		return nil
	}

	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}
