package class

import (
	mb "bitbucket.org/canopei/mindbody"
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"time"
)

// against "unused imports"
var _ xml.Name

type XMLDetailLevel string

const (
	XMLDetailLevelBare XMLDetailLevel = "Bare"

	XMLDetailLevelBasic XMLDetailLevel = "Basic"

	XMLDetailLevelFull XMLDetailLevel = "Full"
)

type StatusCode string

const (
	StatusCodeSuccess StatusCode = "Success"

	StatusCodeInvalidCredentials StatusCode = "InvalidCredentials"

	StatusCodeInvalidParameters StatusCode = "InvalidParameters"

	StatusCodeInternalException StatusCode = "InternalException"

	StatusCodeUnknown StatusCode = "Unknown"

	StatusCodeFailedAction StatusCode = "FailedAction"
)

type ActionCode string

const (
	ActionCodeNone ActionCode = "None"

	ActionCodeAdded ActionCode = "Added"

	ActionCodeUpdated ActionCode = "Updated"

	ActionCodeFailed ActionCode = "Failed"

	ActionCodeRemoved ActionCode = "Removed"
)

type ScheduleType string

const (
	ScheduleTypeAll ScheduleType = "All"

	ScheduleTypeDropIn ScheduleType = "DropIn"

	ScheduleTypeEnrollment ScheduleType = "Enrollment"

	ScheduleTypeAppointment ScheduleType = "Appointment"

	ScheduleTypeResource ScheduleType = "Resource"

	ScheduleTypeMedia ScheduleType = "Media"

	ScheduleTypeArrival ScheduleType = "Arrival"
)

type AppointmentStatus string

const (
	AppointmentStatusBooked AppointmentStatus = "Booked"

	AppointmentStatusCompleted AppointmentStatus = "Completed"

	AppointmentStatusConfirmed AppointmentStatus = "Confirmed"

	AppointmentStatusArrived AppointmentStatus = "Arrived"

	AppointmentStatusNoShow AppointmentStatus = "NoShow"

	AppointmentStatusCancelled AppointmentStatus = "Cancelled"

	AppointmentStatusLateCancelled AppointmentStatus = "LateCancelled"
)

type GetClasses struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClasses"`

	Request *GetClassesRequest `xml:"Request,omitempty"`
}

type GetClassesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassesResponse"`

	GetClassesResult *GetClassesResult `xml:"GetClassesResult,omitempty"`
}

type UpdateClientVisits struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientVisits"`

	Request *UpdateClientVisitsRequest `xml:"Request,omitempty"`
}

type UpdateClientVisitsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientVisitsResponse"`

	UpdateClientVisitsResult *UpdateClientVisitsResult `xml:"UpdateClientVisitsResult,omitempty"`
}

type GetClassVisits struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassVisits"`

	Request *GetClassVisitsRequest `xml:"Request,omitempty"`
}

type GetClassVisitsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassVisitsResponse"`

	GetClassVisitsResult *GetClassVisitsResult `xml:"GetClassVisitsResult,omitempty"`
}

type GetClassDescriptions struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassDescriptions"`

	Request *GetClassDescriptionsRequest `xml:"Request,omitempty"`
}

type GetClassDescriptionsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassDescriptionsResponse"`

	GetClassDescriptionsResult *GetClassDescriptionsResult `xml:"GetClassDescriptionsResult,omitempty"`
}

type GetEnrollments struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetEnrollments"`

	Request *GetEnrollmentsRequest `xml:"Request,omitempty"`
}

type GetEnrollmentsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetEnrollmentsResponse"`

	GetEnrollmentsResult *GetEnrollmentsResult `xml:"GetEnrollmentsResult,omitempty"`
}

type GetClassSchedules struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassSchedules"`

	Request *GetClassSchedulesRequest `xml:"Request,omitempty"`
}

type GetClassSchedulesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassSchedulesResponse"`

	GetClassSchedulesResult *GetClassSchedulesResult `xml:"GetClassSchedulesResult,omitempty"`
}

type AddClientsToClasses struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToClasses"`

	Request *AddClientsToClassesRequest `xml:"Request,omitempty"`
}

type AddClientsToClassesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToClassesResponse"`

	AddClientsToClassesResult *AddClientsToClassesResult `xml:"AddClientsToClassesResult,omitempty"`
}

type RemoveClientsFromClasses struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveClientsFromClasses"`

	Request *RemoveClientsFromClassesRequest `xml:"Request,omitempty"`
}

type RemoveClientsFromClassesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveClientsFromClassesResponse"`

	RemoveClientsFromClassesResult *RemoveClientsFromClassesResult `xml:"RemoveClientsFromClassesResult,omitempty"`
}

type AddClientsToEnrollments struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToEnrollments"`

	Request *AddClientsToEnrollmentsRequest `xml:"Request,omitempty"`
}

type AddClientsToEnrollmentsResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToEnrollmentsResponse"`

	AddClientsToEnrollmentsResult *AddClientsToEnrollmentsResult `xml:"AddClientsToEnrollmentsResult,omitempty"`
}

type RemoveFromWaitlist struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveFromWaitlist"`

	Request *RemoveFromWaitlistRequest `xml:"Request,omitempty"`
}

type RemoveFromWaitlistResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveFromWaitlistResponse"`

	RemoveFromWaitlistResult *RemoveFromWaitlistResult `xml:"RemoveFromWaitlistResult,omitempty"`
}

type GetSemesters struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSemesters"`

	Request *GetSemestersRequest `xml:"Request,omitempty"`
}

type GetSemestersResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSemestersResponse"`

	GetSemestersResult *GetSemestersResult `xml:"GetSemestersResult,omitempty"`
}

type GetCourses struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCourses"`

	Request *GetCoursesRequest `xml:"Request,omitempty"`
}

type GetCoursesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCoursesResponse"`

	GetCoursesResult *GetCoursesResult `xml:"GetCoursesResult,omitempty"`
}

type GetWaitlistEntries struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetWaitlistEntries"`

	Request *GetWaitlistEntriesRequest `xml:"Request,omitempty"`
}

type GetWaitlistEntriesResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetWaitlistEntriesResponse"`

	GetWaitlistEntriesResult *GetWaitlistEntriesResult `xml:"GetWaitlistEntriesResult,omitempty"`
}

type SubstituteClassTeacher struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SubstituteClassTeacher"`

	Request *SubstituteClassTeacherRequest `xml:"Request,omitempty"`
}

type SubstituteClassTeacherResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SubstituteClassTeacherResponse"`

	SubstituteClassTeacherResult *SubstituteClassTeacherResult `xml:"SubstituteClassTeacherResult,omitempty"`
}

type SubtituteClassTeacher struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SubtituteClassTeacher"`

	Request *SubstituteClassTeacherRequest `xml:"Request,omitempty"`
}

type SubtituteClassTeacherResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SubtituteClassTeacherResponse"`

	SubtituteClassTeacherResult *SubstituteClassTeacherResult `xml:"SubtituteClassTeacherResult,omitempty"`
}

type CancelSingleClass struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CancelSingleClass"`

	Request *CancelSingleClassRequest `xml:"Request,omitempty"`
}

type CancelSingleClassResponse struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CancelSingleClassResponse"`

	CancelSingleClassResult *CancelSingleClassResult `xml:"CancelSingleClassResult,omitempty"`
}

type GetClassesRequest struct {
	*MBRequest

	ClassDescriptionIDs *ArrayOfInt    `xml:"ClassDescriptionIDs,omitempty"`
	ClassIDs            *ArrayOfInt    `xml:"ClassIDs,omitempty"`
	StaffIDs            *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	StartDateTime       *mb.CustomTime `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime `xml:"EndDateTime,omitempty"`
	ClientID            string         `xml:"ClientID,omitempty"`
	ProgramIDs          *ArrayOfInt    `xml:"ProgramIDs,omitempty"`
	SessionTypeIDs      *ArrayOfInt    `xml:"SessionTypeIDs,omitempty"`
	LocationIDs         *ArrayOfInt    `xml:"LocationIDs,omitempty"`
	SemesterIDs         *ArrayOfInt    `xml:"SemesterIDs,omitempty"`
	HideCanceledClasses bool           `xml:"HideCanceledClasses,omitempty"`
	SchedulingWindow    bool           `xml:"SchedulingWindow,omitempty"`
}

type MBRequest struct {
	SourceCredentials *SourceCredentials `xml:"SourceCredentials,omitempty"`
	UserCredentials   *UserCredentials   `xml:"UserCredentials,omitempty"`
	XMLDetail         *XMLDetailLevel    `xml:"XMLDetail,omitempty"`
	PageSize          *mb.CustomInt32    `xml:"PageSize,omitempty"`
	CurrentPageIndex  *mb.CustomInt32    `xml:"CurrentPageIndex,omitempty"`
	Fields            *ArrayOfString     `xml:"Fields,omitempty"`
}

type SourceCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SourceCredentials"`

	SourceName string      `xml:"SourceName,omitempty"`
	Password   string      `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt `xml:"SiteIDs,omitempty"`
}

type ArrayOfInt struct {
	Int []*mb.CustomInt32 `xml:"int,omitempty"`
}

type UserCredentials struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UserCredentials"`

	Username   string          `xml:"Username,omitempty"`
	Password   string          `xml:"Password,omitempty"`
	SiteIDs    *ArrayOfInt     `xml:"SiteIDs,omitempty"`
	LocationID *mb.CustomInt32 `xml:"LocationID,omitempty"`
}

type ArrayOfString struct {
	String []string `xml:"string,omitempty"`
}

type ArrayOfLong struct {
	Long []*mb.CustomInt64 `xml:"long,omitempty"`
}

type GetClassesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassesResult"`

	*MBResult

	Classes *ArrayOfClass `xml:"Classes,omitempty"`
}

type MBResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBResult"`

	Status           *StatusCode     `xml:"Status,omitempty"`
	ErrorCode        *mb.CustomInt32 `xml:"ErrorCode,omitempty"`
	Message          string          `xml:"Message,omitempty"`
	XMLDetail        *XMLDetailLevel `xml:"XMLDetail,omitempty"`
	ResultCount      *mb.CustomInt32 `xml:"ResultCount,omitempty"`
	CurrentPageIndex *mb.CustomInt32 `xml:"CurrentPageIndex,omitempty"`
	TotalPageCount   *mb.CustomInt32 `xml:"TotalPageCount,omitempty"`
}

type ArrayOfClass struct {
	Class []*Class `xml:"Class,omitempty"`
}

type Class struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Class"`

	*MBObject

	ClassScheduleID     *mb.CustomInt32   `xml:"ClassScheduleID,omitempty"`
	Visits              *ArrayOfVisit     `xml:"Visits,omitempty"`
	Clients             *ArrayOfClient    `xml:"Clients,omitempty"`
	Location            *Location         `xml:"Location,omitempty"`
	Resource            *Resource         `xml:"Resource,omitempty"`
	MaxCapacity         *mb.CustomInt32   `xml:"MaxCapacity,omitempty"`
	WebCapacity         *mb.CustomInt32   `xml:"WebCapacity,omitempty"`
	TotalBooked         *mb.CustomInt32   `xml:"TotalBooked,omitempty"`
	TotalBookedWaitlist *mb.CustomInt32   `xml:"TotalBookedWaitlist,omitempty"`
	WebBooked           *mb.CustomInt32   `xml:"WebBooked,omitempty"`
	SemesterID          *mb.CustomInt32   `xml:"SemesterID,omitempty"`
	IsCanceled          bool              `xml:"IsCanceled,omitempty"`
	Substitute          bool              `xml:"Substitute,omitempty"`
	Active              bool              `xml:"Active,omitempty"`
	IsWaitlistAvailable bool              `xml:"IsWaitlistAvailable,omitempty"`
	IsEnrolled          bool              `xml:"IsEnrolled,omitempty"`
	HideCancel          bool              `xml:"HideCancel,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	IsAvailable         bool              `xml:"IsAvailable,omitempty"`
	StartDateTime       *mb.CustomTime    `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime    `xml:"EndDateTime,omitempty"`
	ClassDescription    *ClassDescription `xml:"ClassDescription,omitempty"`
	Staff               *Staff            `xml:"Staff,omitempty"`
}

type MBObject struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 MBObject"`
	XSIType string   `xml:"xsi:type,attr,omitempty"`

	Site      *Site          `xml:"Site,omitempty"`
	Messages  *ArrayOfString `xml:"Messages,omitempty"`
	Execute   string         `xml:"Execute,omitempty"`
	ErrorCode string         `xml:"ErrorCode,omitempty"`
}

type Site struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Site"`

	ID                     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name                   string          `xml:"Name,omitempty"`
	Description            string          `xml:"Description,omitempty"`
	LogoURL                string          `xml:"LogoURL,omitempty"`
	PageColor1             string          `xml:"PageColor1,omitempty"`
	PageColor2             string          `xml:"PageColor2,omitempty"`
	PageColor3             string          `xml:"PageColor3,omitempty"`
	PageColor4             string          `xml:"PageColor4,omitempty"`
	AcceptsVisa            bool            `xml:"AcceptsVisa,omitempty"`
	AcceptsDiscover        bool            `xml:"AcceptsDiscover,omitempty"`
	AcceptsMasterCard      bool            `xml:"AcceptsMasterCard,omitempty"`
	AcceptsAmericanExpress bool            `xml:"AcceptsAmericanExpress,omitempty"`
	ContactEmail           string          `xml:"ContactEmail,omitempty"`
	ESA                    bool            `xml:"ESA,omitempty"`
	TotalWOD               bool            `xml:"TotalWOD,omitempty"`
	TaxInclusivePrices     bool            `xml:"TaxInclusivePrices,omitempty"`
	SMSPackageEnabled      bool            `xml:"SMSPackageEnabled,omitempty"`
	AllowsDashboardAccess  bool            `xml:"AllowsDashboardAccess,omitempty"`
	PricingLevel           string          `xml:"PricingLevel,omitempty"`
}

type ClassDescription struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClassDescription"`

	*MBObject

	ImageURL    string          `xml:"ImageURL,omitempty"`
	Level       *Level          `xml:"Level,omitempty"`
	Action      *ActionCode     `xml:"Action,omitempty"`
	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Prereq      string          `xml:"Prereq,omitempty"`
	Notes       string          `xml:"Notes,omitempty"`
	LastUpdated *mb.CustomTime  `xml:"LastUpdated,omitempty"`
	Program     *Program        `xml:"Program,omitempty"`
	SessionType *SessionType    `xml:"SessionType,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type Level struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Level"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
}

type Program struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Program"`

	*MBObject

	ID           *mb.CustomInt32 `xml:"ID,omitempty"`
	Name         string          `xml:"Name,omitempty"`
	ScheduleType *ScheduleType   `xml:"ScheduleType,omitempty"`
	CancelOffset *mb.CustomInt32 `xml:"CancelOffset,omitempty"`
}

type SessionType struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SessionType"`

	*MBObject

	DefaultTimeLength *mb.CustomInt32 `xml:"DefaultTimeLength,omitempty"`
	ProgramID         *mb.CustomInt32 `xml:"ProgramID,omitempty"`
	NumDeducted       *mb.CustomInt32 `xml:"NumDeducted,omitempty"`
	Action            *ActionCode     `xml:"Action,omitempty"`
	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	Name              string          `xml:"Name,omitempty"`
}

type Resource struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Resource"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
}

type ClientService struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientService"`

	*MBObject

	Current        bool            `xml:"Current,omitempty"`
	Count          *mb.CustomInt32 `xml:"Count,omitempty"`
	Remaining      *mb.CustomInt32 `xml:"Remaining,omitempty"`
	Action         *ActionCode     `xml:"Action,omitempty"`
	ID             *mb.CustomInt64 `xml:"ID,omitempty"`
	Name           string          `xml:"Name,omitempty"`
	PaymentDate    *mb.CustomTime  `xml:"PaymentDate,omitempty"`
	ActiveDate     *mb.CustomTime  `xml:"ActiveDate,omitempty"`
	ExpirationDate *mb.CustomTime  `xml:"ExpirationDate,omitempty"`
	Program        *Program        `xml:"Program,omitempty"`
}

type SalesRep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SalesRep"`

	*MBObject

	SalesRepNumber  *mb.CustomInt32 `xml:"SalesRepNumber,omitempty"`
	ID              *mb.CustomInt64 `xml:"ID,omitempty"`
	FirstName       string          `xml:"FirstName,omitempty"`
	LastName        string          `xml:"LastName,omitempty"`
	SalesRepNumbers *ArrayOfInt     `xml:"SalesRepNumbers,omitempty"`
}

type Rep struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Rep"`

	*MBObject

	ID    *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff *Staff          `xml:"Staff,omitempty"`
}

type Staff struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Staff"`

	*MBObject

	Appointments             *ArrayOfAppointment      `xml:"Appointments,omitempty"`
	Unavailabilities         *ArrayOfUnavailability   `xml:"Unavailabilities,omitempty"`
	Availabilities           *ArrayOfAvailability     `xml:"Availabilities,omitempty"`
	Email                    string                   `xml:"Email,omitempty"`
	MobilePhone              string                   `xml:"MobilePhone,omitempty"`
	HomePhone                string                   `xml:"HomePhone,omitempty"`
	WorkPhone                string                   `xml:"WorkPhone,omitempty"`
	Address                  string                   `xml:"Address,omitempty"`
	Address2                 string                   `xml:"Address2,omitempty"`
	City                     string                   `xml:"City,omitempty"`
	State                    string                   `xml:"State,omitempty"`
	Country                  string                   `xml:"Country,omitempty"`
	PostalCode               string                   `xml:"PostalCode,omitempty"`
	ForeignZip               string                   `xml:"ForeignZip,omitempty"`
	SortOrder                *mb.CustomInt32          `xml:"SortOrder,omitempty"`
	LoginLocations           *ArrayOfLocation         `xml:"LoginLocations,omitempty"`
	MultiLocation            bool                     `xml:"MultiLocation,omitempty"`
	AppointmentTrn           bool                     `xml:"AppointmentTrn,omitempty"`
	ReservationTrn           bool                     `xml:"ReservationTrn,omitempty"`
	IndependentContractor    bool                     `xml:"IndependentContractor,omitempty"`
	AlwaysAllowDoubleBooking bool                     `xml:"AlwaysAllowDoubleBooking,omitempty"`
	UserAccessLevel          string                   `xml:"UserAccessLevel,omitempty"`
	ProviderIDs              *ArrayOfString           `xml:"ProviderIDs,omitempty"`
	ProviderIDUpdateList     *ArrayOfProviderIDUpdate `xml:"ProviderIDUpdateList,omitempty"`
	Action                   *ActionCode              `xml:"Action,omitempty"`
	ID                       *mb.CustomInt64          `xml:"ID,omitempty"`
	Name                     string                   `xml:"Name,omitempty"`
	FirstName                string                   `xml:"FirstName,omitempty"`
	LastName                 string                   `xml:"LastName,omitempty"`
	ImageURL                 string                   `xml:"ImageURL,omitempty"`
	Bio                      string                   `xml:"Bio,omitempty"`
	IsMale                   bool                     `xml:"isMale,omitempty"`
}

type ArrayOfAppointment struct {
	Appointment []*Appointment `xml:"Appointment,omitempty"`
}

type Appointment struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Appointment"`

	*ScheduleItem

	GenderPreference string             `xml:"GenderPreference,omitempty"`
	Duration         *mb.CustomInt32    `xml:"Duration,omitempty"`
	ProviderID       string             `xml:"ProviderID,omitempty"`
	Action           *ActionCode        `xml:"Action,omitempty"`
	ID               *mb.CustomInt64    `xml:"ID,omitempty"`
	Status           *AppointmentStatus `xml:"Status,omitempty"`
	StartDateTime    *mb.CustomTime     `xml:"StartDateTime,omitempty"`
	EndDateTime      *mb.CustomTime     `xml:"EndDateTime,omitempty"`
	Notes            string             `xml:"Notes,omitempty"`
	StaffRequested   bool               `xml:"StaffRequested,omitempty"`
	Program          *Program           `xml:"Program,omitempty"`
	SessionType      *SessionType       `xml:"SessionType,omitempty"`
	Location         *Location          `xml:"Location,omitempty"`
	Staff            *Staff             `xml:"Staff,omitempty"`
	Client           *Client            `xml:"Client,omitempty"`
	FirstAppointment bool               `xml:"FirstAppointment,omitempty"`
	ClientService    *ClientService     `xml:"ClientService,omitempty"`
	Resources        *ArrayOfResource   `xml:"Resources,omitempty"`
}

type ScheduleItem struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ScheduleItem"`

	*MBObject
}

type Unavailability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Unavailability"`

	*ScheduleItem

	ID            *mb.CustomInt32 `xml:"ID,omitempty"`
	StartDateTime *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime   *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Description   string          `xml:"Description,omitempty"`
}

type Availability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Availability"`

	*ScheduleItem

	ID                  *mb.CustomInt32 `xml:"ID,omitempty"`
	Staff               *Staff          `xml:"Staff,omitempty"`
	SessionType         *SessionType    `xml:"SessionType,omitempty"`
	Programs            *ArrayOfProgram `xml:"Programs,omitempty"`
	StartDateTime       *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	EndDateTime         *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	BookableEndDateTime *mb.CustomTime  `xml:"BookableEndDateTime,omitempty"`
	Location            *Location       `xml:"Location,omitempty"`
}

type ArrayOfProgram struct {
	Program []*Program `xml:"Program,omitempty"`
}

type Location struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Location"`

	*MBObject

	BusinessID          *mb.CustomInt32   `xml:"BusinessID,omitempty"`
	SiteID              *mb.CustomInt32   `xml:"SiteID,omitempty"`
	BusinessDescription string            `xml:"BusinessDescription,omitempty"`
	AdditionalImageURLs *ArrayOfString    `xml:"AdditionalImageURLs,omitempty"`
	FacilitySquareFeet  *mb.CustomInt32   `xml:"FacilitySquareFeet,omitempty"`
	TreatmentRooms      *mb.CustomInt32   `xml:"TreatmentRooms,omitempty"`
	ProSpaFinderSite    bool              `xml:"ProSpaFinderSite,omitempty"`
	HasClasses          bool              `xml:"HasClasses,omitempty"`
	PhoneExtension      string            `xml:"PhoneExtension,omitempty"`
	Action              *ActionCode       `xml:"Action,omitempty"`
	ID                  *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                string            `xml:"Name,omitempty"`
	Address             string            `xml:"Address,omitempty"`
	Address2            string            `xml:"Address2,omitempty"`
	Tax1                *mb.CustomFloat32 `xml:"Tax1,omitempty"`
	Tax2                *mb.CustomFloat32 `xml:"Tax2,omitempty"`
	Tax3                *mb.CustomFloat32 `xml:"Tax3,omitempty"`
	Tax4                *mb.CustomFloat32 `xml:"Tax4,omitempty"`
	Tax5                *mb.CustomFloat32 `xml:"Tax5,omitempty"`
	Phone               string            `xml:"Phone,omitempty"`
	City                string            `xml:"City,omitempty"`
	StateProvCode       string            `xml:"StateProvCode,omitempty"`
	PostalCode          string            `xml:"PostalCode,omitempty"`
	Latitude            *mb.CustomFloat64 `xml:"Latitude,omitempty"`
	Longitude           *mb.CustomFloat64 `xml:"Longitude,omitempty"`
	DistanceInMiles     *mb.CustomFloat64 `xml:"DistanceInMiles,omitempty"`
	ImageURL            string            `xml:"ImageURL,omitempty"`
	Description         string            `xml:"Description,omitempty"`
	HasSite             bool              `xml:"HasSite,omitempty"`
	CanBook             bool              `xml:"CanBook,omitempty"`
}

type Client struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Client"`

	*MBObject

	NewID                            string                     `xml:"NewID,omitempty"`
	AccountBalance                   *mb.CustomFloat64          `xml:"AccountBalance,omitempty"`
	ClientIndexes                    *ArrayOfClientIndex        `xml:"ClientIndexes,omitempty"`
	Username                         string                     `xml:"Username,omitempty"`
	Password                         string                     `xml:"Password,omitempty"`
	Notes                            string                     `xml:"Notes,omitempty"`
	MobileProvider                   *mb.CustomInt32            `xml:"MobileProvider,omitempty"`
	ClientCreditCard                 *ClientCreditCard          `xml:"ClientCreditCard,omitempty"`
	LastFormulaNotes                 string                     `xml:"LastFormulaNotes,omitempty"`
	AppointmentGenderPreference      string                     `xml:"AppointmentGenderPreference,omitempty"`
	Gender                           string                     `xml:"Gender,omitempty"`
	IsCompany                        bool                       `xml:"IsCompany,omitempty"`
	Inactive                         bool                       `xml:"Inactive,omitempty"`
	ClientRelationships              *ArrayOfClientRelationship `xml:"ClientRelationships,omitempty"`
	Reps                             *ArrayOfRep                `xml:"Reps,omitempty"`
	SaleReps                         *ArrayOfSalesRep           `xml:"SaleReps,omitempty"`
	CustomClientFields               *ArrayOfCustomClientField  `xml:"CustomClientFields,omitempty"`
	LiabilityRelease                 bool                       `xml:"LiabilityRelease,omitempty"`
	EmergencyContactInfoName         string                     `xml:"EmergencyContactInfoName,omitempty"`
	EmergencyContactInfoRelationship string                     `xml:"EmergencyContactInfoRelationship,omitempty"`
	EmergencyContactInfoPhone        string                     `xml:"EmergencyContactInfoPhone,omitempty"`
	EmergencyContactInfoEmail        string                     `xml:"EmergencyContactInfoEmail,omitempty"`
	PromotionalEmailOptIn            bool                       `xml:"PromotionalEmailOptIn,omitempty"`
	CreationDate                     *mb.CustomTime             `xml:"CreationDate,omitempty"`
	Liability                        *Liability                 `xml:"Liability,omitempty"`
	ProspectStage                    *ProspectStage             `xml:"ProspectStage,omitempty"`
	UniqueID                         *mb.CustomInt64            `xml:"UniqueID,omitempty"`
	MembershipIcon                   *mb.CustomInt32            `xml:"MembershipIcon,omitempty"`
	Action                           *ActionCode                `xml:"Action,omitempty"`
	ID                               string                     `xml:"ID,omitempty"`
	FirstName                        string                     `xml:"FirstName,omitempty"`
	MiddleName                       string                     `xml:"MiddleName,omitempty"`
	LastName                         string                     `xml:"LastName,omitempty"`
	Email                            string                     `xml:"Email,omitempty"`
	EmailOptIn                       bool                       `xml:"EmailOptIn,omitempty"`
	AddressLine1                     string                     `xml:"AddressLine1,omitempty"`
	AddressLine2                     string                     `xml:"AddressLine2,omitempty"`
	City                             string                     `xml:"City,omitempty"`
	State                            string                     `xml:"State,omitempty"`
	PostalCode                       string                     `xml:"PostalCode,omitempty"`
	Country                          string                     `xml:"Country,omitempty"`
	MobilePhone                      string                     `xml:"MobilePhone,omitempty"`
	HomePhone                        string                     `xml:"HomePhone,omitempty"`
	WorkPhone                        string                     `xml:"WorkPhone,omitempty"`
	WorkExtension                    string                     `xml:"WorkExtension,omitempty"`
	BirthDate                        *mb.CustomTime             `xml:"BirthDate,omitempty"`
	FirstAppointmentDate             *mb.CustomTime             `xml:"FirstAppointmentDate,omitempty"`
	ReferredBy                       string                     `xml:"ReferredBy,omitempty"`
	HomeLocation                     *HomeLocation              `xml:"HomeLocation,omitempty"`
	YellowAlert                      string                     `xml:"YellowAlert,omitempty"`
	RedAlert                         string                     `xml:"RedAlert,omitempty"`
	PhotoURL                         string                     `xml:"PhotoURL,omitempty"`
	IsProspect                       bool                       `xml:"IsProspect,omitempty"`
	Status                           string                     `xml:"Status,omitempty"`
	ContactMethod                    *mb.CustomInt16            `xml:"ContactMethod,omitempty"`
}

type ArrayOfClientIndex struct {
	ClientIndex []*ClientIndex `xml:"ClientIndex,omitempty"`
}

type ClientIndex struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndex"`

	*MBObject

	RequiredBusinessMode bool                     `xml:"RequiredBusinessMode,omitempty"`
	RequiredConsumerMode bool                     `xml:"RequiredConsumerMode,omitempty"`
	Action               *ActionCode              `xml:"Action,omitempty"`
	ID                   *mb.CustomInt32          `xml:"ID,omitempty"`
	Name                 string                   `xml:"Name,omitempty"`
	Values               *ArrayOfClientIndexValue `xml:"Values,omitempty"`
}

type ArrayOfClientIndexValue struct {
	ClientIndexValue []*ClientIndexValue `xml:"ClientIndexValue,omitempty"`
}

type ClientIndexValue struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientIndexValue"`

	*MBObject

	Action *ActionCode     `xml:"Action,omitempty"`
	ID     *mb.CustomInt32 `xml:"ID,omitempty"`
	Name   string          `xml:"Name,omitempty"`
	Active bool            `xml:"Active,omitempty"`
}

type ClientCreditCard struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientCreditCard"`

	CardType   string `xml:"CardType,omitempty"`
	LastFour   string `xml:"LastFour,omitempty"`
	CardNumber string `xml:"CardNumber,omitempty"`
	CardHolder string `xml:"CardHolder,omitempty"`
	ExpMonth   string `xml:"ExpMonth,omitempty"`
	ExpYear    string `xml:"ExpYear,omitempty"`
	Address    string `xml:"Address,omitempty"`
	City       string `xml:"City,omitempty"`
	State      string `xml:"State,omitempty"`
	PostalCode string `xml:"PostalCode,omitempty"`
}

type ArrayOfClientRelationship struct {
	ClientRelationship []*ClientRelationship `xml:"ClientRelationship,omitempty"`
}

type ClientRelationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClientRelationship"`

	*MBObject

	RelatedClient    *Client       `xml:"RelatedClient,omitempty"`
	Relationship     *Relationship `xml:"Relationship,omitempty"`
	RelationshipName string        `xml:"RelationshipName,omitempty"`
}

type Relationship struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Relationship"`

	ID                *mb.CustomInt32 `xml:"ID,omitempty"`
	RelationshipName1 string          `xml:"RelationshipName1,omitempty"`
	RelationshipName2 string          `xml:"RelationshipName2,omitempty"`
}

type ArrayOfRep struct {
	Rep []*Rep `xml:"Rep,omitempty"`
}

type ArrayOfSalesRep struct {
	SalesRep []*SalesRep `xml:"SalesRep,omitempty"`
}

type ArrayOfCustomClientField struct {
	CustomClientField []*CustomClientField `xml:"CustomClientField,omitempty"`
}

type CustomClientField struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CustomClientField"`

	ID       *mb.CustomInt32 `xml:"ID,omitempty"`
	DataType string          `xml:"DataType,omitempty"`
	Name     string          `xml:"Name,omitempty"`
	Value    string          `xml:"Value,omitempty"`
}

type Liability struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Liability"`

	IsReleased    bool            `xml:"IsReleased,omitempty"`
	AgreementDate *mb.CustomTime  `xml:"AgreementDate,omitempty"`
	ReleasedBy    *mb.CustomInt64 `xml:"ReleasedBy,omitempty"`
}

type ProspectStage struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProspectStage"`

	ID          *mb.CustomInt32 `xml:"ID,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Active      bool            `xml:"Active,omitempty"`
}

type ArrayOfResource struct {
	Resource []*Resource `xml:"Resource,omitempty"`
}

type ArrayOfUnavailability struct {
	Unavailability []*Unavailability `xml:"Unavailability,omitempty"`
}

type ArrayOfAvailability struct {
	Availability []*Availability `xml:"Availability,omitempty"`
}

type ArrayOfLocation struct {
	Location []*Location `xml:"Location,omitempty"`
}

type ArrayOfProviderIDUpdate struct {
	ProviderIDUpdate []*ProviderIDUpdate `xml:"ProviderIDUpdate,omitempty"`
}

type ProviderIDUpdate struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ProviderIDUpdate"`

	OldProviderID string `xml:"OldProviderID,omitempty"`
	NewProviderID string `xml:"NewProviderID,omitempty"`
}

type Visit struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Visit"`

	*MBObject

	ID                          *mb.CustomInt64 `xml:"ID,omitempty"`
	ClassID                     *mb.CustomInt32 `xml:"ClassID,omitempty"`
	AppointmentID               *mb.CustomInt32 `xml:"AppointmentID,omitempty"`
	AppointmentGenderPreference string          `xml:"AppointmentGenderPreference,omitempty"`
	StartDateTime               *mb.CustomTime  `xml:"StartDateTime,omitempty"`
	LateCancelled               bool            `xml:"LateCancelled,omitempty"`
	EndDateTime                 *mb.CustomTime  `xml:"EndDateTime,omitempty"`
	Name                        string          `xml:"Name,omitempty"`
	Staff                       *Staff          `xml:"Staff,omitempty"`
	Location                    *Location       `xml:"Location,omitempty"`
	Client                      *Client         `xml:"Client,omitempty"`
	WebSignup                   bool            `xml:"WebSignup,omitempty"`
	Action                      *ActionCode     `xml:"Action,omitempty"`
	SignedIn                    bool            `xml:"SignedIn,omitempty"`
	AppointmentStatus           string          `xml:"AppointmentStatus,omitempty"`
	MakeUp                      bool            `xml:"MakeUp,omitempty"`
	Service                     *Service        `xml:"Service,omitempty"`
}

type ArrayOfVisit struct {
	Visit []*Visit `xml:"Visit,omitempty"`
}

type ArrayOfClient struct {
	Client []*Client `xml:"Client,omitempty"`
}

type UpdateClientVisitsRequest struct {
	*MBRequest

	Visits    *ArrayOfVisit `xml:"Visits,omitempty"`
	Test      bool          `xml:"Test,omitempty"`
	SendEmail bool          `xml:"SendEmail,omitempty"`
}

type UpdateClientVisitsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 UpdateClientVisitsResult"`

	*MBResult

	Visits *ArrayOfVisit `xml:"Visits,omitempty"`
}

type GetClassVisitsRequest struct {
	*MBRequest

	ClassID *mb.CustomInt32 `xml:"ClassID,omitempty"`
}

type GetClassVisitsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassVisitsResult"`

	*MBResult

	Class *Class `xml:"Class,omitempty"`
}

type GetClassDescriptionsRequest struct {
	*MBRequest

	ClassDescriptionIDs *ArrayOfInt    `xml:"ClassDescriptionIDs,omitempty"`
	ProgramIDs          *ArrayOfInt    `xml:"ProgramIDs,omitempty"`
	StaffIDs            *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	LocationIDs         *ArrayOfInt    `xml:"LocationIDs,omitempty"`
	StartClassDateTime  *mb.CustomTime `xml:"StartClassDateTime,omitempty"`
	EndClassDateTime    *mb.CustomTime `xml:"EndClassDateTime,omitempty"`
}

type GetClassDescriptionsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassDescriptionsResult"`

	*MBResult

	ClassDescriptions *ArrayOfClassDescription `xml:"ClassDescriptions,omitempty"`
}

type ArrayOfClassDescription struct {
	ClassDescription []*ClassDescription `xml:"ClassDescription,omitempty"`
}

type GetEnrollmentsRequest struct {
	*MBRequest

	LocationIDs      *ArrayOfInt    `xml:"LocationIDs,omitempty"`
	ClassScheduleIDs *ArrayOfInt    `xml:"ClassScheduleIDs,omitempty"`
	StaffIDs         *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	ProgramIDs       *ArrayOfInt    `xml:"ProgramIDs,omitempty"`
	SessionTypeIDs   *ArrayOfInt    `xml:"SessionTypeIDs,omitempty"`
	SemesterIDs      *ArrayOfInt    `xml:"SemesterIDs,omitempty"`
	CourseIDs        *ArrayOfLong   `xml:"CourseIDs,omitempty"`
	StartDate        *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate          *mb.CustomTime `xml:"EndDate,omitempty"`
}

type GetEnrollmentsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetEnrollmentsResult"`

	*MBResult

	Enrollments *ArrayOfClassSchedule `xml:"Enrollments,omitempty"`
}

type ArrayOfClassSchedule struct {
	ClassSchedule []*ClassSchedule `xml:"ClassSchedule,omitempty"`
}

type ClassSchedule struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 ClassSchedule"`

	*MBObject

	Classes                    *ArrayOfClass     `xml:"Classes,omitempty"`
	Clients                    *ArrayOfClient    `xml:"Clients,omitempty"`
	Course                     *Course           `xml:"Course,omitempty"`
	SemesterID                 *mb.CustomInt32   `xml:"SemesterID,omitempty"`
	IsAvailable                bool              `xml:"IsAvailable,omitempty"`
	Action                     *ActionCode       `xml:"Action,omitempty"`
	ID                         *mb.CustomInt32   `xml:"ID,omitempty"`
	ClassDescription           *ClassDescription `xml:"ClassDescription,omitempty"`
	DaySunday                  bool              `xml:"DaySunday,omitempty"`
	DayMonday                  bool              `xml:"DayMonday,omitempty"`
	DayTuesday                 bool              `xml:"DayTuesday,omitempty"`
	DayWednesday               bool              `xml:"DayWednesday,omitempty"`
	DayThursday                bool              `xml:"DayThursday,omitempty"`
	DayFriday                  bool              `xml:"DayFriday,omitempty"`
	DaySaturday                bool              `xml:"DaySaturday,omitempty"`
	AllowOpenEnrollment        bool              `xml:"AllowOpenEnrollment,omitempty"`
	AllowDateForwardEnrollment bool              `xml:"AllowDateForwardEnrollment,omitempty"`
	StartTime                  *mb.CustomTime    `xml:"StartTime,omitempty"`
	EndTime                    *mb.CustomTime    `xml:"EndTime,omitempty"`
	StartDate                  *mb.CustomTime    `xml:"StartDate,omitempty"`
	EndDate                    *mb.CustomTime    `xml:"EndDate,omitempty"`
	Staff                      *Staff            `xml:"Staff,omitempty"`
	Location                   *Location         `xml:"Location,omitempty"`
}

type Course struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Course"`

	ID          *mb.CustomInt64 `xml:"ID,omitempty"`
	Name        string          `xml:"Name,omitempty"`
	Description string          `xml:"Description,omitempty"`
	Notes       string          `xml:"Notes,omitempty"`
	StartDate   *mb.CustomTime  `xml:"StartDate,omitempty"`
	EndDate     *mb.CustomTime  `xml:"EndDate,omitempty"`
	Location    *Location       `xml:"Location,omitempty"`
	Organizer   *Staff          `xml:"Organizer,omitempty"`
	Program     *Program        `xml:"Program,omitempty"`
	ImageURL    string          `xml:"ImageURL,omitempty"`
}

type GetClassSchedulesRequest struct {
	*MBRequest

	LocationIDs      *ArrayOfInt    `xml:"LocationIDs,omitempty"`
	ClassScheduleIDs *ArrayOfInt    `xml:"ClassScheduleIDs,omitempty"`
	StaffIDs         *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	ProgramIDs       *ArrayOfInt    `xml:"ProgramIDs,omitempty"`
	SessionTypeIDs   *ArrayOfInt    `xml:"SessionTypeIDs,omitempty"`
	StartDate        *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate          *mb.CustomTime `xml:"EndDate,omitempty"`
}

type GetClassSchedulesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetClassSchedulesResult"`

	*MBResult

	ClassSchedules *ArrayOfClassSchedule `xml:"ClassSchedules,omitempty"`
}

type AddClientsToClassesRequest struct {
	*MBRequest

	ClientIDs       *ArrayOfString  `xml:"ClientIDs,omitempty"`
	ClassIDs        *ArrayOfInt     `xml:"ClassIDs,omitempty"`
	Test            bool            `xml:"Test,omitempty"`
	RequirePayment  bool            `xml:"RequirePayment"`
	Waitlist        bool            `xml:"Waitlist,omitempty"`
	SendEmail       bool            `xml:"SendEmail,omitempty"`
	WaitlistEntryID *mb.CustomInt32 `xml:"WaitlistEntryID,omitempty"`
	ClientServiceID *mb.CustomInt32 `xml:"ClientServiceID,omitempty"`
}

type AddClientsToClassesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToClassesResult"`

	*MBResult

	Classes *ArrayOfClass `xml:"Classes,omitempty"`
}

type RemoveClientsFromClassesRequest struct {
	*MBRequest

	ClientIDs  *ArrayOfString `xml:"ClientIDs,omitempty"`
	ClassIDs   *ArrayOfInt    `xml:"ClassIDs,omitempty"`
	Test       bool           `xml:"Test,omitempty"`
	SendEmail  bool           `xml:"SendEmail,omitempty"`
	LateCancel bool           `xml:"LateCancel,omitempty"`
}

type RemoveClientsFromClassesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveClientsFromClassesResult"`

	*MBResult

	Classes *ArrayOfClass `xml:"Classes,omitempty"`
}

type AddClientsToEnrollmentsRequest struct {
	*MBRequest

	ClientIDs         *ArrayOfString   `xml:"ClientIDs,omitempty"`
	ClassScheduleIDs  *ArrayOfInt      `xml:"ClassScheduleIDs,omitempty"`
	CourseIDs         *ArrayOfInt      `xml:"CourseIDs,omitempty"`
	EnrollDateForward *mb.CustomTime   `xml:"EnrollDateForward,omitempty"`
	EnrollOpen        *ArrayOfDateTime `xml:"EnrollOpen,omitempty"`
	Test              bool             `xml:"Test,omitempty"`
	SendEmail         bool             `xml:"SendEmail,omitempty"`
	Waitlist          bool             `xml:"Waitlist,omitempty"`
	WaitlistEntryID   *mb.CustomInt32  `xml:"WaitlistEntryID,omitempty"`
}

type ArrayOfDateTime struct {
	DateTime []*mb.CustomTime `xml:"dateTime,omitempty"`
}

type AddClientsToEnrollmentsResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 AddClientsToEnrollmentsResult"`

	*MBResult

	Enrollments *ArrayOfClassSchedule `xml:"Enrollments,omitempty"`
}

type RemoveFromWaitlistRequest struct {
	*MBRequest

	WaitlistEntryIDs *ArrayOfInt `xml:"WaitlistEntryIDs,omitempty"`
}

type RemoveFromWaitlistResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 RemoveFromWaitlistResult"`

	*MBResult
}

type GetSemestersRequest struct {
	*MBRequest

	SemesterIDs *ArrayOfInt    `xml:"SemesterIDs,omitempty"`
	StartDate   *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate     *mb.CustomTime `xml:"EndDate,omitempty"`
}

type GetSemestersResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetSemestersResult"`

	*MBResult

	Semesters *ArrayOfSemester `xml:"Semesters,omitempty"`
}

type ArrayOfSemester struct {
	Semester []*Semester `xml:"Semester,omitempty"`
}

type Semester struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Semester"`

	ID                        *mb.CustomInt32   `xml:"ID,omitempty"`
	Name                      string            `xml:"Name,omitempty"`
	Description               string            `xml:"Description,omitempty"`
	StartDate                 *mb.CustomTime    `xml:"StartDate,omitempty"`
	EndDate                   *mb.CustomTime    `xml:"EndDate,omitempty"`
	MultiRegistrationDiscount *mb.CustomFloat64 `xml:"MultiRegistrationDiscount,omitempty"`
	MultiRegistrationDeadline *mb.CustomTime    `xml:"MultiRegistrationDeadline,omitempty"`
}

type GetCoursesRequest struct {
	*MBRequest

	LocationIDs *ArrayOfInt    `xml:"LocationIDs,omitempty"`
	CourseIDs   *ArrayOfLong   `xml:"CourseIDs,omitempty"`
	StaffIDs    *ArrayOfLong   `xml:"StaffIDs,omitempty"`
	ProgramIDs  *ArrayOfInt    `xml:"ProgramIDs,omitempty"`
	StartDate   *mb.CustomTime `xml:"StartDate,omitempty"`
	EndDate     *mb.CustomTime `xml:"EndDate,omitempty"`
	SemesterIDs *ArrayOfInt    `xml:"SemesterIDs,omitempty"`
}

type GetCoursesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetCoursesResult"`

	*MBResult

	Courses *ArrayOfCourse `xml:"Courses,omitempty"`
}

type ArrayOfCourse struct {
	Course []*Course `xml:"Course,omitempty"`
}

type GetWaitlistEntriesRequest struct {
	*MBRequest

	ClassScheduleIDs *ArrayOfInt    `xml:"ClassScheduleIDs,omitempty"`
	ClientIDs        *ArrayOfString `xml:"ClientIDs,omitempty"`
	WaitlistEntryIDs *ArrayOfInt    `xml:"WaitlistEntryIDs,omitempty"`
	ClassIDs         *ArrayOfInt    `xml:"ClassIDs,omitempty"`
	HidePastEntries  bool           `xml:"HidePastEntries,omitempty"`
}

type GetWaitlistEntriesResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 GetWaitlistEntriesResult"`

	*MBResult

	WaitlistEntries *ArrayOfWaitlistEntry `xml:"WaitlistEntries,omitempty"`
}

type ArrayOfWaitlistEntry struct {
	WaitlistEntry []*WaitlistEntry `xml:"WaitlistEntry,omitempty"`
}

type WaitlistEntry struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 WaitlistEntry"`

	ID                    *mb.CustomInt32 `xml:"ID,omitempty"`
	ClassID               *mb.CustomInt64 `xml:"ClassID,omitempty"`
	ClassDate             *mb.CustomTime  `xml:"ClassDate,omitempty"`
	Client                *Client         `xml:"Client,omitempty"`
	ClassSchedule         *ClassSchedule  `xml:"ClassSchedule,omitempty"`
	EnrollmentDateForward *mb.CustomTime  `xml:"EnrollmentDateForward,omitempty"`
	RequestDateTime       *mb.CustomTime  `xml:"RequestDateTime,omitempty"`
	Web                   bool            `xml:"Web,omitempty"`
	VisitRefNo            *mb.CustomInt32 `xml:"VisitRefNo,omitempty"`
}

type SubstituteClassTeacherRequest struct {
	*MBRequest

	ClassID           *mb.CustomInt64 `xml:"ClassID,omitempty"`
	StaffID           *mb.CustomInt64 `xml:"StaffID,omitempty"`
	OverrideConflicts bool            `xml:"OverrideConflicts,omitempty"`
	SendClientEmail   bool            `xml:"SendClientEmail,omitempty"`
	SendOldStaffEmail bool            `xml:"SendOldStaffEmail,omitempty"`
	SendNewStaffEmail bool            `xml:"SendNewStaffEmail,omitempty"`
}

type SubstituteClassTeacherResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 SubstituteClassTeacherResult"`

	*MBResult

	Class *Class `xml:"Class,omitempty"`
}

type CancelSingleClassRequest struct {
	*MBRequest

	ClassID         *mb.CustomInt64 `xml:"ClassID,omitempty"`
	HideCancel      bool            `xml:"HideCancel,omitempty"`
	SendClientEmail bool            `xml:"SendClientEmail,omitempty"`
	SendStaffEmail  bool            `xml:"SendStaffEmail,omitempty"`
}

type CancelSingleClassResult struct {
	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 CancelSingleClassResult"`

	*MBResult

	Class *Class `xml:"Class,omitempty"`
}

type Class_x0020_ServiceSoap struct {
	client *SOAPClient
}

func NewClass_x0020_ServiceSoap(url string, tls bool, auth *BasicAuth) *Class_x0020_ServiceSoap {
	if url == "" {
		url = "https://api.mindbodyonline.com/0_5/ClassService.asmx"
	}
	client := NewSOAPClient(url, tls, auth)

	return &Class_x0020_ServiceSoap{
		client: client,
	}
}

/* Gets a list of classes. */
func (service *Class_x0020_ServiceSoap) GetClasses(request *GetClasses) (*GetClassesResponse, error) {
	response := new(GetClassesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClasses", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Update a list of visits. */
func (service *Class_x0020_ServiceSoap) UpdateClientVisits(request *UpdateClientVisits) (*UpdateClientVisitsResponse, error) {
	response := new(UpdateClientVisitsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/UpdateClientVisits", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a class with a list of clients. */
func (service *Class_x0020_ServiceSoap) GetClassVisits(request *GetClassVisits) (*GetClassVisitsResponse, error) {
	response := new(GetClassVisitsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClassVisits", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of class descriptions. */
func (service *Class_x0020_ServiceSoap) GetClassDescriptions(request *GetClassDescriptions) (*GetClassDescriptionsResponse, error) {
	response := new(GetClassDescriptionsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClassDescriptions", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of enrollments. */
func (service *Class_x0020_ServiceSoap) GetEnrollments(request *GetEnrollments) (*GetEnrollmentsResponse, error) {
	response := new(GetEnrollmentsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetEnrollments", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of class schedules. */
func (service *Class_x0020_ServiceSoap) GetClassSchedules(request *GetClassSchedules) (*GetClassSchedulesResponse, error) {
	response := new(GetClassSchedulesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetClassSchedules", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Adds clients to classes (signup). */
func (service *Class_x0020_ServiceSoap) AddClientsToClasses(request *AddClientsToClasses) (*AddClientsToClassesResponse, error) {
	response := new(AddClientsToClassesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddClientsToClasses", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Removes clients from classes. */
func (service *Class_x0020_ServiceSoap) RemoveClientsFromClasses(request *RemoveClientsFromClasses) (*RemoveClientsFromClassesResponse, error) {
	response := new(RemoveClientsFromClassesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/RemoveClientsFromClasses", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Adds clients to enrollments (signup). */
func (service *Class_x0020_ServiceSoap) AddClientsToEnrollments(request *AddClientsToEnrollments) (*AddClientsToEnrollmentsResponse, error) {
	response := new(AddClientsToEnrollmentsResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/AddClientsToEnrollments", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Removes client from enrollment waitlist */
func (service *Class_x0020_ServiceSoap) RemoveFromWaitlist(request *RemoveFromWaitlist) (*RemoveFromWaitlistResponse, error) {
	response := new(RemoveFromWaitlistResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/RemoveFromWaitlist", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of semesters. */
func (service *Class_x0020_ServiceSoap) GetSemesters(request *GetSemesters) (*GetSemestersResponse, error) {
	response := new(GetSemestersResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetSemesters", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Gets a list of courses. */
func (service *Class_x0020_ServiceSoap) GetCourses(request *GetCourses) (*GetCoursesResponse, error) {
	response := new(GetCoursesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetCourses", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Get waitlist entries. */
func (service *Class_x0020_ServiceSoap) GetWaitlistEntries(request *GetWaitlistEntries) (*GetWaitlistEntriesResponse, error) {
	response := new(GetWaitlistEntriesResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/GetWaitlistEntries", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Substitutes the teacher for a class. */
func (service *Class_x0020_ServiceSoap) SubstituteClassTeacher(request *SubstituteClassTeacher) (*SubstituteClassTeacherResponse, error) {
	response := new(SubstituteClassTeacherResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/SubstituteClassTeacher", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Substitutes the teacher for a class. */
func (service *Class_x0020_ServiceSoap) SubtituteClassTeacher(request *SubtituteClassTeacher) (*SubtituteClassTeacherResponse, error) {
	response := new(SubtituteClassTeacherResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/SubtituteClassTeacher", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

/* Cancels a single class instance. */
func (service *Class_x0020_ServiceSoap) CancelSingleClass(request *CancelSingleClass) (*CancelSingleClassResponse, error) {
	response := new(CancelSingleClassResponse)
	err := service.client.Call("http://clients.mindbodyonline.com/api/0_5/CancelSingleClass", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

var timeout = time.Duration(30 * time.Second)

func dialTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, timeout)
}

type SOAPEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`

	Body SOAPBody
}

type SOAPHeader struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`

	Header interface{}
}

type SOAPBody struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Body"`

	Fault   *SOAPFault  `xml:",omitempty"`
	Content interface{} `xml:",omitempty"`
}

type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

type BasicAuth struct {
	Login    string
	Password string
}

type SOAPClient struct {
	url  string
	tls  bool
	auth *BasicAuth
}

func (b *SOAPBody) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &SOAPFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

func (f *SOAPFault) Error() string {
	return f.String
}

func NewSOAPClient(url string, tls bool, auth *BasicAuth) *SOAPClient {
	return &SOAPClient{
		url:  url,
		tls:  tls,
		auth: auth,
	}
}

func (s *SOAPClient) Call(soapAction string, request, response interface{}) error {
	envelope := SOAPEnvelope{
	//Header:        SoapHeader{},
	}

	envelope.Body.Content = request
	buffer := new(bytes.Buffer)

	encoder := xml.NewEncoder(buffer)
	//encoder.Indent("  ", "    ")

	if err := encoder.Encode(envelope); err != nil {
		return err
	}

	if err := encoder.Flush(); err != nil {
		return err
	}

	log.Println(buffer.String())

	req, err := http.NewRequest("POST", s.url, buffer)
	if err != nil {
		return err
	}
	if s.auth != nil {
		req.SetBasicAuth(s.auth.Login, s.auth.Password)
	}

	req.Header.Add("Content-Type", "text/xml; charset=\"utf-8\"")
	if soapAction != "" {
		req.Header.Add("SOAPAction", soapAction)
	}

	req.Header.Set("User-Agent", "gowsdl/0.1")
	req.Close = true

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: s.tls,
		},
		Dial: dialTimeout,
	}

	client := &http.Client{Transport: tr}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	rawbody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if len(rawbody) == 0 {
		log.Println("empty response")
		return nil
	}

	respEnvelope := new(SOAPEnvelope)
	respEnvelope.Body = SOAPBody{Content: response}
	err = xml.Unmarshal(rawbody, respEnvelope)
	if err != nil {
		return err
	}

	fault := respEnvelope.Body.Fault
	if fault != nil {
		return fault
	}

	return nil
}

type HomeLocation struct {
	*Location

	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 HomeLocation"`
}

type Service struct {
	*ClientService

	XMLName xml.Name `xml:"http://clients.mindbodyonline.com/api/0_5 Service"`
}
