#!/bin/bash
set -e

# Cleanup
rm -rf services && mkdir services && cd services

for service in client site staff class sale; do
    name="$(tr '[:lower:]' '[:upper:]' <<< ${service:0:1})${service:1}"
    gowsdl -p $service -o $service.go "https://api.mindbodyonline.com/0_5/${name}Service.asmx?wsdl"

    # Add our custom types import
    sed -i "s#import (#import(\\`echo -e '\n\t'`mb \"bitbucket.org/canopei/mindbody\"#g" "${service}/${service}.go"

    # Replace the time.Time, float32/float64 and int32/int64 types with our custom types
    sed -i "/var _ time.Time/d" "${service}/${service}.go"
    sed -i "s/time.Time/*mb.CustomTime/" "${service}/${service}.go"
    sed -i -E "s/int([0-9]+)/*mb.CustomInt\1/" "${service}/${service}.go"
    sed -i -E "s/float([0-9]+)/*mb.CustomFloat\1/" "${service}/${service}.go"

    # Cleanup the XML
    sed -i 's/MBRequest"/Request"/g' "${service}/${service}.go"
    sed -i -E "/XMLName.* ArrayOf[a-zA-Z]+\"/d" "${service}/${service}.go"

    # *Request tags should have no XMLName
    sed -i "/XMLName.* .*Request\"/d" "${service}/${service}.go"

    # Add XSIType field to MBObject so we can set an attribute xsi:type on anything derived from MBObject
    sed -i "s# MBObject\"\`# MBObject\"\`\\`echo -e '\n\t'`XSIType string \`xml:\"xsi:type,attr,omitempty\"\`#g" "${service}/${service}.go"

    # Do not output the rawbody of the SOAP responses
    sed -i '/log.Println(string(rawbody))/d' "${service}/${service}.go"
done

# Remove the omitempty from SearchText
sed -i 's/\(SearchText string.*`xml:"SearchText\),omitempty"`/\1"`/' client/client.go

# Remove the omitempty from RequirePayment
sed -i 's/\(RequirePayment.*bool.*`xml:"RequirePayment\),omitempty"`/\1"`/' class/class.go


# Add fields to sale - Item
sed -i "s# Item\"\`# Item\"\`\\`echo -e '\n\t'`ID string \`xml:\"ID,omitempty\"\`#g" sale/sale.go

# Add fields to sale - PaymentInfo
sed -i "s# PaymentInfo\"\`# PaymentInfo\"\`\\`echo -e '\n\t'`Amount *mb.CustomFloat64 \`xml:\"Amount,omitempty\"\`#g" sale/sale.go
sed -i "s# PaymentInfo\"\`# PaymentInfo\"\`\\`echo -e '\n\t'`ID *mb.CustomInt32 \`xml:\"ID,omitempty\"\`#g" sale/sale.go
sed -i "s# PaymentInfo\"\`# PaymentInfo\"\`\\`echo -e '\n\t'`*MBObject#g" sale/sale.go

# Add fields to sale - SOAPEnvelope
sed -i "s# Envelope\"\`# Envelope\"\`\\`echo -e '\n\t'`XSINS string \`xml:\"xmlns:xsi,attr,omitempty\"\`#g" sale/sale.go
sed -i "s#SOAPEnvelope{#SOAPEnvelope{\\`echo -e '\n\t\t'`XSINS: \"http://www.w3.org/2001/XMLSchema-instance\",#g" sale/sale.go


# Fix the HomeLocation in these services
for service in client class sale; do
    sed -i 's/\(HomeLocation.*\)\*Location/\1\*HomeLocation/' "${service}/${service}.go"
    cat <<EOF >> "${service}/${service}.go"

    type HomeLocation struct {
        *Location

        XMLName xml.Name \`xml:"http://clients.mindbodyonline.com/api/0_5 HomeLocation"\`
    }
EOF
done

# Fix the Service in Visit in these services
for service in client class; do
    sed -i '/MakeUp/{n;s/.*/Service *Service \`xml:"Service,omitempty"\`/}' "${service}/${service}.go"
    cat <<EOF >> "${service}/${service}.go"

    type Service struct {
        *ClientService

        XMLName xml.Name \`xml:"http://clients.mindbodyonline.com/api/0_5 Service"\`
    }
EOF
done

# Run gofmt on the generated files
for service in client site staff class sale; do
    go fmt "${service}/${service}.go"
done