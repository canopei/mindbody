package helpers

import (
	mb "bitbucket.org/canopei/mindbody"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	staffMb "bitbucket.org/canopei/mindbody/services/staff"
)

// CreateClientMBRequest creates a Client service MBRequest
func CreateClientMBRequest(siteIDs []int32, sourceUsername string, sourcePassword string) *clientMb.MBRequest {
	var mbSiteIDs []*mb.CustomInt32
	for _, id := range siteIDs {
		if !mbContainsInt32(mbSiteIDs, id) {
			mbSiteIDs = append(mbSiteIDs, &mb.CustomInt32{Int: id})
		}
	}

	mbCredentials := &clientMb.SourceCredentials{
		SourceName: sourceUsername,
		Password:   sourcePassword,
		SiteIDs:    &clientMb.ArrayOfInt{Int: mbSiteIDs},
	}

	return &clientMb.MBRequest{SourceCredentials: mbCredentials}
}

// CreateSiteMBRequest creates a Site service MBRequest
func CreateSiteMBRequest(siteIDs []int32, sourceUsername string, sourcePassword string) *siteMb.MBRequest {
	var mbSiteIDs []*mb.CustomInt32
	for _, id := range siteIDs {
		if !mbContainsInt32(mbSiteIDs, id) {
			mbSiteIDs = append(mbSiteIDs, &mb.CustomInt32{Int: id})
		}
	}

	mbCredentials := &siteMb.SourceCredentials{
		SourceName: sourceUsername,
		Password:   sourcePassword,
		SiteIDs:    &siteMb.ArrayOfInt{Int: mbSiteIDs},
	}

	return &siteMb.MBRequest{SourceCredentials: mbCredentials}
}

// CreateStaffMBRequest creates a Staff service MBRequest
func CreateStaffMBRequest(siteIDs []int32, sourceUsername string, sourcePassword string) *staffMb.MBRequest {
	var mbSiteIDs []*mb.CustomInt32
	for _, id := range siteIDs {
		if !mbContainsInt32(mbSiteIDs, id) {
			mbSiteIDs = append(mbSiteIDs, &mb.CustomInt32{Int: id})
		}
	}

	mbCredentials := &staffMb.SourceCredentials{
		SourceName: sourceUsername,
		Password:   sourcePassword,
		SiteIDs:    &staffMb.ArrayOfInt{Int: mbSiteIDs},
	}

	return &staffMb.MBRequest{SourceCredentials: mbCredentials}
}

// CreateClassMBRequest creates a Class service MBRequest
func CreateClassMBRequest(siteIDs []int32, sourceUsername string, sourcePassword string) *classMb.MBRequest {
	var mbSiteIDs []*mb.CustomInt32
	for _, id := range siteIDs {
		if !mbContainsInt32(mbSiteIDs, id) {
			mbSiteIDs = append(mbSiteIDs, &mb.CustomInt32{Int: id})
		}
	}

	mbCredentials := &classMb.SourceCredentials{
		SourceName: sourceUsername,
		Password:   sourcePassword,
		SiteIDs:    &classMb.ArrayOfInt{Int: mbSiteIDs},
	}

	return &classMb.MBRequest{SourceCredentials: mbCredentials}
}

// CreateSaleMBRequest creates a Sale service MBRequest
func CreateSaleMBRequest(siteIDs []int32, sourceUsername string, sourcePassword string) *saleMb.MBRequest {
	var mbSiteIDs []*mb.CustomInt32
	for _, id := range siteIDs {
		if !mbContainsInt32(mbSiteIDs, id) {
			mbSiteIDs = append(mbSiteIDs, &mb.CustomInt32{Int: id})
		}
	}

	mbCredentials := &saleMb.SourceCredentials{
		SourceName: sourceUsername,
		Password:   sourcePassword,
		SiteIDs:    &saleMb.ArrayOfInt{Int: mbSiteIDs},
	}

	return &saleMb.MBRequest{SourceCredentials: mbCredentials}
}

func mbContainsInt32(s []*mb.CustomInt32, e int32) bool {
	for _, a := range s {
		if a.Int == e {
			return true
		}
	}
	return false
}
