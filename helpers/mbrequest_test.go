package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"

	classMb "bitbucket.org/canopei/mindbody/services/class"
	clientMb "bitbucket.org/canopei/mindbody/services/client"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	siteMb "bitbucket.org/canopei/mindbody/services/site"
	staffMb "bitbucket.org/canopei/mindbody/services/staff"
)

func TestCreateClientMBRequest(t *testing.T) {
	siteIDs := []int32{1, 2, 2, 3}

	request := CreateClientMBRequest(siteIDs, "user", "pass")

	assert.IsType(t, &clientMb.MBRequest{}, request)
	// Check some values
	assert.Equal(t, siteIDs[0], request.SourceCredentials.SiteIDs.Int[0].Int)
	assert.Equal(t, siteIDs[3], request.SourceCredentials.SiteIDs.Int[2].Int)
	// Duplicates are stripped
	assert.Len(t, request.SourceCredentials.SiteIDs.Int, 3)
	assert.Equal(t, "user", request.SourceCredentials.SourceName)
	assert.Equal(t, "pass", request.SourceCredentials.Password)
}

func TestCreateStaffMBRequest(t *testing.T) {
	siteIDs := []int32{1, 2, 2, 3}

	request := CreateStaffMBRequest(siteIDs, "user", "pass")

	assert.IsType(t, &staffMb.MBRequest{}, request)
	// Check some values
	assert.Equal(t, siteIDs[0], request.SourceCredentials.SiteIDs.Int[0].Int)
	assert.Equal(t, siteIDs[3], request.SourceCredentials.SiteIDs.Int[2].Int)
	// Duplicates are stripped
	assert.Len(t, request.SourceCredentials.SiteIDs.Int, 3)
	assert.Equal(t, "user", request.SourceCredentials.SourceName)
	assert.Equal(t, "pass", request.SourceCredentials.Password)
}

func TestCreateClassMBRequest(t *testing.T) {
	siteIDs := []int32{1, 2, 2, 3}

	request := CreateClassMBRequest(siteIDs, "user", "pass")

	assert.IsType(t, &classMb.MBRequest{}, request)
	// Check some values
	assert.Equal(t, siteIDs[0], request.SourceCredentials.SiteIDs.Int[0].Int)
	assert.Equal(t, siteIDs[3], request.SourceCredentials.SiteIDs.Int[2].Int)
	// Duplicates are stripped
	assert.Len(t, request.SourceCredentials.SiteIDs.Int, 3)
	assert.Equal(t, "user", request.SourceCredentials.SourceName)
	assert.Equal(t, "pass", request.SourceCredentials.Password)
}

func TestCreateSaleMBRequest(t *testing.T) {
	siteIDs := []int32{1, 2, 2, 3}

	request := CreateSaleMBRequest(siteIDs, "user", "pass")

	assert.IsType(t, &saleMb.MBRequest{}, request)
	// Check some values
	assert.Equal(t, siteIDs[0], request.SourceCredentials.SiteIDs.Int[0].Int)
	assert.Equal(t, siteIDs[3], request.SourceCredentials.SiteIDs.Int[2].Int)
	// Duplicates are stripped
	assert.Len(t, request.SourceCredentials.SiteIDs.Int, 3)
	assert.Equal(t, "user", request.SourceCredentials.SourceName)
	assert.Equal(t, "pass", request.SourceCredentials.Password)
}

func TestCreateSiteMBRequest(t *testing.T) {
	siteIDs := []int32{1, 2, 2, 3}

	request := CreateSiteMBRequest(siteIDs, "user", "pass")

	assert.IsType(t, &siteMb.MBRequest{}, request)
	// Check some values
	assert.Equal(t, siteIDs[0], request.SourceCredentials.SiteIDs.Int[0].Int)
	assert.Equal(t, siteIDs[3], request.SourceCredentials.SiteIDs.Int[2].Int)
	// Duplicates are stripped
	assert.Len(t, request.SourceCredentials.SiteIDs.Int, 3)
	assert.Equal(t, "user", request.SourceCredentials.SourceName)
	assert.Equal(t, "pass", request.SourceCredentials.Password)
}
